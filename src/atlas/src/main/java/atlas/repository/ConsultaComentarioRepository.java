package atlas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.ConsultaComentario;
import atlas.model.Item;

public interface ConsultaComentarioRepository extends JpaRepository<ConsultaComentario,Integer> {

   
    List<ConsultaComentario> findByItemAndNombre(Item item, String nombre);
}
