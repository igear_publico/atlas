package atlas.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import atlas.model.Item;
import atlas.model.ItemResumen;
import atlas.model.ProjectIdAndTitulo;

@Repository
public interface ItemResumenRepository extends JpaRepository<ItemResumen,Integer> {

}
