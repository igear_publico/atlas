package atlas.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import atlas.model.Item;
import atlas.model.ProjectIdAndTitulo;
import atlas.model.Subtema;

@Repository
public interface ItemRepository extends JpaRepository<Item,Integer> {

	Item findById(int id);
	
	@Query(value="select * from atlas3.item where es_sql", nativeQuery=true)
	List<Item> findAllSQL();
	
	@Query(value="select id,titulo from atlas3.item", nativeQuery=true)
	List<ProjectIdAndTitulo> findAllBasic();

	@Query(value="select id,titulo from atlas3.item", nativeQuery=true)
	List<ProjectIdAndTitulo> findAllBasic2();

    @Query(value="select t.* from atlas3.item t inner join atlas3.v_item_uri v on v.id_item=t.id where atlas=:atlas and tema=:tema  and subtema=:subtema and etiqueta=:etiqueta", nativeQuery=true)
    List<Item> findList(@Param("atlas") String atlas,@Param("tema") String tema,@Param("subtema") String subtema,@Param("etiqueta") String etiqueta);


    
	@Transactional
	void deleteById(int id);
}
