package atlas.repository;

import atlas.model.Atlas;
import atlas.model.Tema;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface AtlasRepository extends JpaRepository<Atlas,Integer> {

    // Busca a partir de su id
    Atlas findById(int id);

    Atlas findByEtiqueta(String etiqueta);

    @Transactional
    void deleteById(int id);
}
