package atlas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.Item;
import atlas.model.Recurso;

public interface RecursoRepository extends JpaRepository<Recurso,Integer> {

   
   List<Recurso> findByItemAndTipo(Item item, int tipo);

   List<Recurso> findByItem(Item item);
}
