package atlas.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.Atlas;
import atlas.model.AtlasXTemas;

public interface AtlasXTemasRepository extends JpaRepository<AtlasXTemas,Integer> {



    @Transactional
    void deleteByAtlas(Atlas atlas);
}
