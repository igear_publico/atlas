package atlas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.Dato;
import atlas.model.Item;

public interface DatoRepository extends JpaRepository<Dato,Integer> {
	  Dato findById(int id);

}
