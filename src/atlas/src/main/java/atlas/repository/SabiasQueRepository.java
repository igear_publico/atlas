package atlas.repository;

import atlas.model.SabiasQue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;


public interface SabiasQueRepository extends JpaRepository<SabiasQue,Integer> {

    SabiasQue findById(int id);

    @Query(value="select distinct sq.* \n" +
            "from atlas3.atlas_x_temas at,atlas3.subtema s,atlas3.subtema_x_item si,atlas3.sabias_que sq \n" +
            "where \n" +
            "at.id_atlas=:id and\n" +
            "at.id_tema=s.id_tema and\n" +
            "s.id=si.id_subtema and\n" +
            "sq.id_item=si.id_item", nativeQuery=true)
    Set<SabiasQue> findByAtlas(@Param("id") int id);

}
