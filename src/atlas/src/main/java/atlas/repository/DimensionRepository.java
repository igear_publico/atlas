package atlas.repository;

import atlas.model.Dimension;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DimensionRepository extends JpaRepository<Dimension,Integer> {

}
