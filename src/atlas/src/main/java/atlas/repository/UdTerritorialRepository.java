package atlas.repository;

import atlas.model.UdTemporal;
import atlas.model.UdTerritorial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UdTerritorialRepository extends JpaRepository<UdTerritorial,Integer> {

}
