package atlas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.SubtemaXItem;

public interface SubtemaXItemRepository extends JpaRepository<SubtemaXItem,Integer> {


}
