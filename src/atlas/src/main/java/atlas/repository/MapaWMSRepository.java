package atlas.repository;

import atlas.model.EstiloMapa;
import atlas.model.MapaWMS;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MapaWMSRepository extends JpaRepository<MapaWMS,Integer> {

}
