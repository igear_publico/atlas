package atlas.repository;

import atlas.model.Fuente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FuenteRepository extends JpaRepository<Fuente,Integer> {
}
