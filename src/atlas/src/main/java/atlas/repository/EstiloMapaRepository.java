package atlas.repository;

import atlas.model.Dimension;
import atlas.model.EstiloMapa;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EstiloMapaRepository extends JpaRepository<EstiloMapa,Integer> {

}
