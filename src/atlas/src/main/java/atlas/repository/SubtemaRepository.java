package atlas.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import atlas.model.Subtema;
import atlas.model.Tema;

public interface SubtemaRepository extends JpaRepository<Subtema,Integer> {
    Subtema findById(int id);

    List<Subtema> findByTemaNot(Tema tema);

    @Query(value="select id,titulo,tema from atlas3.v_subtemas_listado order by titulo", nativeQuery=true)
    List<Object[]> findAllListado();
    
    @Query(value="select s.id,s.titulo,i.id,i.titulo from atlas3.subtema s, atlas3.subtema_x_item si, atlas3.item i\n" +
            "where s.id=si.id_subtema AND i.id=si.id_item", nativeQuery=true)
    List<Object[]> findAllBasic();

    @Query(value="select t.* from atlas3.subtema t inner join atlas3.v_subtema_uri v on v.id_subtema=t.id where atlas=:atlas and tema=:tema and etiqueta=:etiqueta", nativeQuery=true)
    List<Subtema> findList(@Param("atlas") String atlas,@Param("tema") String tema,@Param("etiqueta") String etiqueta);


    @Transactional
    void deleteById(int id);
}
