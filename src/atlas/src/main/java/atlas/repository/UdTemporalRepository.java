package atlas.repository;

import atlas.model.Atlas;
import atlas.model.UdTemporal;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface UdTemporalRepository extends JpaRepository<UdTemporal,Integer> {

}
