package atlas.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import atlas.model.Tema;

public interface TemaRepository extends JpaRepository<Tema,Integer> {
    Tema findById(int id);

    @Query(value="select t.* from atlas3.tema t inner join atlas3.v_tema_uri v on v.id_tema=t.id where atlas=:atlas and etiqueta=:etiqueta", nativeQuery=true)
    List<Tema> findList(@Param("atlas") String atlas,@Param("etiqueta") String etiqueta);
    
    @Transactional
    void deleteById(int id);
}
