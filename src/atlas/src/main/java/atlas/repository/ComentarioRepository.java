package atlas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.Comentario;
import atlas.model.Item;

public interface ComentarioRepository extends JpaRepository<Comentario,Integer> {

   
   Comentario findByItemAndTempo(Item item, String tempo);
}
