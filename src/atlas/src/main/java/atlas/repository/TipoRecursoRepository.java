package atlas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import atlas.model.TipoRecurso;

public interface TipoRecursoRepository extends JpaRepository<TipoRecurso,Integer> {

    List<TipoRecurso> findAll();
    
}
