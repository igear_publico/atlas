package atlas.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import atlas.model.Atlas;
import atlas.model.AtlasXTemas;
import atlas.model.Dato;
import atlas.model.DatoXDimensiones;
import atlas.model.DatoXEstiloMapa;
import atlas.model.DatoXMapaWMS;
import atlas.model.Fuente;
import atlas.model.Item;
import atlas.model.ItemConOrden;
import atlas.model.Recurso;
import atlas.model.SabiasQue;
import atlas.model.Subtema;
import atlas.model.SubtemaConTema;
import atlas.model.Tema;
import atlas.model.TemaConOrden;
import atlas.model.UdTemporal;
import atlas.model.UdTerritorial;
import atlas.repository.AtlasRepository;
import atlas.repository.ItemRepository;
import atlas.repository.ItemResumenRepository;
import atlas.repository.RecursoRepository;
import atlas.repository.SabiasQueRepository;
import atlas.repository.SubtemaRepository;
import atlas.repository.TemaRepository;
import atlas.repository.TipoRecursoRepository;
import atlas.repository.UdTemporalRepository;
import atlas.repository.UdTerritorialRepository;
import atlas.service.DominioDimension;
import atlas.service.ItemService;
import atlas.service.SabiasQueService;

@Controller
@ConditionalOnExpression("${admin:false}")
public class AdminController {

    private final static Logger log = LoggerFactory.getLogger(AdminController.class.getName());

    @Autowired
    AtlasRepository atlasRepository;

    @Autowired
    SabiasQueRepository sabiasQueRepository;

    @Autowired
    TipoRecursoRepository tipoRecursoRepository;
    
    @Autowired
    RecursoRepository recursoRepository;

    @Autowired
    SubtemaRepository subtemaRepository;

    @Autowired
    TemaRepository temaRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ItemResumenRepository itemResumenRepository;
    
    @Autowired
    UdTemporalRepository udTemporalRepository;

    @Autowired
    UdTerritorialRepository udTerritorialRepository;

    @Autowired
    SabiasQueService sabiasQueService;
    
    @Autowired
    ItemService itemService;

	@Resource
	private Environment env;
	
    @RequestMapping(value="/admin/login")
    public String login(Model m){

      
        m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        m.addAttribute("admin",true);
          return "login";
    }
    
    @RequestMapping(value="/admin")
    public String index(Model m){

        HashMap<Integer,Set<SabiasQue>> sabiasQueHashMap = new HashMap<>();

        List<Atlas> atlas = atlasRepository.findAll();

        for(Atlas a: atlas){
            sabiasQueHashMap.put(a.getId(),sabiasQueRepository.findByAtlas(a.getId()));
        }
        m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",true);
        m.addAttribute("atlas",atlasRepository.findAll());
      //  List<ItemResumen> items = itemResumenRepository.findAll();
       // System.err.println("id "+items.get(0).getId());
        //System.err.println("titulo "+items.get(0).getTitulo());
        m.addAttribute("items",itemResumenRepository.findAll());
        m.addAttribute("temas",temaRepository.findAll());
        m.addAttribute("subtemas",subtemaRepository.findAll());
        m.addAttribute("sabiasQueHashMap",sabiasQueHashMap);
        return "admin";
    }
    @RequestMapping(value="/admin/items")
    public String indexItems(Model m){
    	index(m);
    	m.addAttribute("activeTab","tabItems");
        return "admin";
    }
    @RequestMapping(value="/admin/temas")
    public String indexTemas(Model m){
    	index(m);
    	m.addAttribute("activeTab","tabTemas");
        return "admin";
    }
    @RequestMapping(value="/admin/subtemas")
    public String indexSubtemas(Model m){
    	indexTemas(m);
    
    	m.addAttribute("flipSwitch","Subtemas");
        return "admin";
    }
    @RequestMapping(value="/admin/atlas/nuevo")
    public String nuevoAtlas(Model m){
        m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        List<Tema> temas = new ArrayList<>();
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",true);
        m.addAttribute("sabiasQue",null);
        m.addAttribute("atlas",new Atlas());
        m.addAttribute("temas",temas);
        return "adminAtlas";
    }

    @GetMapping("/admin/atlas/{name}")
    public String getAtlas(Model m, @PathVariable String name){

        Atlas atlas  = atlasRepository.findByEtiqueta(name);

        if(atlas == null){
            atlas  = atlasRepository.findByEtiqueta("Aragon");
        }

        m.addAttribute("contextPath",env.getProperty("server.contextPath"));

        ArrayList<TemaConOrden> temasOrdenados = atlas.getTemasConOrden();
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",false);
        m.addAttribute("sabiasQue",sabiasQueService.getSabiasQueAleatorio(atlas.getId()));
        m.addAttribute("atlas",atlas);
        m.addAttribute("temas",temasOrdenados);
        return "adminAtlas";
    }



    @RequestMapping(value="/admin/tema/nuevo")
    public String nuevoTema(Model m){
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",true);
        m.addAttribute("tema",new Tema());
        m.addAttribute("atlas",new Atlas());
        return "adminTema";
    }

    @RequestMapping(value="/admin/item/nuevo")
    public String nuevoItem(Model m){
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        Item item = new Item();
        item.setDatos(new ArrayList<Dato>());
        item.setFuente(new Fuente());

        m.addAttribute("tipos_recurso",tipoRecursoRepository.findAll());
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",true);
        m.addAttribute("item",item);
        m.addAttribute("atlas",new Atlas());

        m.addAttribute("uds_terr",new ArrayList<UdTerritorial>());
        m.addAttribute("uds_temp",new ArrayList<DominioDimension>());
        m.addAttribute("dimensiones", new ArrayList<DominioDimension>());
        m.addAttribute("all_ud_temp",udTemporalRepository.findAll());
        m.addAttribute("all_ud_terr",udTerritorialRepository.findAll());
        return "adminItem";
    }



    @GetMapping("/admin/tema/{id}")
    public String getTema(Model m, @PathVariable int id){
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        Tema tema  = temaRepository.findById(id);
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",false);
        m.addAttribute("subtemas",tema.getSubtemasInfo());
        
        Atlas atlas = new Atlas();
        if (tema.getAtlas().size()>0){
        	atlas.setEtiqueta(((AtlasXTemas)tema.getAtlas().toArray()[0]).getAtlas().getEtiqueta());
        }
        tema.setSubtemas(null);
        tema.setAtlas(null);
        m.addAttribute("tema",tema);
        m.addAttribute("atlas",atlas);
        return "adminTema";
    }


    @RequestMapping(value="/admin/subtema/nuevo")
    public String nuevoSubtema(Model m){
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        List<Tema> temas = temaRepository.findAll();
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",true);
        m.addAttribute("subtema",new Subtema());
        m.addAttribute("atlas",new Atlas());
        m.addAttribute("temas",temas);
        m.addAttribute("tema",new Tema());
        return "adminSubtema";
    }

    @GetMapping("/admin/subtema/{id}")
    public String getSubtema(Model m, @PathVariable int id) throws Exception{
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        Subtema subtema  = subtemaRepository.findById(id);
        List<Tema> temas = temaRepository.findAll();
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",false);
       
        ArrayList<ItemConOrden> itemsOrdenados = subtema.getItemsConOrden();
        m.addAttribute("items",itemsOrdenados);
        
        
        m.addAttribute("temas",temas);
        Atlas atlas = new Atlas();
        if(subtema.getTema()!=null){
        	Tema tema = subtema.getTema();
            m.addAttribute("tema",tema);
            if (tema.getAtlas().size()>0){
            	atlas.setEtiqueta(((AtlasXTemas)tema.getAtlas().toArray()[0]).getAtlas().getEtiqueta());
            }
            
        }else{
            m.addAttribute("tema",new Tema());
        }
        m.addAttribute("atlas",atlas);
        subtema.setItems(null);
        subtema.setTema(null);
        m.addAttribute("subtema",subtema);
        return "adminSubtema";
    }
    @GetMapping("/admin/item/copia/{id}")
    public String getCopiaItem(Model m, @PathVariable int id){
    	String resultado = getItem(m,id);
    	 m.addAttribute("nuevo",true);
    	 Map<String,Object> attrs = m.asMap();
    	 Item item = (Item)attrs.get("item");
    	 item.setId(0);
    	 item.setSubtemas(null);
    	 m.addAttribute("subtemas",null);
    	 Set<Recurso> recursos = item.getRecursos();
    	 for (Recurso recurso:recursos){
    		 recurso.setId(0);
    	 }
    	 List<Dato> datos = item.getDatos();
    	 for (Dato dato:datos){
    		 dato.setId(0);
    		 List<DatoXEstiloMapa> estilos = dato.getEstilos();
    		 for (DatoXEstiloMapa estilo:estilos){
    			 estilo.setId(0);
    		 }
    		 List<DatoXMapaWMS> mapas = dato.getMapas_wms();
    		 for (DatoXMapaWMS wms:mapas){
    			 wms.setId(0);
    		 }
    		 List<DatoXDimensiones> dims =  dato.getDimensiones();
    		 for (DatoXDimensiones dim:dims){
    			 dim.setId(0);
    		 }
    	 }
    	 
    	 for (Recurso recurso:recursos){
    		 recurso.setId(0);
    	 }
    	 m.addAttribute("item",item);
    	 return resultado;
    }
    @GetMapping("/admin/item/{id}")
    public String getItem(Model m, @PathVariable int id){
    	  m.addAttribute("contextPath",env.getProperty("server.contextPath"));
    	 
       Item item  = itemRepository.findById(id);

       Atlas atlas = new Atlas();
       Set<Subtema> subtemasList = item.getSubtemas();
       for (Subtema subtema:subtemasList){
    	   
    	   if(subtema.getTema()!=null){
           		Tema tema = subtema.getTema();
           		
               if (tema.getAtlas().size()>0){
            	  
            	   atlas.setEtiqueta(((AtlasXTemas)tema.getAtlas().toArray()[0]).getAtlas().getEtiqueta());
               		m.addAttribute("etiqueta_subtema",subtema.getEtiqueta());
               		m.addAttribute("etiqueta_tema",tema.getEtiqueta());
               		break;
               }
               
           }
       }

       m.addAttribute("atlas",atlas);
       ArrayList<SubtemaConTema> subtemas = item.getSubtemasConTema();
       m.addAttribute("subtemas",subtemas);
       m.addAttribute("tipos_recurso",tipoRecursoRepository.findAll());
        if(item.getFuente() == null){
            item.setFuente(new Fuente());
        }
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",false);
        for (Subtema subtema:item.getSubtemas()){
        	subtema.setItems(null);
        	
        }
     m.addAttribute("item",item);
        m.addAttribute("all_ud_temp",udTemporalRepository.findAll());
        m.addAttribute("all_ud_terr",udTerritorialRepository.findAll());
       
        return "adminItem";
    }


    @PostMapping("/admin/temaFragment")
    public String getTemaFragment(Model m,@RequestBody Tema tema){
        /**Set<Subtema> subtemas = new HashSet<Subtema>();
        for(Subtema sub :tema.getSubtemas()){
            subtemas.add(subtemaRepository.findById(sub.getId()));
        }
        tema.setSubtemas(subtemas);*/
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        m.addAttribute("admin",true);
        m.addAttribute("nuevo",false);
        m.addAttribute("tema",tema);
        m.addAttribute("atlas",new Atlas());
        return "temaFragment";
    }


    @PostMapping("/admin/itemFragment")
    public String getItemFragment(Model m,@RequestBody Item item){
    	   m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        m.addAttribute("item",item);
        // COMENTARIO
        Set<UdTemporal> uts = item.getUds_temporales();
        ArrayList<DominioDimension> dominios = new ArrayList<DominioDimension>();
        HashMap<Integer, DominioDimension> uds_temp_coment= new  HashMap<Integer, DominioDimension>();
        DominioDimension dd=null;
        for (UdTemporal ud: uts){
            try{
                dd = itemService.getDominioDimension(item, ud);
                dominios.add(dd);
                uds_temp_coment.put(ud.getId(), dd);
            }
            catch(SQLException ex){
                ex.printStackTrace();
            }
        }
        m.addAttribute("uds_comentario",dominios);
        m.addAttribute("comentario",itemService.getComentario(item,dd));

        // MAPA
        List<Dato> datos = item.getDatos();
        m.addAttribute("datos",datos);
        ArrayList<UdTerritorial> uds_terr = new ArrayList<UdTerritorial>();
        ArrayList<DominioDimension> dominiosMapa = new ArrayList<DominioDimension>();
        ArrayList<DominioDimension> dimensionesMapa = new ArrayList<DominioDimension>();
        ArrayList<Integer> uds_t= new ArrayList<Integer>();
        for (Dato dato:datos){
            if (dato.getUd_territorial()!=null){
                if (uds_terr.indexOf(dato.getUd_territorial())<0){
                    uds_terr.add(dato.getUd_territorial());
                }
            }
            if (dato.getUd_temporal()!=null){
                try{
                    if (uds_t.indexOf(dato.getUd_temporal().getId())<0){
                        if (uds_temp_coment.get(dato.getUd_temporal().getId())==null){

                            uds_temp_coment.put(dato.getUd_temporal().getId(), itemService.getDominioDimension(item, dato.getUd_temporal()));
                        }
                        dominiosMapa.add(uds_temp_coment.get(dato.getUd_temporal().getId()));
                        uds_t.add(dato.getUd_temporal().getId());
                    }
                }
                catch(SQLException ex){
                    ex.printStackTrace();
                }
            }
            List<DatoXDimensiones> dims = dato.getDimensiones();

            for (DatoXDimensiones dim:dims){
                try{
                    dimensionesMapa.add(itemService.getDominioDimension(item,dim.getDimension(), dim.getOrden(), dim.isExcluir_en_tabla_no_norm()));
                }
                catch(SQLException ex){
                    ex.printStackTrace();
                }
            }
        }
        m.addAttribute("uds_terr",uds_terr);
        m.addAttribute("uds_temp",dominiosMapa);

        m.addAttribute("dimensiones",dimensionesMapa);


        if(datos.size()>0){
            Dato dato = datos.get(0);
            UdTerritorial ud_terr= dato.getUd_territorial();
            m.addAttribute("selectedUdTerr",(ud_terr!= null ? ud_terr.getId() : 0 ));
            UdTemporal ud_temp= dato.getUd_temporal();
            m.addAttribute("selectedUdTemp",(ud_temp!= null ? ud_temp.getId() : 0 ));
            m.addAttribute("dato",dato);
        }

        m.addAttribute("descargas",recursoRepository.findByItemAndTipo(item, 1));
        m.addAttribute("enlaces",recursoRepository.findByItemAndTipo(item, 2));
        m.addAttribute("videos",recursoRepository.findByItemAndTipo(item,3));

        return "itemFragment";
    }
}
