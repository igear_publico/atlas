package atlas.controller;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import atlas.model.Atlas;
import atlas.model.Dato;
import atlas.model.DatoXDimensiones;
import atlas.model.Item;
import atlas.model.Recurso;
import atlas.model.SabiasQue;
import atlas.model.Subtema;
import atlas.model.Tema;
import atlas.model.UdTemporal;
import atlas.model.UdTerritorial;
import atlas.repository.AtlasRepository;
import atlas.repository.ItemRepository;
import atlas.repository.RecursoRepository;
import atlas.repository.SabiasQueRepository;
import atlas.repository.SubtemaRepository;
import atlas.repository.TemaRepository;
import atlas.service.DominioDimension;
import atlas.service.ItemService;
import atlas.service.SabiasQueService;
import atlas.service.SolrService;

@Controller
public class AtlasController {

    public final static Logger log = LoggerFactory.getLogger(AtlasController.class.getName());

    @Autowired
    AtlasRepository atlasRepository;

    @Autowired
    SabiasQueRepository sabiasQueRepository;

    @Autowired
    TemaRepository temaRepository;

    
    @Autowired
    SubtemaRepository subtemaRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    RecursoRepository recursoRepository;
    
    @Autowired
    SabiasQueService sabiasQueService;

	@Autowired
	SolrService solrService;


	@Autowired
    ItemService itemService;


	@Value("${needEncoding}")
	String needEncoding;

	@Resource
	private Environment env;


    @RequestMapping(value={ "/","/{name}","/{name}/info"})
    public String index(Model m, @PathVariable(required = false) String name){
		Atlas atlas = null;
		if(name != null){
			atlas  = atlasRepository.findByEtiqueta(name);
		}

		if(atlas == null){
			atlas  = atlasRepository.findByEtiqueta("Aragon");
		}
		m.addAttribute("atlas",atlas);
		m.addAttribute("contextPath",env.getProperty("server.contextPath"));
    	try{
			ArrayList<Tema> temasOrdenados = atlas.getTemasOrdenados();

			SabiasQue sabiasque=sabiasQueService.getSabiasQueAleatorio(atlas.getId());
			m.addAttribute("sabiasQue",sabiasque);
			try{
				m.addAttribute("subtema_item_sq",((Subtema)sabiasque.getId_item().getSubtemas().toArray()[0]).getEtiqueta());
				m.addAttribute("tema_item_sq",((Subtema)sabiasque.getId_item().getSubtemas().toArray()[0]).getTema().getEtiqueta());
			}
			catch(Exception ex){
				log.warn("El item no tiene subtema");
			}

			m.addAttribute("temas",temasOrdenados);
			return "index";
		}catch (Exception e){
			log.error("Error obteniendo atlas", e);
    		return "500";
		}
    }

    @RequestMapping(value={"/{atlas_name}/info/{etiq_tema}/{etiq_subtema}/{etiq_item}"})
    public String item(Model m, @PathVariable String atlas_name,@PathVariable(required = false) String etiq_tema,@PathVariable(required = false) String etiq_subtema, @PathVariable String etiq_item){
    	 Atlas atlas  = atlasRepository.findByEtiqueta(atlas_name);
         if(atlas == null){
             atlas  = atlasRepository.findByEtiqueta("Aragon");
         }
         m.addAttribute("atlas",atlas);
         m.addAttribute("contextPath",env.getProperty("server.contextPath"));
         try{

			 List<Subtema> subtemas = subtemaRepository.findList(atlas_name,etiq_tema,etiq_subtema);
			 if (subtemas.size()>0){
				 Subtema subtema =subtemas.get(0);
				 m.addAttribute("subtema",subtema);
			 }

			 List<Item> items =itemRepository.findList(atlas_name,etiq_tema,etiq_subtema,etiq_item);

			 if(items.size()==0){
				 return "404";
			 }
			 Item item = items.get(0);
			 m.addAttribute("item",item);
			 // COMENTARIO
			 Set<UdTemporal> uts = item.getUds_temporales();
			 HashMap<Integer, DominioDimension> uds_temp_coment= new  HashMap<Integer, DominioDimension>();
			 ArrayList<DominioDimension> dominios =  itemService.getDominoTemporalComentario(item,uds_temp_coment);
			 DominioDimension dd=null;
			 if (dominios.size()>0){
				 dd = dominios.get(dominios.size()-1);
			 }
			 m.addAttribute("uds_comentario",dominios);
			 m.addAttribute("comentario",itemService.getComentario(item,dd));

			 // MAPA
			 List<Dato> datos = item.getDatos();
			 m.addAttribute("datos",datos);
			 ArrayList<UdTerritorial> uds_terr = new ArrayList<UdTerritorial>();
			 ArrayList<DominioDimension> dominiosMapa = new ArrayList<DominioDimension>();
			 ArrayList<DominioDimension> dimensionesMapa = new ArrayList<DominioDimension>();
			 ArrayList<Integer> uds_t= new ArrayList<Integer>();
			 for (Dato dato:datos){
				 if (dato.getUd_territorial()!=null){
					 if (uds_terr.indexOf(dato.getUd_territorial())<0){
						 uds_terr.add(dato.getUd_territorial());
					 }
				 }
				 if (dato.getUd_temporal()!=null){
					 try{
						 if (uds_t.indexOf(dato.getUd_temporal().getId())<0){
							 if (uds_temp_coment.get(dato.getUd_temporal().getId())==null){

								 uds_temp_coment.put(dato.getUd_temporal().getId(), itemService.getDominioDimension(item, dato.getUd_temporal()));
							 }
							 dominiosMapa.add(uds_temp_coment.get(dato.getUd_temporal().getId()));
							 uds_t.add(dato.getUd_temporal().getId());
						 }
					 }
					 catch(SQLException ex){
						 ex.printStackTrace();
					 }
				 }
				 List<DatoXDimensiones> dims = dato.getDimensiones();
				 log.info("Num dims:"+dims.size());
				 for (DatoXDimensiones dim:dims){
					 log.info("Dim:"+dim.getOrden());
					 try{
						 dimensionesMapa.add(itemService.getDominioDimension(item,dim.getDimension(), dim.getOrden(), dim.isExcluir_en_tabla_no_norm()));
					 }
					 catch(SQLException ex){
						 log.error("Error obteniendo dimensiones",ex);
					 }
				 }
			 }

			 m.addAttribute("uds_terr",uds_terr);
			 m.addAttribute("uds_temp",dominiosMapa);
			 m.addAttribute("dimensiones",dimensionesMapa);
			 if (datos.size()>0){
				 Dato dato = datos.get(0);
				 UdTerritorial ud_terr= dato.getUd_territorial();
				 m.addAttribute("selectedUdTerr",(ud_terr!= null ? ud_terr.getId() : 0 ));
				 UdTemporal ud_temp= dato.getUd_temporal();
				 m.addAttribute("selectedUdTemp",(ud_temp!= null ? ud_temp.getId() : 0 ));

				 m.addAttribute("dato",dato);
			 }


			 List<Recurso> videos =new ArrayList<Recurso>();


			 for(Recurso r: item.getRecursos()){
				 if(r.getTipo().getId() == 3){
					 videos.add(r);
				 }
			 }
			 m.addAttribute("recursos",item.getRecursos());
			 m.addAttribute("videos",videos);

			 return "item";

		 }catch (Exception e){
			log.error("Error obteniendo item", e);
         	return "500";
		 }
    }


    @RequestMapping(value={"/{atlas_name}/info/{etiq_tema:(?!informacion-por-zona-basica-de-salud).*$}","/{atlas_name}/info/{etiq_tema:(?!informacion-por-zona-basica-de-salud).*$}/{etiq_subtema}"})
    public String tema(Model m, @PathVariable String atlas_name, @PathVariable String etiq_tema,@PathVariable(required = false) String etiq_subtema){
        Atlas atlas  = atlasRepository.findByEtiqueta(atlas_name);
        if(atlas == null){
            atlas  = atlasRepository.findByEtiqueta("Aragon");
        }
        m.addAttribute("atlas",atlas);
        m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        try{
			Subtema subtema=null;
			if (etiq_subtema!=null){
				List<Subtema> subtemas = subtemaRepository.findList(atlas_name,etiq_tema,etiq_subtema);
				if (subtemas.size()>0){
					subtema =subtemas.get(0);
					m.addAttribute("subtema_id",subtema.getId());
				}
			}
			Tema tema;
			if (subtema!=null){
				tema = subtema.getTema();
			}
			else{
				List<Tema> temas = temaRepository.findList(atlas_name,etiq_tema);
				if(temas.size()>0){
					tema = temas.get(0);
				}else{
					return "404";
				}
			}
			m.addAttribute("tema",tema);
			return "tema";
		}catch (Exception e){
			log.error("Error obteniendo tema", e);
        	return "500";
		}

    }
    @RequestMapping(value={"/Salud/info/informacion-por-zona-basica-de-salud"})
    public String tablaZBS(Model m){
    	String atlas_name="Salud";
    	String etiq_tema="informacion-por-zona-basica-de-salud";
        Atlas atlas  = atlasRepository.findByEtiqueta(atlas_name);
       
        m.addAttribute("atlas",atlas);
        m.addAttribute("contextPath",env.getProperty("server.contextPath"));
        try{
			Tema tema;
			List<Tema> temas = temaRepository.findList(atlas_name,etiq_tema);
			if(temas.size()>0){
				tema = temas.get(0);
			}else{
				return "404";
			}

			m.addAttribute("tema",tema);
			return "tablaZBS";
		}catch (Exception e){
			log.error("Error obteniendo tabla ZBS", e);
        	return "500";
		}
       

    }
	@RequestMapping(value="/{atlas_name}/busqueda")
	public String busquedaEnListado(Model m, @PathVariable String atlas_name, @RequestParam String q, @RequestParam int pag){
		if(needEncoding.equals("true")){
			try{
				q = new String(q.getBytes("ISO-8859-1"), "UTF-8");
			}catch (Exception e){}
		}
		log.info("busquedaEnListado: "+q);
		m.addAttribute("query",q);
		m.addAttribute("contextPath",env.getProperty("server.contextPath"));
    	try{
			Atlas atlas  = atlasRepository.findByEtiqueta(atlas_name);
			if(atlas == null){
				atlas  = atlasRepository.findByEtiqueta("Aragon");
			}
			m.addAttribute("atlas",atlas);

			String res =  solrService.searchItem(atlas_name,q, pag-1);
			JsonObject response = new JsonParser().parse(res).getAsJsonObject();
			JsonArray docs = response.getAsJsonObject("response").getAsJsonArray("docs");

			List<Item> items = new ArrayList<Item>();
			log.info("Procesando resultados");
			for(int i = 0;i<docs.size();i++){
				JsonObject object = docs.get(i).getAsJsonObject();
				if(object.get("clase").getAsString().equals("item")){
					Item item = new Item();
					item.setId(object.get("id_en_atlas").getAsInt());
					String comentario;
					if(object.get("comentario") == null){
						comentario = "";
					}else{
						comentario = object.get("comentario").getAsString().replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");

					}
					if(comentario.length()>250){
						item.setComentario(comentario.substring(0,250) +"...");
					}else{
						item.setComentario(comentario);
					}
					item.setTitulo(object.get("titulo_txt").getAsString());
					item.setEtiqueta(object.get("etiqueta").getAsString());

					JsonArray subtemas = object.getAsJsonArray("subtemas_txt");
					JsonArray temas = object.getAsJsonArray("temas_txt");
					JsonArray etiq_subtemas = object.getAsJsonArray("etiq_subtemas");
					JsonArray etiq_temas = object.getAsJsonArray("etiq_temas");

					Set<Subtema> set = new HashSet<>();

					for(int j = 0;j<subtemas.size();j++ ){
						Subtema s = new Subtema();
						s.setTitulo(subtemas.get(j).getAsString());
						s.setEtiqueta(etiq_subtemas.get(j).getAsString());

						Tema t = new Tema();
						if(j<temas.size()){
							t.setNombre(temas.get(j).getAsString());
							t.setEtiqueta(etiq_temas.get(j).getAsString());
						}else{
							t.setNombre(temas.get(j-1).getAsString());
							t.setEtiqueta(etiq_temas.get(j-1).getAsString());
						}

						s.setTema(t);
						set.add(s);
					}
					item.setSubtemas(set);
					items.add(item);
				}
			}

			m.addAttribute("items",items);
			m.addAttribute("paginaActual",pag);
			int total = response.getAsJsonObject("response").get("numFound").getAsInt();
			log.info("Procesamiento finalizado: "+ total);
			m.addAttribute("total",total);
			int paginasTotales = (int) Math.ceil((double)total / 10);
			List<Integer> paginasAnteriores = new ArrayList<>();

			for(int i = 1; i<4; i++){
				if((pag-i)>0){
					paginasAnteriores.add(0,(pag-i));
				}
			}
			List<Integer> paginasPosteriores = new ArrayList<>();
			for(int i = 1; i<4; i++){
				if((pag+i)<=paginasTotales){
					paginasPosteriores.add((pag+i));
				}
			}
			m.addAttribute("paginasAnteriores",paginasAnteriores);
			m.addAttribute("paginasPosteriores",paginasPosteriores);
		}catch (Exception e){
    		log.info("Error");
    		logException(e);
			e.printStackTrace();
			m.addAttribute("total",0);
			m.addAttribute("items",new ArrayList<>());
		}
		return "busqueda";
	}


	public static void logException(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();

		log.error(sStackTrace);
	}


}
