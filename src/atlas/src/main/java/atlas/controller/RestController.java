package atlas.controller;

import static org.springframework.http.HttpStatus.OK;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import atlas.model.Dato;
import atlas.model.Item;
import atlas.repository.DatoRepository;
import atlas.repository.ItemRepository;
import atlas.service.DatoService;
import atlas.service.ItemService;
import atlas.service.SolrService;
import atlas.service.ZBSService;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Value("${logging.path}")
    String logpath;

    @Autowired
    ItemRepository itemRepository;
    
    @Autowired
    ItemService itemService;
    
    @Autowired
    DatoRepository datoRepository;
    
    @Autowired
    DatoService datoService;

    @Autowired
    ZBSService zbsService;
    
    @Autowired
    SolrService solrService;
    
    @RequestMapping(value = "/logs", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity logs() throws Exception {
        byte[] encoded = Files.readAllBytes(Paths.get(logpath+"spring.log"));
        String s = new String(encoded, StandardCharsets.US_ASCII);
        return ResponseEntity.status(OK).body(s);
    }
    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity test() throws Exception {
        String s = "OK";
        return ResponseEntity.status(OK).body(s);
    }
    
    @RequestMapping(value = {"/{atlas_name}/info/{tema}/{subtema}/{item_id}/comentario/{tempo}","/{atlas_name}/info/{tema}/{subtema}/item/{item_id}/comentario/{tempo}"}, method = RequestMethod.GET, produces = "text/html")
    public ResponseEntity getComentario(@PathVariable String item_id, @PathVariable String tempo) throws Exception {

    	 Item item = itemRepository.findById(new Integer(item_id));
    	String res =  itemService.getComentario(item, tempo);
        return ResponseEntity.status(OK).body(res);
    }

    @RequestMapping(value = {"/{atlas_name}/info/dato/{dato_id}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}",
    		"/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}/{dim2}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}/{dim2}",
    		"/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}/{dim2}/{dim3}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}/{dim2}/{dim3}","/{atlas_name}/info/{tema}/{subtema}/dato/{dato_id}/{tempo}/{dim1}/{dim2}/{dim3}/{dim4}","/{atlas_name}/{subtema_id}/item/dato/{dato_id}/{tempo}/{dim1}/{dim2}/{dim3}/{dim4}"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getValues(@PathVariable String dato_id, @PathVariable(required = false) String tempo, @PathVariable(required = false) String dim1,
    		 @PathVariable(required = false) String dim2, @PathVariable(required = false) String dim3, @PathVariable(required = false) String dim4) throws Exception {

    	Dato dato = datoRepository.findById(new Integer(dato_id));
    	if ((dato.getTabla_datos()==null) && (dato.getTabla_no_normalizada()==null)){
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No hay tabla de datos asociada al dato");
    	}
    	String res =  datoService.getValores(dato, (tempo!=null && tempo.trim().length()==0 ? null:tempo),dim1,dim2,dim3,dim4);
        return ResponseEntity.status(OK).body(res);
    }
    
    @RequestMapping(value = {"/Salud/info/zbs/{zbs_id}/{sexo_id}"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getInfoZBS(@PathVariable String zbs_id,@PathVariable  String sexo_id) throws Exception {

    
    	String res =  zbsService.getInfo(zbs_id, sexo_id);
        return ResponseEntity.status(OK).body(res);
    }

    @RequestMapping(value = {"/{atlas_name}/buscar"},params={"q"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity busquedaSolr(@PathVariable String atlas_name,@RequestParam(value = "q") String q ) throws Exception {

    	String res =  solrService.search(atlas_name,q);
        return ResponseEntity.status(OK).body(res);
    }

}
