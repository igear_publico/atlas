package atlas.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import atlas.Authentication;
import atlas.model.Atlas;
import atlas.model.AtlasXTemas;
import atlas.model.Dimension;
import atlas.model.EstiloMapa;
import atlas.model.Fuente;
import atlas.model.Item;
import atlas.model.MapaWMS;
import atlas.model.ProjectIdAndTitulo;
import atlas.model.Recurso;
import atlas.model.Subtema;
import atlas.model.SubtemaConTema;
import atlas.model.SubtemaXItem;
import atlas.model.Tema;
import atlas.model.UdTemporal;
import atlas.model.UdTerritorial;
import atlas.repository.AtlasRepository;
import atlas.repository.AtlasXTemasRepository;
import atlas.repository.DatoRepository;
import atlas.repository.DimensionRepository;
import atlas.repository.EstiloMapaRepository;
import atlas.repository.FuenteRepository;
import atlas.repository.ItemRepository;
import atlas.repository.MapaWMSRepository;
import atlas.repository.RecursoRepository;
import atlas.repository.SubtemaRepository;
import atlas.repository.SubtemaXItemRepository;
import atlas.repository.TemaRepository;
import atlas.repository.UdTemporalRepository;
import atlas.repository.UdTerritorialRepository;
import atlas.service.DatoService;
import atlas.service.ItemService;


@org.springframework.web.bind.annotation.RestController
@ConditionalOnExpression("${admin:false}")
public class AdminRestController {

    @Value("${logging.path}")
    String logpath;


    @Autowired
    ItemRepository itemRepository;

    @Autowired
    AtlasRepository atlasRepository;

    @Autowired
    AtlasXTemasRepository atlasXTemasRepository;
    
    @Autowired
    TemaRepository temaRepository;

    @Autowired
    EstiloMapaRepository estiloMapaRepository;

    @Autowired
    MapaWMSRepository mapaWMSRepository;

    @Autowired
    UdTerritorialRepository udTerritorialRepository;

    @Autowired
    UdTemporalRepository udTemporalRepository;

    @Autowired
    DimensionRepository dimensionRepository;

    @Autowired
    FuenteRepository fuenteRepository;

    @Autowired
    SubtemaRepository subtemaRepository;
    
    @Autowired
    SubtemaXItemRepository subtemaXItemRepository;

    @Autowired
    RecursoRepository recursoRepository;

    @Autowired
    ItemService itemService;

    @Autowired
    DatoRepository datoRepository;

    @Autowired
    DatoService datoService;
    
	@Resource
	private Environment env;

    private final static Logger log = LoggerFactory.getLogger(AdminRestController.class.getName());


    @RequestMapping(value = "/admin/atlas/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity removeAtlas(@PathVariable int id ,@RequestParam String token) {
    	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
    		atlasXTemasRepository.deleteByAtlas(atlasRepository.findById(id));
    		atlasRepository.deleteById(id);
    		return ResponseEntity.status(OK).body("{}");
    	
    }


    @RequestMapping(value = "/admin/tema/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity removeTema(@PathVariable int id ,@RequestParam String token) {
        log.info("Remove tema: "+id);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        temaRepository.deleteById(id);
        return ResponseEntity.status(OK).body("{}");
    }
    @RequestMapping(value = "/admin/subtema/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity removeSubtema(@PathVariable int id ,@RequestParam String token) {
        log.info("Remove subtema: "+id);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        subtemaRepository.deleteById(id);
        return ResponseEntity.status(OK).body("{}");
    }

    @RequestMapping(value = "/admin/item/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity removeItem(@PathVariable int id ,@RequestParam String token) {
        log.info("Remove item: "+id);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        itemRepository.deleteById(id);
        return ResponseEntity.status(OK).body("{}");
    }






    @RequestMapping(value = "/admin/atlas/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newAtlas(@RequestBody Atlas atlas ,@RequestParam String token) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9]");
        if(atlas.getEtiqueta()==null || atlas.getEtiqueta().equals("") || p.matcher(atlas.getEtiqueta()).find()){
            return ResponseEntity.status(BAD_REQUEST).body("{\"error\":\"Etiqueta inválida\" }");
        }else{
         	boolean autorizado = false;
        	try{
        		autorizado = Authentication.validateToken(env, token);
        	}
        	catch(Exception ex){
        		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
    		}
        	if(!autorizado){
        		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
        	}
        	Set<AtlasXTemas> temas = atlas.getTemas();
        	atlas.setTemas(null);
            Atlas saved = atlasRepository.save(atlas);
           	for(AtlasXTemas x: temas){
           		x.setId_atlas(saved.getId());
    		
            atlasXTemasRepository.save(x);
        }
            return ResponseEntity.status(OK).body("{\"id\":\""+saved.getEtiqueta()+"\"}");
        }
    }

    @RequestMapping(value = "/admin/tema/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newTema(@RequestBody Tema tema ,@RequestParam String token) {
        log.info("Create tema: "+tema);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        Tema saved = temaRepository.save(tema);
        return ResponseEntity.status(OK).body("{\"id\":"+saved.getId()+"}");
    }

    @RequestMapping(value = "/admin/subtema/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newSubtema(@RequestBody Subtema subtema ,@RequestParam String token) {
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
    	Set<SubtemaXItem> items = subtema.getItems();
    	subtema.setItems(null);
    	 Subtema saved = subtemaRepository.save(subtema);
       	for(SubtemaXItem x: items){
       		x.setId_subtema(saved.getId());
       	
       		subtemaXItemRepository.save(x);
       	}	
       
        return ResponseEntity.status(OK).body("{\"id\":"+saved.getId()+"}");
    }

    @RequestMapping(value = "/admin/fuente/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newFuente(@RequestBody Fuente fuente ,@RequestParam String token) {
        log.info("Create fuente: "+fuente);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        Fuente saved = fuenteRepository.save(fuente);
        return ResponseEntity.status(OK).body(saved.toString());
    }


    @RequestMapping(value = "/admin/dimension/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newDimension(@RequestBody Dimension dimension ,@RequestParam String token) {
        log.info("Create dimension: "+dimension);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        Dimension saved = dimensionRepository.save(dimension);
        return ResponseEntity.status(OK).body(saved.toString());
    }

    @RequestMapping(value = "/admin/estilo/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newEstilo(@RequestBody EstiloMapa estiloMapa ,@RequestParam String token) {
        log.info("Create estiloMapa: "+estiloMapa);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        EstiloMapa saved = estiloMapaRepository.save(estiloMapa);
        return ResponseEntity.status(OK).body(saved.toString());
    }

    @RequestMapping(value = "/admin/mapa/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newMapa(@RequestBody MapaWMS mapaWMS ,@RequestParam String token) {
        log.info("Create mapaWMS: "+mapaWMS);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        MapaWMS saved = mapaWMSRepository.save(mapaWMS);
        return ResponseEntity.status(OK).body(saved.toString());
    }


    @RequestMapping(value = "/admin/item/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newItem(@RequestBody Item item ,@RequestParam String token) {
        log.info("Create item: "+item);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
     /*   ArrayList<Dato> datos = new ArrayList<Dato>();
        System.err.println("datos"+ item.getDatos().size());
        for(Dato d: item.getDatos()){
        	System.err.println("dato");
        	 d.setItem(item);
             datos.add(d);
        } 
        item.setDatos(datos);*/
        if(item.getFuente().getTxt() == null){
            item.setFuente(null);
        }
        Set<Recurso> recursos = new HashSet<Recurso>();
        for(Recurso r: item.getRecursos()){
            r.setItem(item);
            recursos.add(r);
        }
        item.setRecursos(recursos);

        Item saved = itemRepository.save(item);
        return ResponseEntity.status(OK).body("{\"id\":"+saved.getId()+"}");
    }

    @RequestMapping(value = "/admin/udterritorial/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newUdTerritorial(@RequestBody UdTerritorial udTerritorial ,@RequestParam String token) {
        log.info("Create udTerritorial: "+udTerritorial);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        UdTerritorial saved = udTerritorialRepository.save(udTerritorial);
        return ResponseEntity.status(OK).body(saved.toString());
    }


    @RequestMapping(value = "/admin/udtemporal/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity newUdTemporal(@RequestBody UdTemporal udTemporal ,@RequestParam String token) {
        log.info("Create udTemporal: "+udTemporal);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        UdTemporal saved = udTemporalRepository.save(udTemporal);
        return ResponseEntity.status(OK).body(saved.toString());
    }



    @RequestMapping(value = "/admin/atlas/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity updateAtlas(@RequestBody Atlas atlas ,@RequestParam String token) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9]");
        if(atlas.getEtiqueta()==null || atlas.getEtiqueta().equals("") || p.matcher(atlas.getEtiqueta()).find()){
            return ResponseEntity.status(BAD_REQUEST).body("{}");
        }else{
         	boolean autorizado = false;
        	try{
        		autorizado = Authentication.validateToken(env, token);
        	}
        	catch(Exception ex){
        		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
    		}
        	if(!autorizado){
        		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
        	}
            
            Atlas saved = atlasRepository.save(atlas);
            return ResponseEntity.status(OK).body("{}");
        }
    }



    @RequestMapping(value = "/admin/tema/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity updateTema(@RequestBody Tema tema ,@RequestParam String token) {
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
     //   tema.setAtlas(tema.getAtlas());
    	tema.setAtlas(null);
        Tema saved = temaRepository.save(tema);
        return ResponseEntity.status(OK).body("{}");
    }

    @RequestMapping(value = "/admin/subtema/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity updateTema(@RequestBody Subtema subtema ,@RequestParam String token) {
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        Subtema saved = subtemaRepository.save(subtema);
        return ResponseEntity.status(OK).body("{}");
    }

    @RequestMapping(value = "/admin/item/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity updateItem(@RequestBody Item item ,@RequestParam String token) {
        log.info("Update item: "+item);
     	boolean autorizado = false;
    	try{
    		autorizado = Authentication.validateToken(env, token);
    	}
    	catch(Exception ex){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No se ha podido validar el token");
		}
    	if(!autorizado){
    		return ResponseEntity.status(UNAUTHORIZED).body("El token introducido no es válido");
    	}
        if(item.getFuente().getTxt()==null || item.getFuente().getTxt().equals("")){
            item.setFuente(null);
        }
        Set<Recurso> recursos = new HashSet<Recurso>();
        for(Recurso r: item.getRecursos()){
            r.setItem(item);
            recursos.add(r);
        }
        item.setRecursos(recursos);


        Item saved = itemRepository.save(item);
        return ResponseEntity.status(OK).body("{}");
    }



    @RequestMapping(value = "/admin/items/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllItems() {
        List<Item> items =new ArrayList<>();

        for(ProjectIdAndTitulo p: itemRepository.findAllBasic()){
            items.add(new Item(p));
        }
        String response = "{ "+
                "\"items\": " + items +
                "}";
        return ResponseEntity.status(OK).body(response);
    }

    @RequestMapping(value = "/admin/temas/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllTemas() {
        List<Tema> temas = temaRepository.findAll();
        String response = "{ "+
                "\"temas\": " + temas +
                "}";
        return ResponseEntity.status(OK).body(response);
    }
    @RequestMapping(value = "/admin/estilos", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllEstilos() {
        List<EstiloMapa> estilos = estiloMapaRepository.findAll();
        String response = "{ "+
                "\"estilos\": " + estilos +
                "}";
        return ResponseEntity.status(OK).body(response);
    }

    @RequestMapping(value = "/admin/mapas", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllMapas() {
        List<MapaWMS> mapas = mapaWMSRepository.findAll();
        String response = "{ "+
                "\"mapas\": " + mapas +
                "}";
        return ResponseEntity.status(OK).body(response);
    }

    @RequestMapping(value = "/admin/subtemas/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllSubtemas() {
        List<Object[]> listado = subtemaRepository.findAllListado();
        List<SubtemaConTema> subtemas =new ArrayList<>();

        for(Object[] p: listado){
        	 SubtemaConTema subtema = new SubtemaConTema();
        	    subtema.setId(new Integer(p[0].toString()));
        	    subtema.setTitulo(p[1].toString());
        	    if (p[2]!=null){
        	    	subtema.setNombre_tema(p[2].toString());
        	    }
        	    
            subtemas.add(subtema);
        }
   

        String response = "{ "+
                "\"subtemas\": " + subtemas +
                "}";
        return ResponseEntity.status(OK).body(response);
    }

    @RequestMapping(value = "/admin/fuentes", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllFuentes() {
        List<Fuente> fuentes = fuenteRepository.findAll();
        String response = "{ "+
                "\"fuentes\": " + fuentes +
                "}";
        return ResponseEntity.status(OK).body(response);
    }

    @RequestMapping(value = "/admin/dimension", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAllDimension() {
        List<Dimension> dimensiones = dimensionRepository.findAll();
        String response = "{ "+
                "\"dimensiones\": " + dimensiones +
                "}";
        return ResponseEntity.status(OK).body(response);
    }



    @RequestMapping(value = "/admin/estilo/{id}/clases", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getClasesEstilo(@PathVariable Integer id) {
        EstiloMapa estiloMapa = estiloMapaRepository.findOne(id);
        String response = "{ "+
                "\"clases\": " + estiloMapa.getClases() +
                "}";
        return ResponseEntity.status(OK).body(response);
    }


}
