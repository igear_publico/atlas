package atlas;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.bind.DatatypeConverter;

import org.apache.catalina.Manager;
import org.springframework.core.env.Environment;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;


public  class Authentication {

	static private String CLIENT_SECRET = "Y2FydG9VcGRhdGUyMDE2Ojs=!";
	static private String ROL_APP = "ROL_G_ATLAS";

	static public boolean validateToken (Environment env, String token) throws Exception{
	
		URL url = new URL(env.getProperty("AUTHENTICATION_URL")+token);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();
		int code = connection.getResponseCode();
	
		connection.disconnect();

		return hasAdminRol(token);


	}

	protected static Claims parseToken(String jwt) throws Exception {
		
			//This line will throw an exception if it is not a signed JWS (as expected)
			Claims claims = Jwts.parser()         
					.setSigningKey(DatatypeConverter.parseBase64Binary(CLIENT_SECRET))
					.parseClaimsJws(jwt).getBody();
			//System.out.println("ID: " + claims.getId());
			return claims;

		
	}
	public static String getUser(String jwt)  throws Exception{
		//This line will throw an exception if it is not a signed JWS (as expected)
		Claims claims = parseToken(jwt);
		return claims.getSubject();
	}

	protected static boolean hasAdminRol(String token) throws Exception{
		Claims claims = parseToken(token);
		Object resultado = claims.get(ROL_APP);
		return((resultado!=null)&&(resultado.toString().equalsIgnoreCase("true")));
			
	
	}
	

	
	
}
