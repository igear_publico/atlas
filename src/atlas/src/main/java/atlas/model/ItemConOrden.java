package atlas.model;

import com.google.gson.annotations.Expose;

public class ItemConOrden extends Item{
	@Expose
	int orden;

	@Expose
	int id_relation;
	
	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public int getId_relation() {
		return id_relation;
	}

	public void setId_relation(int id_relation) {
		this.id_relation = id_relation;
	}


	
	
}
