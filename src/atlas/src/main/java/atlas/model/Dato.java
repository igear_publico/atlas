package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.persistence.*;

@Entity
@Table(name = "dato", schema = "atlas3")
public class Dato {

	@Expose
	@Id
	@SequenceGenerator(name="dato_seq",
			sequenceName="atlas3.dato_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dato_seq")
	int id;

	@Expose
	    String tabla_datos; // tabla de datos para mapa vectorial. Si tabla_normalizada se usará también para pintar la tabla
	@Expose
	    String tabla_conex;
	@Expose
	    boolean tabla_normalizada;

	@Expose
	    String tabla_filtro;

	@Expose
    String tabla_no_normalizada;  // si tabla_normalizada=false se usará esta se pintará esta tabla
	
	@Expose
    String tabla_no_norm_filtro;
	
	@Expose
    String tabla_no_norm_campos; // campos a recuperar para construir la tabla (SELECT). 

	@Expose
    String tabla_no_norm_campos_sort; // campos a utilizar para ordenar cada una de las columnas de tabla_nor_norm_columnas. OPCIONAL, solo si son diferentes a los que se muestran. Deben estar incluídos en el tabla_no_norm_campos 

	@Expose
    String tabla_no_norm_columnas; // columnas a incluir en la tabla y en el orden requerido (separados por comas). Deben estar incluídos en el tabla_no_norm_campos.

	@Expose
	    boolean mapa_vectorial;

	@Expose
	    int mapa_vectorial_orden;

	@Expose
		boolean mapa_wms;

	@Expose
	boolean grafico;

	@Expose
    String ud_medida;

	    @OneToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE}, orphanRemoval = true)
		@JoinColumn(name = "id_dato")
		List<DatoXEstiloMapa> estilos;

		@Expose
	    @ManyToOne (cascade = {CascadeType.MERGE})
	    @JoinColumn(name="id_ud_territorial")
	    UdTerritorial ud_territorial;

		@Expose
		@ManyToOne (cascade = {CascadeType.MERGE})
	    @JoinColumn(name="id_ud_temporal")
	    UdTemporal ud_temporal;

	    @OneToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE}, orphanRemoval = true)
	    @JoinColumn(name = "id_dato")
	    List<DatoXMapaWMS> mapas_wms;
	    
	    @OneToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE}, orphanRemoval = true)
	    @JoinColumn(name = "id_dato")
	    List<DatoXDimensiones> dimensiones;
	  
/*	    @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "id_item")
	    Item item;*/
	    public Dato() {
			super();
		}
	    
	    public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getTabla_datos() {
			return tabla_datos;
		}

		public void setTabla_datos(String tabla_datos) {
			this.tabla_datos = tabla_datos;
		}

		public String getTabla_conex() {
			return tabla_conex;
		}

		public void setTabla_conex(String tabla_conex) {
			this.tabla_conex = tabla_conex;
		}

		public boolean isTabla_normalizada() {
			return tabla_normalizada;
		}

		public void setTabla_normalizada(boolean tabla_normalizada) {
			this.tabla_normalizada = tabla_normalizada;
		}

		public String getTabla_filtro() {
			return tabla_filtro;
		}

		public void setTabla_filtro(String tabla_filtro) {
			this.tabla_filtro = tabla_filtro;
		}

		public boolean isMapa_vectorial() {
			return mapa_vectorial;
		}

		public void setMapa_vectorial(boolean mapa_vectorial) {
			this.mapa_vectorial = mapa_vectorial;
		}

		public int getMapa_vectorial_orden() {
			return mapa_vectorial_orden;
		}

		public void setMapa_vectorial_orden(int mapa_vectorial_orden) {
			this.mapa_vectorial_orden = mapa_vectorial_orden;
		}

		public boolean isMapa_wms() {
			return mapa_wms;
		}

		public void setMapa_wms(boolean mapa_wms) {
			this.mapa_wms = mapa_wms;
		}

		public UdTerritorial getUd_territorial() {
			return ud_territorial;
		}

		public void setUd_territorial(UdTerritorial ud_territorial) {
			this.ud_territorial = ud_territorial;
		}

		public UdTemporal getUd_temporal() {
			return ud_temporal;
		}

		public void setUd_temporal(UdTemporal ud_temporal) {
			this.ud_temporal = ud_temporal;
		}

		public List<DatoXMapaWMS> getMapas_wms() {

			TreeSet tree = new TreeSet(mapas_wms);

					
					List<DatoXMapaWMS> result =new ArrayList(new TreeSet(mapas_wms));

			return result;
		}

		public void setMapas_wms(List<DatoXMapaWMS> mapas_wms) {
			this.mapas_wms = mapas_wms;
		}
	
		public List<DatoXDimensiones> getDimensiones() {
			 return new ArrayList(new TreeSet(dimensiones));
		
		}

		public void setDimensiones(List<DatoXDimensiones> dimensiones) {
			this.dimensiones = dimensiones;
		}

		public List<DatoXEstiloMapa> getEstilos() {
			return estilos;
		}

		public void setEstilos(List<DatoXEstiloMapa> estilos) {
			this.estilos = estilos;
		}


	public String getTabla_no_normalizada() {
			return tabla_no_normalizada;
		}

		public void setTabla_no_normalizada(String tabla_no_normalizada) {
			this.tabla_no_normalizada = tabla_no_normalizada;
		}

		public String getTabla_no_norm_filtro() {
			return tabla_no_norm_filtro;
		}

		public void setTabla_no_norm_filtro(String tabla_no_norm_filtro) {
			this.tabla_no_norm_filtro = tabla_no_norm_filtro;
		}

	public String getTabla_no_norm_campos() {
			return tabla_no_norm_campos;
		}

		public void setTabla_no_norm_campos(String tabla_no_norm_campos) {
			this.tabla_no_norm_campos = tabla_no_norm_campos;
		}

	public String getTabla_no_norm_columnas() {
			return tabla_no_norm_columnas;
		}

		public void setTabla_no_norm_columnas(String tabla_no_norm_columnas) {
			this.tabla_no_norm_columnas = tabla_no_norm_columnas;
		}

	public boolean isGrafico() {
			return grafico;
		}

		public void setGrafico(boolean grafico) {
			this.grafico = grafico;
		}

	public String getUd_medida() {
			return ud_medida;
		}

		public void setUd_medida(String ud_medida) {
			this.ud_medida = ud_medida;
		}



	public String getTabla_no_norm_campos_sort() {
			return tabla_no_norm_campos_sort;
		}

		public void setTabla_no_norm_campos_sort(String tabla_no_norm_campos_sort) {
			this.tabla_no_norm_campos_sort = tabla_no_norm_campos_sort;
		}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}


}
