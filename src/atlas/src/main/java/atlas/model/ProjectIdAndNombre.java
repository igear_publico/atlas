package atlas.model;

public interface ProjectIdAndNombre {
    String getId();
    String getNombre();
}
