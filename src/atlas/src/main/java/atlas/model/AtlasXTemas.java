package atlas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

@Entity
@Table(name = "atlas_x_temas", schema = "atlas3")
public class AtlasXTemas  implements Comparable<AtlasXTemas>{
	@Expose
	@Id
	  @SequenceGenerator(name="atlas_x_temas_id_seq",
      sequenceName="atlas3.atlas_x_temas_id_seq",
      allocationSize=1)
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="atlas_x_temas_id_seq")
	int id;
	
	@ManyToOne (targetEntity = Tema.class, fetch = FetchType.LAZY)
    @JoinColumn(name="id_tema",insertable = false, updatable = false)
	Tema tema;
	
	
	@Column(name = "id_tema")
	private Integer id_tema;
	
    @ManyToOne(targetEntity = Atlas.class, fetch = FetchType.LAZY)
    @JoinColumn(name="id_atlas",insertable = false, updatable = false)
    Atlas  atlas;
	
    @Column(name = "id_atlas")
	private Integer id_atlas;
    
    @Expose
	Integer orden;
	
	public AtlasXTemas() {
		super();
	}

	public Tema getTema() {
		return tema;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	public Atlas getAtlas() {
		return atlas;
	}

	public void setAtlas(Atlas atlas) {
		this.atlas = atlas;
	}

	public int compareTo(AtlasXTemas t) {
		if ((getOrden() == null)||(t.getOrden()==null)){
    		return -1;
    	}
    	if (getOrden()>t.getOrden()){
    		return 1;
    	}
    	else{
    		return -1;
    	}
        
    }
	
   
	public Integer getId_tema() {
		return id_tema;
	}

	public void setId_tema(Integer id_tema) {
		this.id_tema = id_tema;
	}

	public Integer getId_atlas() {
		return id_atlas;
	}

	public void setId_atlas(Integer id_atlas) {
		this.id_atlas = id_atlas;
	}

	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
