package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "dimension", schema = "atlas3")
public class Dimension {

	@Expose
    @Id
	@SequenceGenerator(name="dimension_seq",
			sequenceName="atlas3.dimension_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dimension_seq")
    int id;

	@Expose
    String nombre;

	@Expose
    String titulo;

	@Expose
    String tabla_valores;


    public Dimension() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTabla_valores() {
		return tabla_valores;
	}

	public void setTabla_valores(String tabla_valores) {
		this.tabla_valores = tabla_valores;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
}
