package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = "dato_x_mapawms", schema = "atlas3")
public class DatoXMapaWMS  implements Comparable<DatoXMapaWMS> {

	@Expose
	@Id
	@SequenceGenerator(name="dato_x_mapa_seq",
			sequenceName="atlas3.dato_x_mapa_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dato_x_mapa_seq")
	int id;

	@ManyToOne 
    @JoinColumn(name="id_mapa")
	MapaWMS mapa;

	@Expose
	boolean solo_imprimir;

	@Expose
	boolean siempre_visible;

	@Expose
	boolean visible=true;
	
	@Expose
	int orden;

	@Expose
	boolean gfi_popup;
	
	@Expose
	String popup;
	
	@Expose
	String tempo;

	@Expose
	String dimension1;

	@Expose
	String dimension2;

	@Expose
	String dimension3;

	@Expose
	String dimension4;
	
	public DatoXMapaWMS() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public MapaWMS getMapa() {
		return mapa;
	}
	public void setMapa(MapaWMS mapa) {
		this.mapa = mapa;
	}
	
	public boolean isSolo_imprimir() {
		return solo_imprimir;
	}
	public void setSolo_imprimir(boolean solo_imprimir) {
		this.solo_imprimir = solo_imprimir;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	
    public boolean isSiempre_visible() {
		return siempre_visible;
	}
	public void setSiempre_visible(boolean siempre_visible) {
		this.siempre_visible = siempre_visible;
	}
	public int compareTo(DatoXMapaWMS t) {
    	if (getOrden()==t.getOrden()){
    		if (getMapa().getId()>=t.getMapa().getId()){
    			return 1;
    		}
    		else {
    			return -1;
    		}
    		
    	}
    	else if (getOrden()>=t.getOrden()){
    		return 1;
    	}
    	else{
    		return -1;
    	}
        
    }
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public String getDimension1() {
		return dimension1;
	}
	public void setDimension1(String dimension1) {
		this.dimension1 = dimension1;
	}
	public String getDimension2() {
		return dimension2;
	}
	public void setDimension2(String dimension2) {
		this.dimension2 = dimension2;
	}
	public String getDimension3() {
		return dimension3;
	}
	public void setDimension3(String dimension3) {
		this.dimension3 = dimension3;
	}
	public String getDimension4() {
		return dimension4;
	}
	public void setDimension4(String dimension4) {
		this.dimension4 = dimension4;
	}

	

	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public boolean isGfi_popup() {
		return gfi_popup;
	}
	public void setGfi_popup(boolean gfi_popup) {
		this.gfi_popup = gfi_popup;
	}
	public String getPopup() {
		return popup;
	}
	public void setPopup(String popup) {
		this.popup = popup;
	}
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
	
}
