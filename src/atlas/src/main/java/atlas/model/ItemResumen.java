package atlas.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.google.gson.annotations.Expose;

@Entity
@Immutable
@Table(name = "v_item_summary", schema = "atlas3")
public class ItemResumen{
	 @Expose
	    @Id
	String id;
	 @Expose
	String titulo;
	 @Expose
	int num_datos;
	 @Expose
	int num_subtemas;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNum_datos() {
		return num_datos;
	}
	public void setNum_datos(int num_datos) {
		this.num_datos = num_datos;
	}
	public int getNum_subtemas() {
		return num_subtemas;
	}
	public void setNum_subtemas(int num_subtemas) {
		this.num_subtemas = num_subtemas;
	}


}
