package atlas.model;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

@Entity
@Table(name = "atlas", schema = "atlas3")
public class Atlas {

    @Expose
    @Id
    @SequenceGenerator(name="atlas_seq",
            sequenceName="atlas3.atlas_seq",
            allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="atlas_seq")
    int id;

    @Expose
    String titulo;

    @Expose
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String resumen;

    @Expose
    @Column(unique = true)
    String etiqueta;

    @OneToMany (mappedBy="atlas", fetch = FetchType.LAZY ,cascade = {CascadeType.ALL}, orphanRemoval = true)
	Set<AtlasXTemas> temas;


    public Atlas() {
        temas = new TreeSet<AtlasXTemas>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public Set<AtlasXTemas> getTemas() {
        return temas;
    }
    public ArrayList<Tema> getTemasOrdenados() {
    	if (temas !=null){
   	TreeSet<AtlasXTemas> temasOrdenados = new TreeSet(temas);
   	ArrayList<Tema> resultado = new ArrayList<Tema>();
    	for (AtlasXTemas unTema:temasOrdenados){
    		resultado.add(unTema.tema);
    	}
        return resultado;
    	}
    	else{
    		return null;
    	}
    	
    	
    }
    
    public ArrayList<TemaConOrden> getTemasConOrden() {
    	if (temas!=null){
   	TreeSet<AtlasXTemas> temasOrdenados = new TreeSet(temas);
   	ArrayList<TemaConOrden> resultado = new ArrayList<TemaConOrden>();
    	for (AtlasXTemas unTema:temasOrdenados){
    		TemaConOrden tema = new TemaConOrden();
    		tema.setId(unTema.getId_tema());
    		tema.setId_relation(unTema.id);
    		tema.setNombre(unTema.tema.getNombre());
    		tema.setTipo_icono(unTema.tema.getTipo_icono());
    		tema.setIcono(unTema.tema.getIcono());
    		tema.setOrden(unTema.orden);
    		resultado.add(tema);
    	}
        return resultado;
    	}
    	return null;
    }
    public void setTemas(Set<AtlasXTemas> temas) {
        this.temas = temas;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }


    public Tema[] getTemasInfo(){

        if(getTemas()!=null){
            Tema[] temas = new Tema[getTemas().size()];
            int i = 0;
            for(AtlasXTemas t: getTemas()){
            	Tema tt = t.getTema();
                Tema tema = new Gson().fromJson(t.getTema().toString(),Tema.class);
                tt.setAtlas(null);
                tt.setSubtemas(null);
                temas[i] = tt;
                i++;
            }
            return temas;
        }else{
            return new Tema[0];
        }
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }

}
