package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.Date;

import javax.persistence.*;

@Entity
@Table(name = "mapa_wms", schema = "atlas3")
public class MapaWMS {

	@Expose
	@Id
	@SequenceGenerator(name="mapa_seq",
			sequenceName="atlas3.mapa_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="mapa_seq")
	int id;

	@Expose
	String url;
	@Expose
	String capas;
	@Expose
	String estilo;
	@Expose
	String version;
	@Expose
	String formato;
	@Expose
	String leyenda;
	@Expose
	boolean glg;
	@Expose
	String cql_filter;
	@Expose
	String etiqueta;
	@Expose
	boolean wmst;
	@Expose
	Date fecha_defecto;
	
	public MapaWMS() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCapas() {
		return capas;
	}

	public void setCapas(String capas) {
		this.capas = capas;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getLeyenda() {
		return leyenda;
	}

	public void setLeyenda(String leyenda) {
		this.leyenda = leyenda;
	}

	public boolean isGlg() {
		return glg;
	}

	public void setGlg(boolean glg) {
		this.glg = glg;
	}

	public String getCql_filter() {
		return cql_filter;
	}

	public void setCql_filter(String cql_filter) {
		this.cql_filter = cql_filter;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public boolean isWmst() {
		return wmst;
	}

	public void setWmst(boolean wmst) {
		this.wmst = wmst;
	}

	public Date getFecha_defecto() {
		return fecha_defecto;
	}

	public void setFecha_defecto(Date fecha_defecto) {
		this.fecha_defecto = fecha_defecto;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
}
