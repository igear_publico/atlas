package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = "estilo_clases", schema = "atlas3")
public class EstiloClases {

	@Expose
	@Id
	@SequenceGenerator(name="estilo_clase_seq",
			sequenceName="atlas3.estilo_clase_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="estilo_clase_seq")
	int id;

	@Expose
	String valor;

	@Expose
	Double valor_min;

	@Expose
	Double valor_max;

	@Expose
	String color;
	
	public EstiloClases() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getValor_min() {
		return valor_min;
	}

	public void setValor_min(Double valor_min) {
		this.valor_min = valor_min;
	}

	public Double getValor_max() {
		return valor_max;
	}

	public void setValor_max(Double valor_max) {
		this.valor_max = valor_max;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}

	
}
