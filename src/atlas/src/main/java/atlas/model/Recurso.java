package atlas.model;

import javax.persistence.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "recurso", schema = "atlas3")
public class Recurso  implements Comparable<Recurso>{

	@Expose
	@Id
	@SequenceGenerator(name="recurso_seq",
			sequenceName="atlas3.recurso_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="recurso_seq")
	    int id;
	
	@Expose
	   @ManyToOne(targetEntity = TipoRecurso.class, fetch = FetchType.LAZY)
    @JoinColumn(name="tipo",insertable = false, updatable = false)
   TipoRecurso tipo;
	
	@Expose
    @Column(name = "tipo")
	private Integer id_tipo;
    
	@Expose
	   @Lob
	    @Type(type = "org.hibernate.type.TextType")
	   String url;

	@Expose
	   @Lob
	    @Type(type = "org.hibernate.type.TextType")
	   String descripcion;

	@Expose
	   @Lob
	    @Type(type = "org.hibernate.type.TextType")
	   String opciones;
	
	@Expose
	   @Lob
	    @Type(type = "org.hibernate.type.TextType")
	   String opciones_etiquetas;
	
	    @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "id_item")
	    Item item;
	  
	    public Recurso() {
			super();
		}


		public int getId() {
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		

		public TipoRecurso getTipo() {
			return tipo;
		}


		public void setTipo(TipoRecurso tipo) {
			this.tipo = tipo;
		}


		public Integer getId_tipo() {
			return id_tipo;
		}


		public void setId_tipo(Integer id_tipo) {
			this.id_tipo = id_tipo;
		}


		public String getUrl() {
			return url;
		}


		public void setUrl(String url) {
			this.url = url;
		}


		public String getDescripcion() {
			return descripcion;
		}


		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}


		public Item getItem() {
			return item;
		}


		public void setItem(Item item) {
			this.item = item;
		}


		public String getGridClass(int index){
	    	int resto = index % 3;

	    	if(resto == 0){
	    		return "ui-block-a";
			}if(resto == 1){
				return "ui-block-b";
			}if(resto == 2){
				return "ui-block-c";
			}else{
				return "ui-block-d";
			}
		}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}

	public Recurso getInfo(){
		return new Gson().fromJson(toString(),Recurso.class);
	}
	
	 public int compareTo(Recurso t) {
		
		 if ((t.getTipo()==null)||(getTipo().getOrden() == null)||(t.getTipo().getOrden()==null)){
	    		return -1;
	    	}
	    	if (getTipo().getOrden()>t.getTipo().getOrden()){
	    		return 1;
	    	}
	    	else{
	    		return -1;
	    	}
	    }


	public String[] getOpciones() {
		if (opciones != null){
			return opciones.split("##");
		}
		return null;
	}


	public void setOpciones(String opciones) {
		this.opciones = opciones;
	}


	public String[] getOpciones_etiquetas() {
		if (opciones_etiquetas != null){
			return opciones_etiquetas.split("##");
		}
		else{
			return getOpciones();
		}
		
	}


	public void setOpciones_etiquetas(String opciones_etiquetas) {
		this.opciones_etiquetas = opciones_etiquetas;
	}
	 
}
