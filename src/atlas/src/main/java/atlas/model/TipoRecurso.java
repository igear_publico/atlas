package atlas.model;

import javax.persistence.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "codtiporecurso", schema = "atlas3")
public class TipoRecurso {

	@Expose
	@Id
    int id;
	@Expose
	   String descripcion;
	@Expose
	   String icono;

	@Expose
	   Integer orden;

	    public TipoRecurso() {
			super();
		}


		public int getId() {
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		public String getIcono() {
			return icono;
		}


		public void setIcono(String icono) {
			this.icono = icono;
		}


		public Integer getOrden() {
			return orden;
		}


		public void setOrden(Integer orden) {
			this.orden = orden;
		}


		public String getDescripcion() {
			return descripcion;
		}


		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}


	
}
