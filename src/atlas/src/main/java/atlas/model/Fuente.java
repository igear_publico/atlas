package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "fuente", schema = "atlas3")
public class Fuente {

    @Expose
    @Id
    @SequenceGenerator(name="fuente_seq",
            sequenceName="atlas3.fuente_seq",
            allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fuente_seq")
    int id;

    @Expose
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String txt;

    public Fuente() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
