package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = "ud_temporal", schema = "atlas3")
public class UdTemporal {

	@Expose
    @Id
	@SequenceGenerator(name="ud_temporal_seq",
			sequenceName="atlas3.ud_temporal_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ud_temporal_seq")
    int id;

	@Expose
    String etiqueta;
	@Expose
    String formato_datos;
	@Expose
    String formato_presentacion;
    
    public UdTemporal() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getFormato_datos() {
		return formato_datos;
	}

	public void setFormato_datos(String formato_datos) {
		this.formato_datos = formato_datos;
	}

	public String getFormato_presentacion() {
		return formato_presentacion;
	}

	public void setFormato_presentacion(String formato_presentacion) {
		this.formato_presentacion = formato_presentacion;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}

    
}
