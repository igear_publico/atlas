package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.util.List;
import java.util.Set;

@Entity
@Table(name = "consulta_comentario", schema = "atlas3")
public class ConsultaComentario {


	@Expose
    @Id
    int id;

	@Expose
    String nombre;

	@Expose
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String sql;

	@Expose
    String conexion;

    @ManyToOne
    @JoinColumn(name = "id_item")
    Item item;
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getConexion() {
		return conexion;
	}

	public void setConexion(String conexion) {
		this.conexion = conexion;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
    
}
