package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ud_territorial", schema = "atlas3")
public class UdTerritorial {

	@Expose
    @Id
	@SequenceGenerator(name="ud_territorial_seq",
			sequenceName="atlas3.ud_territorial_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ud_territorial_seq")
    int id;

	@Expose
    String etiqueta;
	@Expose
    String geojson;
	@Expose
	String campo_codigo;

	@Expose
	int srs;

	@Expose
	Double opacity;
	
	public UdTerritorial() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getGeojson() {
		return geojson;
	}

	public void setGeojson(String geojson) {
		this.geojson = geojson;
	}

    public String getCampo_codigo() {
		return campo_codigo;
	}

	public void setCampo_codigo(String campo_codigo) {
		this.campo_codigo = campo_codigo;
	}

	public int getSrs() {
		return srs;
	}

	public void setSrs(int srs) {
		this.srs = srs;
	}

	public Double getOpacity() {
		return opacity;
	}

	public void setOpacity(Double opacity) {
		this.opacity = opacity;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}

}
