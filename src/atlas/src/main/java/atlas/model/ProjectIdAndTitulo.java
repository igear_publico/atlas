package atlas.model;

public interface ProjectIdAndTitulo {
    String getId();
    String getTitulo();
}
