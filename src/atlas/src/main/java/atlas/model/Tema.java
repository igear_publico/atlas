package atlas.model;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

@Entity
@Table(name = "tema", schema = "atlas3")
public class Tema{


    @Expose
    @Id
    @SequenceGenerator(name="tema_seq",
            sequenceName="atlas3.tema_seq",
            allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tema_seq")
    int id;

    @Expose
    String nombre;

    @Expose
    String etiqueta;
    
    @Expose
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String resumen;

    @Expose
    String icono;

    @Expose
    String tipo_icono;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="id_tema",insertable = false, updatable = false)
    Set<AtlasXTemas>  atlas;

    @OneToMany
    @JoinColumn(name="id_tema")
    Set<Subtema> subtemas;

    public Tema() {
        atlas = new TreeSet<AtlasXTemas>();
        subtemas = new TreeSet<Subtema>();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getTipo_icono() {
        return tipo_icono;
    }

    public void setTipo_icono(String tipo_icono) {
        this.tipo_icono = tipo_icono;
    }

    public Set<AtlasXTemas> getAtlas() {
        return atlas;
    }

    public void setAtlas(Set<AtlasXTemas> atlas) {
        this.atlas = atlas;
    }


  

	public Set<Subtema> getSubtemas() {
		if (subtemas!=null){
		TreeSet tree = new TreeSet(subtemas);
		return tree;
		}
		else{
			return null;
		}
    }

    public void setSubtemas(Set<Subtema> subtemas) {
        this.subtemas = subtemas;
    }




    public Atlas[] getAtlasInfo(){
        if(getAtlas()!=null){
            Atlas[] atlas = new Atlas[getAtlas().size()];
            int i = 0;
            for(AtlasXTemas a: getAtlas()){
                a.atlas.setTemas(null);
                atlas[i] = a.atlas;
                i++;
            }
            return atlas;
        }else{
            return new Atlas[0];
        }
    }


    public String getEtiqueta() {
		return etiqueta;
	}


	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}


	public Subtema[] getSubtemasInfo(){

        if(getSubtemas()!=null){
            Subtema[] temas = new Subtema[getSubtemas().size()];
            int i = 0;
            for(Subtema t: getSubtemas()){
                t.setItems(null);
                t.setTema(null);
                temas[i] = t;
                i++;
            }
            return temas;
        }else{
            return new Subtema[0];
        }
    }



    @Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
