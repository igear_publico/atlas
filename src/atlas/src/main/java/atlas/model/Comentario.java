package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.util.List;
import java.util.Set;

@Entity
@Table(name = "comentario", schema = "atlas3")
public class Comentario {


	@Expose
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    int id;


	@Expose
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String texto;


	@Expose
    String tempo;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_item")
    Item item;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTempo() {
		return tempo;
	}

	public void setTempo(String tempo) {
		this.tempo = tempo;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
    
}
