package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = "dato_x_estilo", schema = "atlas3")
public class DatoXEstiloMapa  {

	@Expose
	@Id
	@SequenceGenerator(name="dato_x_estilo_id_seq",
			sequenceName="atlas3.dato_x_estilo_id_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dato_x_estilo_id_seq")
	int id;

	@ManyToOne (cascade = {CascadeType.MERGE})
    @JoinColumn(name="id_estilo")
	EstiloMapa estilo;


	@Expose
	String tempo;
	@Expose
	String dimension1;
	@Expose
	String dimension2;
	@Expose
	String dimension3;
	@Expose
	String dimension4;
	@Expose
	boolean popup_defecto=true;

	@Expose
	String popup;
	@Expose
	String col_tabla_no_norm;
	@Expose
	String col_tabla_no_norm_valor;
	
	public DatoXEstiloMapa() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public EstiloMapa getEstilo() {
		return estilo;
	}
	public void setEstilo(EstiloMapa estilo) {
		this.estilo = estilo;
	}
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public String getDimension1() {
		return dimension1;
	}
	public void setDimension1(String dimension1) {
		this.dimension1 = dimension1;
	}
	public String getDimension2() {
		return dimension2;
	}
	public void setDimension2(String dimension2) {
		this.dimension2 = dimension2;
	}
	public String getDimension3() {
		return dimension3;
	}
	public void setDimension3(String dimension3) {
		this.dimension3 = dimension3;
	}
	public String getDimension4() {
		return dimension4;
	}
	public void setDimension4(String dimension4) {
		this.dimension4 = dimension4;
	}
	public boolean isPopup_defecto() {
		return popup_defecto;
	}
	public void setPopup_defecto(boolean popup_defecto) {
		this.popup_defecto = popup_defecto;
	}
	public String getPopup() {
		return popup;
	}
	public void setPopup(String popup) {
		this.popup = popup;
	}

	public String getCol_tabla_no_norm() {
		return col_tabla_no_norm;
	}
	public void setCol_tabla_no_norm(String col_tabla_no_norm) {
		this.col_tabla_no_norm = col_tabla_no_norm;
	}
	
	public String getCol_tabla_no_norm_valor() {
		return col_tabla_no_norm_valor;
	}
	public void setCol_tabla_no_norm_valor(String col_tabla_no_norm_valor) {
		this.col_tabla_no_norm_valor = col_tabla_no_norm_valor;
	}
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
	
}
