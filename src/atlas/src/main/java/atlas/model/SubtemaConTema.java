package atlas.model;

import com.google.gson.annotations.Expose;

public class SubtemaConTema extends Subtema{
	@Expose
	String nombre_tema;

	public String getNombre_tema() {
		return nombre_tema;
	}

	public void setNombre_tema(String nombre_tema) {
		this.nombre_tema = nombre_tema;
	}
	
	
}
