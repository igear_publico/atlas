package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "subtema", schema = "atlas3")
public class Subtema  implements Comparable<Subtema>{

    @Expose
    @Id
    @SequenceGenerator(name="subtema_seq",
            sequenceName="atlas3.subtema_seq",
            allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="subtema_seq")
    int id;

    @Expose
    String titulo;
    
    @Expose
    String etiqueta;
    
    @Expose
    Integer orden;
    
	@ManyToOne (targetEntity = Tema.class, fetch = FetchType.LAZY)
    @JoinColumn(name="id_tema",insertable = false, updatable = false)
	Tema tema;
	
	
	@Column(name = "id_tema")
	private Integer id_tema;
	
    @OneToMany (mappedBy="subtema", fetch = FetchType.LAZY ,cascade = CascadeType.ALL, orphanRemoval = true)
   	Set<SubtemaXItem> items;

    public Subtema() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema id_tema) {
        this.tema = id_tema;
    }

    public Integer getId_tema() {
		return id_tema;
	}

	public void setId_tema(Integer id_tema) {
		this.id_tema = id_tema;
	}

	public Set<SubtemaXItem> getItems() {
    	if (items!=null){
        return  new TreeSet(items);
    	}
    	else{
    		return null;
    	}
    }

    public ArrayList<ItemConOrden> getItemsConOrden() {
    	if (items!=null){
   	TreeSet<SubtemaXItem> itemsOrdenados = new TreeSet(items);
   	ArrayList<ItemConOrden> resultado = new ArrayList<ItemConOrden>();
    	for (SubtemaXItem unItem:itemsOrdenados){
    		ItemConOrden item = new ItemConOrden();
    		item.setId(unItem.getId_item());
    		item.setId_relation(unItem.id);
    		item.setTitulo(unItem.item.getTitulo());
    		item.setOrden(unItem.orden);
    		resultado.add(item);
    	}
        return resultado;
    	}
    	return null;
    }
    public void setItems(Set<SubtemaXItem> items) {
        this.items = items;
    }
    
    

    public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	
	 public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public int compareTo(Subtema t) {
		 if ((getOrden() == null)||(t.getOrden()==null)){
	    		return -1;
	    	}
	    	if (getOrden()>t.getOrden()){
	    		return 1;
	    	}
	    	else{
	    		return -1;
	    	}
	    }


	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }

}
