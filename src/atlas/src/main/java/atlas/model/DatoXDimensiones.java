package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = "dato_x_dimensiones", schema = "atlas3")
public class DatoXDimensiones   implements Comparable<DatoXDimensiones>{

	@Expose
	@Id
	@SequenceGenerator(name="dato_x_dimensiones_seq",
			sequenceName="atlas3.dato_x_dimensiones_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dato_x_dimensiones_seq")
	int id;

	@ManyToOne (cascade = {CascadeType.MERGE})
    @JoinColumn(name="id_dimension")
	Dimension	dimension;

	@Expose
	int orden;
	
	@Expose
	boolean excluir_en_tabla_no_norm;
	
	public DatoXDimensiones() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Dimension getDimension() {
		return dimension;
	}
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	
	
	public boolean isExcluir_en_tabla_no_norm() {
		return excluir_en_tabla_no_norm;
	}
	public void setExcluir_en_tabla_no_norm(boolean excluir_en_tabla_no_norm) {
		this.excluir_en_tabla_no_norm = excluir_en_tabla_no_norm;
	}
	public int compareTo(DatoXDimensiones t) {
    	if (getOrden()==t.getOrden()){
    		return 0;
    	}
    	else if (getOrden()>t.getOrden()){
    		return 1;
    	}
    	else{
    		return -1;
    	}
        
    }

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
	
}
