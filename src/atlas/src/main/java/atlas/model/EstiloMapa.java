package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "estilo_mapa", schema = "atlas3")
public class EstiloMapa {

	@Expose
	@Id
	@SequenceGenerator(name="estilomapa_seq",
			sequenceName="atlas3.estilomapa_seq",
			allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="estilomapa_seq")
	int id;

	@Expose
	boolean chincheta;
	@Expose
	String icono;

	@Expose
	String etiqueta;


	@OneToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name = "id_estilo")
	List<EstiloClases> clases;

	public EstiloMapa() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isChincheta() {
		return chincheta;
	}

	public void setChincheta(boolean chincheta) {
		this.chincheta = chincheta;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public List<EstiloClases> getClases() {
		return clases;
	}

	public void setClases(List<EstiloClases> clases) {
		this.clases = clases;
	}


	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}



	
}
