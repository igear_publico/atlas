package atlas.model;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.core.env.Environment;

public class DataSourcesManager{
	HashMap<String, DataSource> data_sources=  new HashMap<String, DataSource>();
	
	

	public DataSource getDataSource(String name, Environment env){
		if (data_sources.get(name)!=null){
		
			return data_sources.get(name);
		}
		else{
			
		  DataSource  ds = DataSourceBuilder.create()
	                   .driverClassName("org.postgresql.Driver")
	                   .username(env.getProperty(name+".username"))
	                   .password(env.getProperty(name+".password"))
	                   .url(env.getProperty(name+".url"))
	                   .build();
		 ((org.apache.tomcat.jdbc.pool.DataSource)ds).setTestOnBorrow(true);
		 ((org.apache.tomcat.jdbc.pool.DataSource)ds).setValidationQuery("SELECT 1");
		 ((org.apache.tomcat.jdbc.pool.DataSource)ds).setValidationInterval(30000);
		  data_sources.put(name, ds);
		  return ds;
	       
		}
	}
	   
}
