package atlas.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "item", schema = "atlas3")
public class Item implements Serializable {


    @Expose
    @Id
    @SequenceGenerator(name="item_seq",
            sequenceName="atlas3.item_seq",
            allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="item_seq")
    int id;

    @Expose
    String titulo;

    @Expose
    String etiqueta;
    
    @Expose
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    String comentario;


    @Expose
    boolean es_sql;

    @Expose
    boolean destacar;

    @OneToMany( fetch = FetchType.EAGER,  cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JoinColumn(name = "id_item")
    List<Dato> datos;
    
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name="id_fuente")
    Fuente fuente;

    @OneToMany( cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JoinColumn(name = "id_item")
    Set<Recurso> recursos;

   
    @ElementCollection
    @CollectionTable(name = "items_x_keyword",
            schema = "atlas3",
            joinColumns = @JoinColumn(name = "id_item"))
    @Column(name = "keyword")
    Set<String> keywords;


    @ManyToMany
    @JoinTable(
            name = "subtema_x_item",
            schema = "atlas3",
            joinColumns = @JoinColumn(name = "id_item"),
            inverseJoinColumns = @JoinColumn(name = "id_subtema"))
    Set<Subtema> subtemas;
 

    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "comentario_x_ud_temporal",
            schema = "atlas3",
            joinColumns = @JoinColumn(name = "id_item"),
            inverseJoinColumns = @JoinColumn(name = "id_ud_temporal"))
    Set<UdTemporal> uds_temporales;
    
    public Item() {
    }

    public Item(ProjectIdAndTitulo projection) {
        this.id = Integer.parseInt(projection.getId());
        this.titulo = projection.getTitulo();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

   

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public boolean isEs_sql() {
        return es_sql;
    }

    public void setEs_sql(boolean es_sql) {
        this.es_sql = es_sql;
    }

    public boolean isDestacar() {
        return destacar;
    }

    public void setDestacar(boolean destacar) {
        this.destacar = destacar;
    }

    public Fuente getFuente() {
        return fuente;
    }

    public void setFuente(Fuente fuente) {
        this.fuente = fuente;
    }


    public Set<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }


	public List<Dato> getDatos() {
		return datos;
	}


	public void setDatos(List<Dato> datos) {
		this.datos = datos;
	}


	public Set<UdTemporal> getUds_temporales() {
		return uds_temporales;
	}


	public void setUds_temporales(Set<UdTemporal> uds_temporales) {
		this.uds_temporales = uds_temporales;
	}

    public Set<Subtema> getSubtemas() {
        return subtemas;
    }

    public ArrayList<SubtemaConTema> getSubtemasConTema() {
    	if (subtemas!=null){
    		ArrayList<SubtemaConTema> resultado = new ArrayList<SubtemaConTema>();
   		for (Subtema unsubtema:subtemas){
   			SubtemaConTema subtema = new SubtemaConTema();
   			subtema.setId(unsubtema.getId());
   			subtema.setNombre_tema(unsubtema.tema.getNombre());
   			subtema.setTitulo(unsubtema.getTitulo());
   			
    		resultado.add(subtema);
    	}
        return resultado;
    	}
    	return null;
    }
    public void setSubtemas(Set<Subtema> subtemas) {
        this.subtemas = subtemas;
    }

    public Set<Recurso> getRecursos() {
    	if (recursos!=null){
    	TreeSet tree = new TreeSet(recursos);
		return tree;
    	}
    	else{
    		return null;
    	}
    }

    public void setRecursos(Set<Recurso> recursos) {
        this.recursos = recursos;
    }

    public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }


}
