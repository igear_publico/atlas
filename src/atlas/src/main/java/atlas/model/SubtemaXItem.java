package atlas.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "subtema_x_item", schema = "atlas3")
public class SubtemaXItem  implements Comparable<SubtemaXItem>{
	@Expose
	@Id
	@SequenceGenerator(name="subtema_x_item_id_seq",
	sequenceName="atlas3.subtema_x_item_id_seq",
	allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="subtema_x_item_id_seq")
	int id;


	@Column(name = "id_item")
	private Integer id_item;



	@Column(name = "id_subtema")
	private Integer id_subtema;



	@ManyToOne (cascade = {CascadeType.MERGE},targetEntity = Item.class, fetch = FetchType.LAZY)
    @JoinColumn(name="id_item",insertable = false, updatable = false)
	Item item;
	
    @ManyToOne  (targetEntity = Subtema.class, fetch = FetchType.LAZY)
    @JoinColumn(name="id_subtema",insertable = false, updatable = false)
    Subtema subtema;
	
    @Expose
	Integer orden;
	
	public SubtemaXItem() {
		super();
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	


	public Item getItem() {
		return item;
	}


	public void setItem(Item item) {
		this.item = item;
	}


	public Subtema getSubtema() {
		return subtema;
	}


	public void setSubtema(Subtema subtema) {
		this.subtema = subtema;
	}


	public Integer getId_item() {
		return id_item;
	}


	public void setId_item(Integer id_item) {
		this.id_item = id_item;
	}


	public Integer getId_subtema() {
		return id_subtema;
	}


	public void setId_subtema(Integer id_subtema) {
		this.id_subtema = id_subtema;
	}


	public int compareTo(SubtemaXItem t) {
		if ((getOrden() == null)||(t.getOrden()==null)){
    		return -1;
    	}
    	if (getOrden()>t.getOrden()){
    		return 1;
    	}
    	else{
    		return -1;
    	}
        
    }
}
