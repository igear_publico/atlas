package atlas.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import atlas.controller.AtlasController;
import atlas.model.DataSourcesManager;
import atlas.model.Dato;
import atlas.model.DatoXDimensiones;

@Service
public class DatoService {

	@Resource
	private Environment env;	

	DataSourcesManager dsm = new DataSourcesManager();



	public String getValores(Dato dato, String tempo, String dim1, String dim2, String dim3, String dim4) throws Exception{


		List<DatoXDimensiones> dims = dato.getDimensiones();
		String conds=getConds(dims,dim1,dim2,dim3,dim4,false);
		
		Connection conn = dsm.getDataSource(dato.getTabla_conex(),env).getConnection();
		try{
			String valores_mapa=null;
			String valores_tabla_no_norm=null;
			if (dato.getTabla_datos()!=null){
				valores_mapa=getValores( conn, null, dato.getTabla_datos(), dato.getTabla_filtro(), tempo, conds, true);
			}
			if (!dato.isTabla_normalizada()){
				valores_tabla_no_norm ="\"tabla\":"+ getValores( conn, dato.getTabla_no_norm_campos(), dato.getTabla_no_normalizada(), dato.getTabla_no_norm_filtro(), tempo, getConds(dims,dim1,dim2,dim3,dim4,true), false);
			}
			String valores="";
			if (valores_mapa!=null){
				valores+=(valores_tabla_no_norm!=null?"{\"mapa\":":"")+valores_mapa;
			}
			if (valores_tabla_no_norm!=null){
				valores+=(valores.length()>0?",":"{")+valores_tabla_no_norm+"}";
			}
			return	valores ;
			
		}
		finally{
			conn.close();
		}
	}

	public String getConds(List<DatoXDimensiones> dims, String dim1, String dim2, String dim3, String dim4, boolean tabla_no_norm){
		String conds="";
		for (DatoXDimensiones dim:dims){
			if (!(tabla_no_norm && dim.isExcluir_en_tabla_no_norm())){
				int id_dim = dim.getOrden();
				String dim_value = (id_dim==1 ? dim1:(id_dim==2 ? dim2:(id_dim==3 ? dim3:(id_dim==4 ? dim4:null))));
				if ((dim_value != null)&&(dim_value.length()>0)){

					conds+=" AND "+dim.getDimension().getNombre()+"='"+dim_value+"'";
				}
			}
		}
		return conds;
	}
	public String getValores(Connection conn, String campos, String tabla, String filtro, String tempo, String conds, boolean byCodmun) throws Exception{

			Statement stmt = conn.createStatement();
			AtlasController.log.debug("select "+(campos !=null ? campos:"*")+" from "+tabla+" where "+(filtro!=null? filtro+ " AND ":"")+""
					+ (tempo!=null ? "tempo::character varying='"+tempo+"'": "1=1") +conds);
			ResultSet rs = stmt.executeQuery("select "+(campos !=null ? campos:"*")+" from "+tabla+" where "+(filtro!=null? filtro+ " AND ":"")+""
					+ (tempo!=null ? "tempo::character varying='"+tempo+"'": "1=1")+conds);

			ResultSetMetaData rsmd = rs.getMetaData();

			String attrs="";
			while (rs.next()){
				if (attrs.length()>0){
					attrs +=",";
				}
				if (byCodmun){
					attrs +="\""+rs.getString("codmun")+"\":{";
				}
				else{
					attrs+="{";
				}
				for (int i=1; i<=rsmd.getColumnCount();i++){
					if (i>1){
						attrs +=",";
					}
					
					String valor = rs.getString(i);
					if (rs.wasNull()){
						valor="null";
					}
					else{
						valor = rs.getString(i).replaceAll("\"","\\\\\"");
						if ((rsmd.getColumnType(i)!= java.sql.Types.NUMERIC)&&(rsmd.getColumnType(i)!= java.sql.Types.BIGINT)&&(rsmd.getColumnType(i)!= java.sql.Types.DECIMAL)
								&&(rsmd.getColumnType(i)!= java.sql.Types.DOUBLE)&&(rsmd.getColumnType(i)!= java.sql.Types.FLOAT)&&(rsmd.getColumnType(i)!= java.sql.Types.INTEGER)
								&&(rsmd.getColumnType(i)!= java.sql.Types.REAL)&&(rsmd.getColumnType(i)!= java.sql.Types.SMALLINT)){
							valor = "\""+valor+"\"";
						}
					}
					attrs+="\""+rsmd.getColumnName(i)+"\":"+valor;
				}
				attrs +="}";
			}
			if (byCodmun){
				return "{"+attrs+"}";
			}
			else{
				return "["+attrs+"]";
			}
		}
		

}
