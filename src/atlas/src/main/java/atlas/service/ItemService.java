package atlas.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Service;

import atlas.controller.AtlasController;
import atlas.model.Comentario;
import atlas.model.ConsultaComentario;
import atlas.model.DataSourcesManager;
import atlas.model.Dato;
import atlas.model.Dimension;
import atlas.model.Item;
import atlas.model.UdTemporal;
import atlas.repository.ComentarioRepository;
import atlas.repository.ConsultaComentarioRepository;

@Service
public class ItemService {

	@Resource
	private Environment env;	

	DataSourcesManager dsm = new DataSourcesManager();

	@Autowired
	private DataSource dataSource;

	@Autowired
	ComentarioRepository comentarioRepository;

	@Autowired	
	ConsultaComentarioRepository consultaComentarioRepository;
	
	
	public  ArrayList<DominioDimension>  getDominoTemporalComentario(Item item, HashMap<Integer, DominioDimension> uds_temp_coment){
		 Set<UdTemporal> uts = item.getUds_temporales();
    	 ArrayList<DominioDimension> dominios = new ArrayList<DominioDimension>();
    	
    	 DominioDimension dd=null;
    	 for (UdTemporal ud: uts){
    		 try{
    			 dd = getDominioDimension(item, ud);
    			
    			dominios.add(dd);
    			uds_temp_coment.put(ud.getId(), dd);
    		 }
    		 catch(SQLException ex){
    			 ex.printStackTrace();
    		 }
    	 }
    	 return dominios;
	}
	public  ArrayList<DominioDimension>  getDominoTemporalComentario(Item item){
		return this.getDominoTemporalComentario(item,new  HashMap<Integer, DominioDimension>());
	}
	public DominioDimension getDominioDimension(Item item, UdTemporal dim) throws SQLException{
		AtlasController.log.debug("getDominioDimension");
		DominioDimension result= new DominioDimension();
		result.etiqueta=dim.getEtiqueta();
		result.id=dim.getId();
		List<Dato> datos=item.getDatos();
		ArrayList<String> dominio_codigos = new ArrayList<String>();
		ArrayList<String> dominio_etiquetas = new ArrayList<String>();
		SimpleDateFormat format =null;
		if (dim.getFormato_presentacion()!=null){
			try{
				format = new SimpleDateFormat(dim.getFormato_presentacion());
			}
			catch(IllegalArgumentException ex){
				AtlasController.log.info("No es formato fecha. Asumimos calendario");
			}
		}
		if ((format!=null)||((dim.getFormato_presentacion()==null)&&(dim.getFormato_datos()==null))){
			for (Dato dato:datos){
				if (dato.getTabla_datos()!=null){
					
					Connection conn = dsm.getDataSource(dato.getTabla_conex(),env).getConnection();
					try{
						Statement stmt = conn.createStatement();
						String fecha = "tempo";
						String cond_tempo =null;
						if (dim.getFormato_datos()!=null){
							fecha =  "to_date(tempo::character varying,'"+dim.getFormato_datos()+"')";
							cond_tempo ="length(tempo::character varying)="+dim.getFormato_datos().length();
						}
						AtlasController.log.debug("select distinct tempo, "+fecha+" as fecha from "+dato.getTabla_datos()+" where "+(dato.getTabla_filtro()!=null? dato.getTabla_filtro()+ (cond_tempo !=null ? " AND "+cond_tempo:" "):"")+""
								+" order by tempo");
						ResultSet rs = stmt.executeQuery("select distinct tempo, "+fecha+" as fecha from "+dato.getTabla_datos()+" where "+(dato.getTabla_filtro()!=null? dato.getTabla_filtro()+ (cond_tempo !=null ? " AND "+cond_tempo:" "):"")+""
								+" order by tempo");
						while (rs.next()){
							String valor = rs.getString("tempo");
							if (dominio_codigos.indexOf(valor)<0){
								dominio_codigos.add(rs.getString("tempo"));
								if (format!=null){
									dominio_etiquetas.add(format.format(rs.getDate("fecha")));
								}
								else{
									dominio_etiquetas.add(rs.getString("tempo"));
								}
							}

						}
					}
					finally{
						conn.close();
					}
				}
			}
			result.dominio_codigos=dominio_codigos;
			result.dominio_etiquetas= dominio_etiquetas;
		}
		return result;
	}

	public DominioDimension getDominioDimension(Item item, Dimension dim, int orden, boolean excluir_en_tabla) throws SQLException{
		DominioDimension result= new DominioDimension();
		result.etiqueta=dim.getTitulo();
		result.id=orden;
		result.excluir_en_tabla = excluir_en_tabla;
		List<Dato> datos=item.getDatos();
		ArrayList<String> dominio_codigos = new ArrayList<String>();
		ArrayList<String> dominio_etiquetas = new ArrayList<String>();
		HashMap<String,String> codelist = new HashMap<String,String>();
		Connection conn = DataSourceUtils.getConnection(dataSource);
		try{
			Statement stmt = conn.createStatement();
			AtlasController.log.debug("select id,descripcion from atlas3."+dim.getTabla_valores()+"  order by id");
			ResultSet rs = stmt.executeQuery("select id,descripcion from  atlas3."+dim.getTabla_valores()+"  order by id");
			while (rs.next()){

				
				if (datos.get(0).getTabla_datos()!=null){
					codelist.put(rs.getString("id"),rs.getString("descripcion"));	
				}
				else{
					dominio_codigos.add(rs.getString("id"));
					dominio_etiquetas.add(rs.getString("descripcion"));
				}

			}
		}
		finally{
			conn.close();
		}
		for (Dato dato:datos){
			if (dato.getTabla_datos()!=null){
				conn = dsm.getDataSource(dato.getTabla_conex(),env).getConnection();
				try{
					Statement stmt = conn.createStatement();
					AtlasController.log.debug("select distinct "+dim.getNombre()+" from "+dato.getTabla_datos()+(dato.getTabla_filtro()!=null? " where "+dato.getTabla_filtro():"")+" order by "+dim.getNombre());
					ResultSet rs = stmt.executeQuery("select distinct "+dim.getNombre()+" from "+dato.getTabla_datos()+(dato.getTabla_filtro()!=null?" where "+ dato.getTabla_filtro():"")+" order by "+dim.getNombre());
					while (rs.next()){
						if (codelist.get(rs.getString(dim.getNombre()))!=null){
							dominio_codigos.add(rs.getString(dim.getNombre()));
							dominio_etiquetas.add(codelist.get(rs.getString(dim.getNombre())));
						}

					}

				}
				finally{
					conn.close();
				}
			}
		
		}


		result.dominio_codigos=dominio_codigos;
		result.dominio_etiquetas= dominio_etiquetas;
		return result;
	}
	public String getComentario(Item item, DominioDimension dd){
		if ((dd!=null) && item.isEs_sql()){
			if (dd.dominio_codigos.size()>0){
				String tempo = dd.dominio_codigos.get(dd.dominio_codigos.size()-1);

				return getComentario(item,tempo);
			}
			else{
				return null;
			}

		}
		else{
			return item.getComentario();
		}

	}


	public String getComentario(Item item, String tempo){

		Comentario comentario = comentarioRepository.findByItemAndTempo(item, tempo);
		if (comentario != null){
			return comentario.getTexto();
		}
		else{
			try{
				
				return getAndSaveComentarioSQL( item, tempo);
			}
			catch(SQLException ex){
				AtlasController.log.error("No se ha podido construir el comentario SQL",ex);
				return item.getComentario();
			}
		}


	}

	protected String getAndSaveComentarioSQL(Item item,String tempo)throws SQLException{
		String texto= getComentarioSQL(item,tempo);
		Comentario comentario = new Comentario();
		comentario.setItem(item);
		comentario.setTempo(tempo);
		comentario.setTexto(texto);
		comentarioRepository.save(comentario);
		return texto;

	}

	protected String getComentarioSQL(Item item, String tempo) throws SQLException{

		HashMap<String,HashMap<String,String>> sqls = new HashMap<String,HashMap<String,String>>();
		String SEPARADOR_INICIO = "<@@@";
		String SEPARADOR_FIN = "###>";
		String ETIQUETA_TEMPO="__TEMPO__";
		int posSeparadorIni = 0;
		int posSeparadorFin = -1;
		String comentario_resto = item.getComentario();
		StringBuilder texto = new StringBuilder();

		posSeparadorIni = comentario_resto.indexOf(SEPARADOR_INICIO);
		posSeparadorFin = comentario_resto.indexOf(SEPARADOR_FIN);

		while (posSeparadorIni != -1) {
			texto.append(comentario_resto.substring(0, posSeparadorIni));
			String[] query = comentario_resto.substring(posSeparadorIni + SEPARADOR_INICIO.length(), posSeparadorFin).split("\\.");
			if (sqls.get(query[0])==null){
			//	AtlasController.log.debug(query[0]);
				List<ConsultaComentario> consultas = consultaComentarioRepository.findByItemAndNombre(item, query[0]);

				if (consultas.size()!=0){
					ConsultaComentario consulta= consultas.get(0);
					String sql = consulta.getSql().replaceAll(ETIQUETA_TEMPO, tempo);
					Connection conn = dsm.getDataSource(consulta.getConexion(),env).getConnection();
					try{
						Statement stmt = conn.createStatement();
				//		AtlasController.log.debug(sql);
						ResultSet rs = stmt.executeQuery(sql);
						ResultSetMetaData md= rs.getMetaData();
						HashMap<String,String> valores = new HashMap<String,String>();
						if (rs.next()){
							for (int i=1; i<=md.getColumnCount(); i++){
								valores.put(md.getColumnName(i),rs.getString(md.getColumnName(i)));
							}
						}
						sqls.put(query[0], valores);
					}
					finally{
						conn.close();
					}
				}
				else{
					AtlasController.log.error("Error: no está dada de alta la sql "+query[0]);

				}

			}
			comentario_resto = comentario_resto.substring(posSeparadorFin+ SEPARADOR_FIN.length());

			try{
				texto.append(sqls.get(query[0]).get(query[1]));
			}
			catch(NullPointerException ex){
				AtlasController.log.error("Error al evaluar la sql ",ex);
				texto.append("ERROR al evaluar "+query[0]+"."+query[1]);
			}
			catch(ArrayIndexOutOfBoundsException ex){
				AtlasController.log.error("La referencia a la consulta SQL no es correcta. No contiene . como separador",ex);
				texto.append("La referencia a la consulta SQL no es correcta. No contiene . como separador");
			}
			posSeparadorIni = comentario_resto.indexOf(SEPARADOR_INICIO);
			posSeparadorFin = comentario_resto.indexOf(SEPARADOR_FIN);

		}
		texto.append(comentario_resto);
		return texto.toString();
	}
}
