package atlas.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import atlas.controller.AtlasController;
import atlas.model.DataSourcesManager;
import atlas.model.Dato;
import atlas.model.DatoXDimensiones;

@Service
public class ZBSService {

	@Resource
	private Environment env;	

	DataSourcesManager dsm = new DataSourcesManager();



	public String getInfo(String zbs, String gender) throws Exception{

		
		Connection conn = dsm.getDataSource("conexAtlas_salud",env).getConnection();
		Statement stmt=null;//Objeto para la ejecucion de sentencias
		Statement stmt2=null;//Objeto para la ejecucion de sentencias
		ResultSet rs=null;//Objeto para guardar los resultados
		ResultSet rs2=null;//Objeto para guardar los resultados
		String valores="";
		try{
			stmt=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			//Creamos una sentencia a partir de la conexión, que refleje cambios y solo de lectura
		stmt2=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			//Seleccionamos la tabla
AtlasController.log.debug("select saludp.indicadores.codigo_indicador_agrupado, saludp.indicadores.identificador_indicador, indicadores.grupo_indicador, indicadores.nombre_indicador_agrupado, indicadores.periodo_de_estudio_indicador, saludp.v_atlas_sp.sexo, indicadores.unidad_medida_indicador, saludp.v_atlas_sp.valor_redondeado as valores_indicador, saludp.v_atlas_sp.significacion_estadistica, saludp.v_atlas_sp.texto_tasa, saludp.v_atlas_sp.casos, saludp.v_atlas_sp.casos_mostrar,saludp.v_atlas_sp.prp_dosdecimales, saludp.v_atlas_sp.indicador_quintiles, saludp.v_atlas_sp.ici, saludp.v_atlas_sp.ics,saludp.v_atlas_sp.tasa as tasa from saludp.v_atlas_sp inner join saludp.indicadores on saludp.v_atlas_sp.identificador_del_indicador=saludp.indicadores.identificador_indicador and saludp.v_atlas_sp.codzbs='" + zbs + "' and (saludp.v_atlas_sp.sexo='" + gender + "' or saludp.v_atlas_sp.sexo='3')  order by indicadores.grupo_indicador, indicadores.codigo_indicador_agrupado,  indicadores.periodo_de_estudio_indicador desc, saludp.v_atlas_sp.sexo");
		rs = stmt.executeQuery("select saludp.indicadores.codigo_indicador_agrupado, saludp.indicadores.identificador_indicador, indicadores.grupo_indicador, indicadores.nombre_indicador_agrupado, indicadores.periodo_de_estudio_indicador, saludp.v_atlas_sp.sexo, indicadores.unidad_medida_indicador, saludp.v_atlas_sp.valor_redondeado as valores_indicador, saludp.v_atlas_sp.significacion_estadistica, saludp.v_atlas_sp.texto_tasa, saludp.v_atlas_sp.casos, saludp.v_atlas_sp.casos_mostrar,saludp.v_atlas_sp.prp_dosdecimales, saludp.v_atlas_sp.indicador_quintiles, saludp.v_atlas_sp.ici, saludp.v_atlas_sp.ics,saludp.v_atlas_sp.tasa as tasa from saludp.v_atlas_sp inner join saludp.indicadores on saludp.v_atlas_sp.identificador_del_indicador=saludp.indicadores.identificador_indicador and saludp.v_atlas_sp.codzbs='" + zbs + "' and (saludp.v_atlas_sp.sexo='" + gender + "' or saludp.v_atlas_sp.sexo='3')  order by indicadores.grupo_indicador, indicadores.codigo_indicador_agrupado,  indicadores.periodo_de_estudio_indicador desc, saludp.v_atlas_sp.sexo");
		
		valores+="{\"datos\":[";
		
			ResultSetMetaData md = rs.getMetaData();
			int columnCount = md.getColumnCount();
			
			String previousId_indicador = "";
			boolean ponerComa=false;
			while (rs.next()) {			
				float tasa = rs.getFloat("tasa");
				String id_indicador = rs.getString("codigo_indicador_agrupado");
				
				if (id_indicador.equals(previousId_indicador)) {
					// como vienen ordenados por codigo y año, y solo queremos el más reciente, saltar
					continue;
				} else {
					previousId_indicador = id_indicador;
				}
				
				
				
				
				String tempo = rs.getString("periodo_de_estudio_indicador");
				int dimens = Integer.parseInt(rs.getString("sexo"));

				rs2 = stmt2.executeQuery("select valor_minimo_tasa, punto_corte_primerquintil_tasa, punto_corte_segundoquintil_tasa, punto_corte_tercerquintil_tasa, punto_corte_cuartoquintil_tasa, valor_maximo_tasa, sexo from saludp.indicadores where codigo_indicador_agrupado='" + id_indicador + "' and saludp.indicadores.periodo_de_estudio_indicador='"+tempo+"' and valor_minimo_tasa is not null");
				boolean hayQuintiles=false;
				
				// Van hasta 4 porque dimension toma valores de 1 a 3
				float[][] quintiles = new float[4][6];
				while (rs2.next()) {
				hayQuintiles=true;
				
					int dimens2 = Integer.parseInt(rs2.getString(7));

						// ampliar +/-1 el intervalo por si hay decimales
					quintiles[dimens2][0] = Float.parseFloat(rs2.getString(1))-1;
					quintiles[dimens2][1] = Float.parseFloat(rs2.getString(2));
					quintiles[dimens2][2] = Float.parseFloat(rs2.getString(3));
					quintiles[dimens2][3] = Float.parseFloat(rs2.getString(4));
					quintiles[dimens2][4] = Float.parseFloat(rs2.getString(5));
					quintiles[dimens2][5] = Float.parseFloat(rs2.getString(6))+1;
				}
				rs2.close();
				
				if (hayQuintiles){
				if (ponerComa){
					valores+=",";
				}
				ponerComa=true;
				valores+="{";
				int interval_tasa = -1;

				if ((tasa >= quintiles[dimens][0]) && (tasa <= quintiles[dimens][1])) {
					interval_tasa = 1;
				} else if ((tasa >= quintiles[dimens][1]) && (tasa <= quintiles[dimens][2])) {
					interval_tasa = 2;
				} else if ((tasa >= quintiles[dimens][2]) && (tasa <= quintiles[dimens][3])) {
					interval_tasa = 3;
				} else if ((tasa >= quintiles[dimens][3]) && (tasa <= quintiles[dimens][4])) {
					interval_tasa = 4;
				} else if ((tasa >= quintiles[dimens][4]) && (tasa <= quintiles[dimens][5])) {
					interval_tasa = 5;
				}
				
				valores+="\"interval_tasa\": \"" + interval_tasa + "\"";

				boolean isIdxPrivacion = rs.getString("nombre_indicador_agrupado").equalsIgnoreCase("índice de privación");
				boolean isAnyos = rs.getString("periodo_de_estudio_indicador").indexOf('-') != -1;
				boolean isPct =  rs.getString("unidad_medida_indicador").equalsIgnoreCase("Porcentaje");
				boolean isREM =  rs.getString("unidad_medida_indicador").equalsIgnoreCase("REM");
				boolean isSuaviz =  rs.getString("unidad_medida_indicador").toLowerCase().indexOf("rrsuavizad") != -1;
				String val=rs.getString("valores_indicador");
				val = (val!=null?val:""); 
				if (isIdxPrivacion){
					valores+=",\"valor\": " + val ;
				}
				else if (isPct){
					valores+=",\"porcentaje\": " + val;
				}
				if (isAnyos){
					valores+=",\"edad\": " + val;
				}
				
				if (isREM){
					valores+=",\"rem\": " + val;
				}
				if (isSuaviz){
					valores+=",\"rems\": " + val;
					
				}

				for (int i=1; i<=columnCount;i++) {
					if (md.getColumnName(i).equals("tasa")) {
						continue;
					}
					if (md.getColumnName(i).equals("prp_dosdecimales") && (!isSuaviz)) {
						continue;
					}
					valores+=",";
					 val=rs.getString(i);
					 boolean numero=false;
					 
					 try{
						 new Double(val);
						 numero=true;
					 }
					 catch(Exception ex){
						 
					 }
					 
						//logger.debug("valor "+val);
						//logger.debug("valor formateado "+(val!=null ?val.replaceAll("\"","\\\\\""):val));
					valores+="\""+md.getColumnName(i)+"\":"+(numero ?"":"\"")+(val!=null?val.replaceAll("\"","\\\\\""):"")+(numero ?"":"\"");
				}
				valores+="}";
				
				}
				
				
			}
		
		
			valores+="]";

		
			valores+="}";

			rs.close();
			stmt.close();
			stmt2.close();
			
			
		}
		finally{
			conn.close();
		}
		return	valores ;
	}


}
