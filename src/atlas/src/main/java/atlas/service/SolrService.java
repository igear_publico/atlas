package atlas.service;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import atlas.controller.AtlasController;

@Service
public class SolrService {

	private final RestTemplate restTemplate;
	@Resource
	private Environment env;	
	
    @Autowired
	public SolrService( RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();

    }


	public String search(String atlas, String texto) throws Exception{
		 String request =env.getProperty("solr_url")+"/select?q=comentario:"+texto+" or busqueda:"+texto+"&fq=atlas:"+atlas+"&wt=json";
		 AtlasController.log.debug(request);
	     String response = this.restTemplate.getForObject(request, String.class);
	     return response;
	}

	public String searchItem(String atlas, String texto,int start) throws Exception{
    	if(texto.equals("")){
    		texto = "*";
		}else{
			texto="("+texto+"~1 OR "+texto+"^3)";
		}
		String request =env.getProperty("solr_url")+"/select?q=comentario:"+texto+" or busqueda:"+texto+"&start="+start*10+"&rows=10"+"&fq=atlas:"+atlas+"&fq=clase:item&wt=json";
		AtlasController.log.debug(request);
		String response = this.restTemplate.getForObject(request, String.class);
		AtlasController.log.debug(response);
		return response;
	}
	
	 public void refresh(){

    	 String request =env.getProperty("solr_url")+"/update?stream.body=<delete><query>*:*</query></delete>&commit=true";
		 AtlasController.log.debug(request);
	     String response = this.restTemplate.getForObject(request, String.class);
	     AtlasController.log.debug(response);
	     
	     request =env.getProperty("solr_url")+"/dataimport?command=full-import";
		 AtlasController.log.debug(request);
	     response = this.restTemplate.getForObject(request, String.class);
	     AtlasController.log.debug(response);
    }

}
