package atlas.service;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import atlas.controller.AtlasController;
import atlas.model.Comentario;
import atlas.model.DataSourcesManager;
import atlas.model.Item;
import atlas.repository.ComentarioRepository;
import atlas.repository.ItemRepository;


@Service
public class CacheReloader {
	@Resource
	private Environment env;	

	DataSourcesManager dsm = new DataSourcesManager();

	@Autowired
	ItemRepository itemRepository;
	@Autowired
    ItemService itemService;
	
	@Autowired
    SolrService solrService;
	
	@Autowired
	ComentarioRepository comentarioRepository;

    //Segundos - Minutos - Hora - Día del més - Més - Día de la semana
    @Scheduled(cron = "0 0 1 * * *")
    //@Scheduled(cron = "* * * * * *")
    //@Scheduled(fixedDelay = 1000000, initialDelay = 1000)
    public void reloadComentarios() throws Exception {
    	AtlasController.log.info("Regenerando caché de comentarios");
/*
    	List<Item> items= itemRepository.findAllSQL();
    	Connection conn = dsm.getDataSource("conexAtlas_atlas",env).getConnection();
    	Statement stmt = conn.createStatement();
    	for (Item item:items){
    		AtlasController.log.debug("Procesando item "+item.getId());
    	  	try{
        	 ArrayList<DominioDimension> dominios = itemService.getDominoTemporalComentario(item);
        	 for (DominioDimension dd:dominios){
        		 ArrayList<String> tempos = dd.dominio_codigos;
        		 for (int i=0; i<tempos.size();i++){
        			 String tempo = tempos.get(i);
        			 try{
        				
        					String texto= itemService.getComentarioSQL(item,tempo);
        					Comentario comentario =  comentarioRepository.findByItemAndTempo(item,tempo);
        					if (comentario == null){
            					AtlasController.log.debug("INSERT INTO atlas3.comentario (tempo,texto,id_item) values ('"+tempo+"',...");
        						stmt.executeUpdate("INSERT INTO atlas3.comentario (tempo,texto,id_item) values ('"+tempo+"','"+texto+"',"+item.getId()+")");
        					}
        					else{
        						AtlasController.log.debug("UPDATE atlas3.comentario SET ... where id="+comentario.getId());
        						stmt.executeUpdate("UPDATE atlas3.comentario SET texto='"+texto+"' where id="+comentario.getId());
        					}
        					
        			 }
        			 catch(SQLException ex){
        				 AtlasController.log.error("No se ha podido construir el comentario SQL para el item "+item.getId()+ "("+tempo+")");
        			 }
        		 }
        	 }
    	  	}
    	  	catch (Exception ex){
    	  		AtlasController.log.error("No se ha podido obtener los valores de tempo del item "+item.getId(),ex);
    	  	}

    	}*/
    	AtlasController.log.info("Fin del proceso de recarga de comentarios");
    	AtlasController.log.info("Reindexando Solr");
    	solrService.refresh();
    	AtlasController.log.info("Fin del proceso de reindexado");
    }

}


