package atlas.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class DominioDimension {

	@Expose
	public int id;

	@Expose
	public String etiqueta;

	@Expose
	public boolean excluir_en_tabla;

	@Expose
	public ArrayList<String> dominio_codigos;

	@Expose
	public ArrayList<String> dominio_etiquetas;


	@Override
	public String toString() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
}
