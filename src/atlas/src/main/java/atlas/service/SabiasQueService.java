package atlas.service;

import atlas.model.Item;
import atlas.model.SabiasQue;
import atlas.model.Tema;
import atlas.repository.ItemRepository;
import atlas.repository.SabiasQueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.Set;

@Service
public class SabiasQueService {


    @Autowired
    SabiasQueRepository sabiasQueRepository;

    public SabiasQue getSabiasQueAleatorio(int idAtlas){
        SabiasQue sabiasQue = null;
        Set<SabiasQue> sabiasQues = sabiasQueRepository.findByAtlas(idAtlas);

        int size = sabiasQues.size();
        if(size>0){
            int item = new Random().nextInt(size);
            int i = 0;
            for(SabiasQue obj : sabiasQues)
            {
                if (i == item){
                    sabiasQue = obj;
                }
                i++;
            }
        }

        return sabiasQue;
    }
}
