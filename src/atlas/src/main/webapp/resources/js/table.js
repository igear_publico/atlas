var table=null;

var colorDependingBg = {};

function getFontColorDependingBg(bgColor) {
	if (bgColor=="transparent"){
		return "#000";
	}
	if (! colorDependingBg[bgColor]) {
		var c = bgColor.substring(1);      // strip #
		var rgb = parseInt(c, 16);   // convert rrggbb to decimal
		var r = (rgb >> 16) & 0xff;  // extract red
		var g = (rgb >>  8) & 0xff;  // extract green
		var b = (rgb >>  0) & 0xff;  // extract blue

		var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

		if (luma < 140) {
			colorDependingBg[bgColor] = "#FFF";
		} else {
			colorDependingBg[bgColor] = "#000";
		}
	}
	return colorDependingBg[bgColor];
}

function getColumnFormatter(campo_mostrar){
	return function(value, options, rowData){
			return rowData[campo_mostrar];
		};
	
}

function loadTable(valores_tabla){
	
	var column_style = new Object();
	var sort_columns = new Array();
	var invisible_cols = new Array();
	if (table==null){
		if(dato.tabla_normalizada){
			 table = FooTable.init('#dataTable', {
					"columns": [
						{ "name": "codmun", "title": dato.ud_territorial.etiqueta},
						{ "name": "valor", "title": "Valor"+(dato.ud_medida ? " ("+dato.ud_medida+")":"")}
					]});
		}
		else{
			var columns=new Array();
			var campos=dato.tabla_no_norm_campos.split(",");
			var titulos=dato.tabla_no_norm_columnas.split(",");
			var campos_sort;
			
			if (dato.tabla_no_norm_campos_sort && (dato.tabla_no_norm_campos_sort!=dato.tabla_no_norm_campos)){
				campos_sort = dato.tabla_no_norm_campos_sort.split(",");
			}
				for (var i=0; i<titulos.length; i++){
					var column = new Object();
					
					column.title=titulos[i].trim();
					if (campos_sort && campos_sort[i] && (campos_sort[i].trim()!=campos[i].trim())){
						column.name=campos_sort[i].trim();
						var campo_mostrar = campos[i].trim();
						sort_columns.push(column.name);
						invisible_cols.push(campo_mostrar);
						column.formatter =  getColumnFormatter(campo_mostrar);
					}
					else{
						column.name=campos[i].trim();
					}
					columns.push(column);
				
				}
				for (var j=0; j<invisible_cols.length;j++){
					
					var column = new Object();
					column.name=invisible_cols[j];
					column.visible=false;
					columns.push(column);
			
			}
			 table = FooTable.init('#dataTable', {
					"columns": columns});
		}
		$('.footable-filtering-search input').attr("placeholder", "Buscar");
		table.sort(0);
	}
	else if (!dato.tabla_normalizada){
		
		
		if (dato.tabla_no_norm_campos_sort && (dato.tabla_no_norm_campos_sort!=dato.tabla_no_norm_campos)){
			var campos_sort = dato.tabla_no_norm_campos_sort.split(",");
			var titulos=dato.tabla_no_norm_columnas.split(",");
			var campos=dato.tabla_no_norm_campos.split(",");
			for (var i=0; i<titulos.length; i++){
				if (campos_sort && campos_sort[i] && (campos_sort[i].trim()!=campos[i].trim())){
					sort_columns.push(campos_sort[i].trim());
					invisible_cols.push(campos[i].trim());
				}
			}
		}
	}
	if (dato.tabla_normalizada){
		var	utList = dUtList[dato.ud_territorial.etiqueta];
		var uts = Object.keys(valores);
		var datos_tabla = new Array();
		for (var i=0;i<uts.length;i++){
			var valor = valores[uts[i]].valor;
			if (valor){
			try{
				valor = parseFloat(valor);
			}
			catch(err){
				// no es numero
			}
			}
			var values ={ 
				codmun:utList[uts[i]],
				valor:	{
					options:{
						"style": {
							"backgroundColor":getColor(uts[i]),
							"color": getFontColorDependingBg(getColor(uts[i]))
						}} ,
					value:valor
				}

		};
			datos_tabla.push(values);
			
		}
		table.loadRows(datos_tabla,true);
		
	}
	else{
		var column_style = new Object();
		var colorear = false;
		var campos=dato.tabla_no_norm_columnas.split(",");
		var col_names=dato.tabla_no_norm_campos.split(",");
			for (var i=0; i<campos.length; i++){
				var estilo = getEstilo(dato.estilos,campos[i].trim());
				if (estilo){
					column_style[campos[i].trim()]=estilo;
					colorear=true;
				}
			}
			
			
		if (colorear){
			var datos_tabla = new Array();
			for (var i=0;i<valores_tabla.length;i++){
				
				var values = new Object();
				for (var j=0; j<campos.length;j++){
					var columna = campos[j].trim();
					var col_name = col_names[j].trim();
					if (column_style[columna]){
						var bgColor = getColor4Value(column_style[columna].estilo,valores_tabla[i][column_style[columna].col_tabla_no_norm_valor]);
						values[col_name]=new Object();
						values[col_name].options = {	"style": {	"backgroundColor": bgColor, "color": getFontColorDependingBg(bgColor) }};
						values[col_name].value=valores_tabla[i][col_name];
					}
					
					else{
						values[col_name]=valores_tabla[i][col_name];
					}
				
				}
				for (var j=0; j<sort_columns.length;j++){
						if (values[invisible_cols[j]].options){
							values[sort_columns[j]]=values[invisible_cols[j]];
							values[sort_columns[j]].value=valores_tabla[i][sort_columns[j]];
						}
						else{
							values[sort_columns[j]]=valores_tabla[i][sort_columns[j]];
						}
						values[invisible_cols[j]] = valores_tabla[i][invisible_cols[j]];
					
				
				}
				datos_tabla.push(values);

			}
			table.loadRows(datos_tabla,true);
		}
		
		else{
			table.loadRows(valores_tabla,true);
		}
	}
	
}

function emptyTable(){
	
	if (table){
		table.rows.load([],false);
	}
}

function deleteTable(){
	table=null;
	$("#dataTable").empty();
}

function updTabla(id_item){
	$(".udTerritorial").val($("#selectTablaUdTerr").val());
	$("select.udTerritorial").selectmenu("refresh");
	updMapa(id_item);
}

function updTempoTabla(id){
	$(".udTemporal"+id).val($("#selectTablaUdTemp"+id).val());
	$("select.udTemporal"+id).selectmenu("refresh");
	updTempoMapa(id);
}

function updDimTabla(id){
	$(".dimMapa"+id).val($("#selectTablaDim"+id).val());
	$("select.dimMapa"+id).selectmenu("refresh");
	updDimMapa(id);
}

function getColor4Value(estilo_mapa,valueToFind){
		for (var i = 0; i < estilo_mapa.clases.length; i++) {
			var valor = estilo_mapa.clases[i].valor;
			if( (typeof valor!='undefined') && (valor != null )){
				if (valor==valueToFind){
					return estilo_mapa.clases[i].color;
				}
			}
			else{
				var minV =  estilo_mapa.clases[i].valor_min;
				var maxV =  estilo_mapa.clases[i].valor_max;
				if (isNaN(minV) || isNaN(maxV)) {
					if ((valueToFind == parseFloat(minV)) && (valueToFind == parseFloat(maxV))) {
						return intervalStyles[type][i].fillcolor;
					}
				} else {
					if ((valueToFind >= parseFloat(minV)) && (valueToFind <= parseFloat(maxV))) {
						return estilo_mapa.clases[i].color;
					}
				}
			}
		}
	
	return "transparent";
}

function downloadTable(){
		table.rows.collapse();
		try{
		$("#excelFireTable").remove();
		}

		catch(err){}
		try{
			$("#paraExcel").remove();
			}
			catch(err){}
		  myClone = document.getElementById("dataTable").cloneNode(true);
		  myClone.id="excelDataTable";
		  $("body").append("<div id='paraExcel' style='display:none;'></div>");
		  document.getElementById("paraExcel").appendChild(myClone);
		  $('#excelDataTable th,#excelDataTable td ').filter(function() {
			    return $(this).css('display') == 'none';
			}).remove();
		  $("#excelDataTable .footable-filtering").remove();
		  $("#excelDataTable .footable-paging").remove();
		  $("#excelDataTable .noExcel").remove();
		  $("#excelDataTable img").remove();
		var htmls = document.getElementById("excelDataTable").outerHTML.replace(/<td /ig,"<td x:num ");
        var uri = 'data:application/vnd.ms-excel;base64,';
        var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>{table}</body></html>'; 
        var base64 = function(s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        };

        var format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
                return c[p];
            })
        };

        

        var ctx = {
            worksheet : 'Worksheet',
            table : htmls
        }


        var link = document.createElement("a");
        link.download = etiqueta_item+".xls";
        link.href = uri + base64(format(template, ctx));
        link.click();
		

}