
function initScroll() {

	$(window).scroll(function() {
		  if ($(window).scrollTop()>5){
			  $("#scrollTop").show();
		  }
		  else{
			  $("#scrollTop").hide();
		  }
	});
	
	 $("#scrollTop").click(function(event) {
		 $('html,body').animate({scrollTop:0}, 500);
	 });
}


function goToUrlSection(){
	if(location.hash){
		goToSection(location.hash);
	}
}
function goToSection(section){
var gap = 140;
$('html,body').animate({scrollTop:$(section).offset().top - gap}, 500);
}