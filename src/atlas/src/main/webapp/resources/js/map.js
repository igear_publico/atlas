var DOTS_PER_M=3571.4285;
var WMS_SRS="EPSG:25830";
WMS_BBOX_X_MIN=-1034440;
WMS_BBOX_Y_MIN=3560031;
WMS_BBOX_X_MAX=2259829;
WMS_BBOX_Y_MAX=5822607;
//[493707.6026845637, 4413376, 886332.3973154363, 4755088]
var bbox_aragon = [571580, 4400803, 812351,
                   4760039];
//[434305.34375, 4490332.98828125, 949625.65625, 4843082.01171875]

OVERVIEW_BOUND_X_MIN = 530000;
OVERVIEW_BOUND_X_MAX= 855000;
OVERVIEW_BOUND_Y_MIN = 4410000;
OVERVIEW_BOUND_Y_MAX= 4760000;

var SATELITE_WMS_URL=["https://wmspro-idearagon.aragon.es/erdas-iws/ogc/wms/AragonFoto"];
var SATELITE_WMS_LAYERS=["modis,spot,landsat,orto_reciente"];

var map,map_projection,overlay;

proj4.defs("EPSG:25830","+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs");
ol.proj.proj4.register(proj4);

var estilo;
var bboxList;
var raster_gfi;
var selected_ft;
var gettingCoordsRes=null;

function initMap() {
	map_projection = ol.proj.get('EPSG:25830');
	map_projection.setExtent([WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX]);

	var options = {
			projection : map_projection,
//			maxExtent : new OpenLayers.Bounds(WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX),
			center : [675533, 4589000],
			zoom:12

	};
	var overviewLayer = new ol.layer.Image(
			{
				source: new ol.source.ImageStatic({url:'/lib/IDEAragon/themes/default/images/mapaOverview.gif',
					imageExtent:[OVERVIEW_BOUND_X_MIN, OVERVIEW_BOUND_Y_MIN, OVERVIEW_BOUND_X_MAX,
					             OVERVIEW_BOUND_Y_MAX],
					             imageSize:[130,140]})
			}
	);
	var controlOptions = {
			projection : map_projection,
			center : [675533, 4589000],
			extent: [675532, 4588999,675534, 4589001],
			maxResolution: (OVERVIEW_BOUND_X_MAX-OVERVIEW_BOUND_X_MIN)/130,
			minResolution:(OVERVIEW_BOUND_X_MAX-OVERVIEW_BOUND_X_MIN)/130/*,
	    			maxZoom:0,
	    			minZoom:0*/

	};

	map = new ol.Map({
		controls: [new ol.control.ScaleLine(),
		           new ol.control.Zoom(),
		           new ol.control.ZoomToExtent({

		        	   label:"",
		        	   tipLabel:"Ir a la extensión de Aragón",
		        	   extent: bbox_aragon
		           }),

		           new ol.control.OverviewMap({
		        	   view:new ol.View(controlOptions),
		        	   layers: [overviewLayer],
		        	   tipLabel: 'Mapa de situación'
		           })],
		           target: $('#map')[0],
		           //  layers:[baseLayer],
		           view: new ol.View(options)
	});
	map.getView().fit(bbox_aragon,{"size": map.getSize(),"constrainResolution":false});

	addLayers();
	setTimeout(function(){
		map.getView().setMinZoom(map.getView().getZoom());
		//configureMap();

	},1000);
	includeNorthControl();
	$( "#mainPage" ).on( "pageshow", function (event) {
		map.updateSize();
	} );

	$("#btnToc").prepend('<button onclick="javascript:collapseTocButton()" class="mapButton" title="Capas"><span id="tocCollapseIcon" >»</span><i id="tocExpandIcon" class="oculto fa fa-layer-group"></i></button>');
	var toolToc = new ol.control.Control({element: document.getElementById("btnToc")});
	map.addControl(toolToc);
	if($( document ).width()<=510){
		collapseTocButton();
	}

	$("#btnGoTo").prepend('<button onclick="javascript:collapseGoToButton()" class="mapButton" title="Ir a"><span id="goToCollapseIcon" class="oculto">»</span><i id="goToExpandIcon" class="fa fa-search"></i></button>');
	var toolGoTo = new ol.control.Control({element: document.getElementById("btnGoTo")});
	map.addControl(toolGoTo);
	initGoToAutocomplete();

	overlay = new ol.Overlay({
		element: document.getElementById('infoTooltip'),
		positioning: 'bottom-left'
	});

	overlay.setMap(map);
	overlay.getElement().style.display =  'none';
	/*	selectControl =new ol.interaction.Select({
		filter: function (feature, layer) {
			return (feature.getGeometry().getType()!='Point');
		}});
	map.addInteraction(selectControl);*/
	map.on(['pointerclick','singleclick'], function (evt) {
		if (gettingCoordsRes){
			$("#recurso"+gettingCoordsRes).val(Math.round(evt.coordinate[0])+";"+Math.round(evt.coordinate[1]));
			
			  $([document.documentElement, document.body]).animate({
			        scrollTop: $("#recurso"+gettingCoordsRes).offset().top+200
			    }, 100);
			  gettingCoordsRes=null;
		}
		else if (raster_gfi){
			$.ajax({
				url: raster_gfi.mapa.url+"?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&LAYERS="+raster_gfi.mapa.capas+"&STYLES=&INFO_FORMAT=application/json&CRS=EPSG:25830&BBOX="+
				evt.coordinate[0] + ',' + evt.coordinate[1] + ',' + (evt.coordinate[0]+1) + ',' + (evt.coordinate[1]+1)+'&WIDTH=1&HEIGHT=1&QUERY_LAYERS='+raster_gfi.mapa.capas+'&X=1&Y=1'+
				(raster_gfi.time ? "&TIME="+raster_gfi.time:""),
				type: 'GET',
				success:	function(data) {
					var json =data;
					try{
						json = $.parseJSON(data);
					}
					catch(err){

					}
					var features = json.features;
					if (features.length>0){

						if (raster_gfi.popup){
							valor_popup=getPopupPersonalizado(raster_gfi.popup,features[0].properties);
						}
						else {

							valor_popup=getPopupDefecto(features[0].properties["GRAY_INDEX"]);
						}
					}
					if (valor_popup) {

						overlay.setPosition(evt.coordinate);
						overlay.getElement().innerHTML = valor_popup;
						overlay.getElement().style.display =  '';

					} else {
						//	selectControl.getFeatures().clear();
						overlay.getElement().style.display =  'none';
					}

				},

				error: 		function(data, textStatus, errorThrown) {
					console.log('Error obteniendo GFI. State: ' + data.readyState + ", Status:" + data.status);
					resultado= false;
				}
			});
		}
		

	});
	map.on(['pointermove','pointerclick','singleclick'], function (evt) {

		document.body.style.cursor =  '';
		var texto_popup='';
		map.forEachFeatureAtPixel(evt.pixel, function (feature) {
			//console.log("pixel"+feature.get(dato.ud_territorial.campo_codigo));
			if (dato.ud_territorial){
				var cmun = feature.get(dato.ud_territorial.campo_codigo);
				var valor_popup="";
				if (valores[cmun]){
					if (( !dato.mapa_vectorial) || estilo.popup_defecto ){
						valor_popup= '(' +getPopupDefecto(valores[cmun].valor) + ')' ;
					}
					else if (estilo.popup && (estilo.popup.length >0)){
						valor_popup=getPopupPersonalizado(estilo.popup,valores[cmun]);

					}
				}
				var	name = dUtList[dato.ud_territorial.etiqueta][cmun] +  '<br/>'+valor_popup;


				if (name) {

					overlay.setPosition(evt.coordinate);
					if (texto_popup){
						texto_popup+="<br/>";
					}
					texto_popup+=name;
				//	overlay.getElement().innerHTML = name;


		//			return feature;
				}
			}
		},{"layerFilter":function(layer){return layer.get("popup");}});

		if (texto_popup) {
			overlay.getElement().innerHTML = texto_popup;
			overlay.getElement().style.display =  '';
		} else {
			//	selectControl.getFeatures().clear();
			overlay.getElement().style.display =  'none';
		}

		document.body.style.cursor = texto_popup ? 'pointer' : '';

	});

	map.getView().on('change:resolution', function () {

		var escala = Math.round(map.getView().getResolution()*DOTS_PER_M);

	});

}

function initGoToAutocomplete(){
	var ut = "Municipios";

	$( "#zoomTo").autocomplete({
		appendTo:"#btnGoTo",
		select: function( event, ui ) {
			if (dato.ud_territorial){
				ut=dato.ud_territorial.etiqueta;
			}
			zoomToFt(cUtList[ut][ui.item.value.toUpperCase()]);
		},
		source: function(request, response) {
			if (dato.ud_territorial){
				ut=dato.ud_territorial.etiqueta;
			}
			var results = $.ui.autocomplete.filter(customArray[ut], request.term);
			response(results.slice(0, 10));
		}
	});
}
function collapseTocButton(){

	if ($("#toc").hasClass("oculto")) {
		$("#toc").removeClass("oculto");

		$("#tocExpandIcon").hide();
		$("#tocCollapseIcon" ).show();
		$("#tocCollapseIcon" ).removeClass("oculto");
		$("#btnToc").addClass("expanded");

	} else {
		$("#toc").addClass("oculto");
		$("#tocCollapseIcon").hide();
		$("#tocExpandIcon").show();
		$("#tocExpandIcon" ).removeClass("oculto");
		$("#btnToc").removeClass("expanded");
	}

}

function collapseGoToButton(){

	if ($("#goTo").hasClass("oculto")) {
		$("#goTo").removeClass("oculto");

		$("#goToExpandIcon").hide();
		$("#goToCollapseIcon" ).removeClass("oculto");
		$("#goToCollapseIcon" ).show();
		$("#btnGoTo").addClass("expanded");

	} else {
		$("#goTo").addClass("oculto");
		$("#goToCollapseIcon").hide();
		$("#goToExpandIcon").show();
		$("#btnGoTo").removeClass("expanded");
	}

}
function includeNorthControl() {
	var icon = document.createElement('i');
	icon.className = "fa fa-compass";
	icon.id="northIcon";
	var element= document.createElement('span');
	element.appendChild(icon);
	map.addControl(new ol.control.Rotate({"autoHide":false,
		"label":element,
		tipLabel:"Flecha de norte"}));
	setTimeout(function(){$(".ol-rotate-reset").attr("title","Flecha de norte");},200);
}

function addLayers(){
	raster_gfi=null;

	if (dato.ud_territorial){

		loadVectorLayer(contextPath+"/resources/data/geojson/"+dato.ud_territorial.geojson, "EPSG:"+dato.ud_territorial.srs,9999-(dato.mapa_vectorial ? dato.mapa_vectorial_orden:0),dato.estilos,dato.ud_territorial.campo_codigo,dato.ud_territorial.opacity);
		loadSearchLayer(contextPath+"/resources/data/geojson/"+dato.ud_territorial.geojson, "EPSG:"+dato.ud_territorial.srs,  dato.ud_territorial.campo_codigo) ;

	}
	else{
		
		loadSearchLayer(contextPath+"/resources/data/geojson/T02_Municipios_4326.json", "EPSG:25830","CMUNINE");
	}
	if (dato.mapas_wms){
		loadWMSLayers();
	}

}

function getGlgLegend(wms){
	var listaLeyendas =  wms.capas;
	var listaStyles;
	if (wms.estilo){
		listaStyles = wms.estilo;
	}
	return wms.url + "?SERVICE=WMS&VERSION="+(wms.version ? wms.version : "1.0.0")+"&REQUEST=GetLegendGraphic&format=image/png&layer=" + listaLeyendas + (listaStyles? "&STYLE="+listaStyles:"")+"&width=20&height=20&legend_options=forceLabels:on;fontName:Lucida%20Sans%20Regular";

}
function loadSoloImprimirLegend(dimension){


	for (var i=0; i<dato.mapas_wms.length; i++){

		var mapa = dato.mapas_wms[i];

		if (coincideDimension(mapa,dimension) && coincideTempo(mapa)){

			if (mapa.solo_imprimir){
				var wms = mapa.mapa;

				$("#toc .etiqueta_vectorial").text((wms.etiqueta ? wms.etiqueta : $("h1.titulo").text()+(dato.ud_medida ?" ("+ dato.ud_medida+")":"")));
				var urlCurrentLegend = wms.leyenda;
				if (wms.glg){


					urlCurrentLegend =getGlgLegend(wms);

					$("#toc .legend_vectorial img").attr("src",urlCurrentLegend);


				}
				else{
					$("#toc .legend_vectorial img").attr("src",urlCurrentLegend);
				}
			}

		}
	}
}
function loadWMSLayers(dimension){
	getDimensions();
	for (var i=0; i<dato.mapas_wms.length; i++){

		var wms = dato.mapas_wms[i];

		if (coincideDimension(wms,dimension) && coincideTempo(wms)){

			if (wms.solo_imprimir){
				addLegend2Toc(wms,wms.id,true,(wms.mapa.etiqueta ? wms.mapa.etiqueta : $("h1.titulo").text()+(dato.ud_medida ?" ("+ dato.ud_medida+")":"")));
			}
			else{
				if (wms.gfi_popup){
					raster_gfi=wms;
				}
				var layer = loadWMSLayer(wms.mapa, 9999-wms.orden,wms.siempre_visible || wms.visible);
				layer.set("dim1",wms.dimension1);
				layer.set("dim2",wms.dimension2);
				layer.set("dim3",wms.dimension3);
				layer.set("dim4",wms.dimension4);
				layer.set("pk",wms.id)
				if (wms.siempre_visible){
					addLegend2Toc(wms,wms.id,false,(wms.mapa.etiqueta ? wms.mapa.etiqueta : $("h1.titulo").text()+(dato.ud_medida ?" ("+ dato.ud_medida+")":"")));

				}
				else{
					addLayer2Toc(layer,wms);
				}
			}
		}
	}
}
function loadVectorLayer(urlData, srs, zIndex, estilos, campo_codigo, opacity) {
	var projection = new ol.proj.Projection({code:"EPSG:25830"});

	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData,
			format: new ol.format.GeoJSON({dataProjection: new ol.proj.Projection({code:srs}), featureProjection:projection})
		})
	});
	if (dato.mapa_vectorial && estilos && estilos.length>0 && estilos[0].estilo && estilos[0].estilo.chincheta){
		layer.setStyle(function(feature) {
			// filtrar o hacer lo que se necesite
			//return feature.getId() !== undefined ? style : null;
			var style = new ol.style.Style({
				image: new ol.style.Icon({src:estilos[0].estilo.icono}),

				text: new ol.style.Text({

					text: (valores && valores[feature.get(campo_codigo)]?valores[feature.get(campo_codigo)].valor:""),
					fill: new ol.style.Fill({color: 'black'}),
					stroke: new ol.style.Stroke({color: 'white', width: 2})
				})
			});
			return style;
		});
	}
	else{
		
			layer.setStyle( function(feature) {
				// filtrar o hacer lo que se necesite
				//return feature.getId() !== undefined ? style : null;
				var color=getColor(feature.get(campo_codigo));
				var style = new ol.style.Style({
					//image: getIcon(feature, customIcon),
					image: new ol.style.Circle({
				        radius: 5,
				        fill: new ol.style.Fill({color: color}),
				        stroke: new ol.style.Stroke({
				          color: (color!="transparent"?"#000000":color) , 
				          width:(color=="transparent"?0:1)
				        })
				      }),
					stroke: new ol.style.Stroke({
						color: "#CCC" ,
						width: 1
					}),
					fill: new ol.style.Stroke({
						color: color,
					})
				});
				return style;
			});
	}
	layer.setZIndex(zIndex);

	if (opacity){
		layer.setOpacity(opacity);
	}
	else{
		layer.setOpacity(0.75);
	}

	layer.set("vectorial",true);
	layer.set("popup",true);

	map.addLayer(layer);

	return layer;

}

function loadSearchLayer(urlData, srs,  campo_codigo) {
	var projection = new ol.proj.Projection({code:"EPSG:25830"});

	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData,
			format: new ol.format.GeoJSON({dataProjection: new ol.proj.Projection({code:srs}), featureProjection:projection})
		})
	});
	layer.setStyle( function(feature) {
		// filtrar o hacer lo que se necesite
		//return feature.getId() !== undefined ? style : null;
		var style = new ol.style.Style({
			//image: getIcon(feature, customIcon),
			stroke: new ol.style.Stroke({
				color: getSelectedColor(feature.get(campo_codigo),"transparent") ,
				width: getSelectedWidth(feature.get(campo_codigo),"0.1")
			}),
			fill: new ol.style.Stroke({
				color: "transparent",
			})
		});
		return style;
	});

	bboxList=new Object();
	$("#zoomTo").attr("placeholder",(dato.ud_territorial ? dato.ud_territorial.etiqueta : "Municipios"));

	layer.getSource().on("addfeature", function(event){

		var feature = event.feature;
		bboxList[feature.getProperties()[campo_codigo]]= feature.getGeometry().getExtent();

	});

	layer.setZIndex(10000);
	layer.set("vectorial",true);
	map.addLayer(layer);

	return layer;

}

function getSelectedColor(ft_code,color){
	if (ft_code==selected_ft){
		return 'rgba(0,162,232,1)';
	}
	else{
		return color;
	}

}
function getSelectedWidth(ft_code,width){
	if (ft_code==selected_ft){
		return 6;
	}
	else{
		return width;
	}

}
function loadWMSLayer(wms,zIndex,visible){
	var layer =new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': wms.capas,'VERSION':(wms.version ? wms.version: '1.1.1'),'FORMAT': (wms.formato ? wms.formato : 'image/png')},
			url:wms.url,
			projection: map_projection
		})
	});
	if (wms.wmst && wms.fecha_defecto){
		layer.getSource().updateParams({'TIME': wms.fecha_defecto.substr(0,10)});
		if (raster_gfi){
			raster_gfi.time =  wms.fecha_defecto.substr(0,10);
		}
		var valDate = new Date(wms.fecha_defecto);
		$("#serieTempoMapD").val(valDate.getDate());
		$("#serieTempoMapM").val(valDate.getMonth()+1);
		$("#serieTempoMapY").val(valDate.getFullYear());
		$( "#selectMapaUdTemp4").datepicker("setDate", valDate);
	}
	layer.set("vectorial",false);
	layer.set("wmst",wms.wmst);
	layer.setVisible(visible);
	layer.setZIndex(zIndex);
	map.addLayer(layer);

	return layer;
}
function addLegend2Toc(mapa, layerIdx,solo_imprimir,etiqueta){
	var wms = mapa.mapa;


	var urlCurrentLegend = wms.leyenda;
	if (etiqueta){
		$("#toc").append("<li id='layer_"+layerIdx+"' class='texto-variable "+(solo_imprimir ? " etiqueta_vectorial ":"")+(layerIdx ? " legend_"+layerIdx:"")+"'>"+etiqueta+"</li>");
	}
	if (wms.glg){

		var listaLeyendas =  wms.capas.split(',');
		var listaStyles;
		if (wms.estilo){
			listaStyles = wms.estilo.split(',');
		}
		var i = 0;
		for (var layerItem in listaLeyendas) {
			i++;
			// incluyo VERSION FIJO A 1.0.0
			// deberia haber tantos styles como layers
			var currentStyle;
			try {
				currentStyle = listaStyles[layerItem];
			} catch(err) {
				// no hay estilos
			}

			urlCurrentLegend = wms.url + "?SERVICE=WMS&VERSION="+(wms.version ? wms.version : "1.0.0")+"&REQUEST=GetLegendGraphic&format=image/png&layer=" + listaLeyendas[layerItem] + (currentStyle? "&STYLE="+currentStyle:"")+"&width=20&height=20&legend_options=forceLabels:on;fontName:Lucida%20Sans%20Regular";
			$("#toc").append("<li id='legend_"+(layerIdx ? layerIdx:"")+"_"+i+"' class='legend "+(layerIdx ? "legend_"+layerIdx:"")+(solo_imprimir ? " legend_vectorial":"")+"'><img src='"+urlCurrentLegend+"'></img></li>");
		}

	}
	else{
		$("#toc").append("<li id='legend_"+(layerIdx ? layerIdx:"") +"' class='legend legend"+(layerIdx ? " legend_"+layerIdx:"")+(solo_imprimir ? " legend_vectorial":"")+"'><img src='"+urlCurrentLegend+"'></img></li>");
	}
}

function addLayer2Toc(layer,mapa){
	var layerIdx=mapa.id;
	var wms = mapa.mapa;
	$("#toc").append("<li id='layer_"+layerIdx+"' class='texto-variable'><input type='checkbox' onchange='setVisibleLayer("+layerIdx+")' "+(layer.getVisible() ? "checked":"")+"/>"+wms.etiqueta+"</li>");
	layer.on("change:visible",function(){
		$("#layer_"+layerIdx+" input").prop('checked', layer.getVisible());
	});
	addLegend2Toc(mapa, layerIdx);
}
function getColor(cmun){

	if (estilo && valores && valores[cmun]){
		var valueToFind = parseFloat(valores[cmun].valor);
		if (!valueToFind){
			valueToFind=valores[cmun].valor;
		}
		var estilo_mapa = estilo.estilo;
		return getColor4Value(estilo_mapa,valueToFind);

	}
	return "transparent";
}


function updTempoMapa(ud_id){
	var tempo=null;
	if (ud_id==4){
		tempo = formatDate($("#selectMapaUdTemp"+ud_id).datepicker( "getDate" ));
	}
	else{
		tempo = $("#selectMapaUdTemp"+ud_id).val();
		$(".udTemporal"+ud_id).val(tempo);
		$("select.udTemporal").selectmenu("refresh");
		$("select.udTemporalMapa").selectmenu("refresh");
		updComentario(item,ud_id,false);
	}
	if (tempo){
		$("select.udTemporalMapa").not(".udTemporal"+ud_id).val("").change();
		if ($("select.udTemporalMapa").not(".udTemporal"+ud_id).length>0){
		for (var i=0; i<datos.length;i++){
			if (datos[i].ud_temporal.id==ud_id){
				dato=datos[i];
				break;
			}
		}
		}
		if (ud_id!=4){
			clearCalendar();
		}
		if (dato.mapa_vectorial){

			loadDataValues(tempo, true);
			loadSoloImprimirLegend();
		}
		else if (dato.mapa_wms){
			var layers = map.getLayers();
			for (var i=0; i<layers.getLength();i++){
				if (layers.item(i).get("wmst")){
					layers.item(i).getSource().updateParams({'TIME': tempo});
					if (raster_gfi){
						raster_gfi.time =  tempo;
					}
				}
			}

		}
	}
}

function updDimMapa(ud_id){
	var valor= $("#selectMapaDim"+ud_id).val();
	$(".dimMapa"+ud_id).val(valor);
	$("select.dimMapa"+ud_id).selectmenu("refresh");
	if (valor){
		if (dato.mapa_vectorial){
			loadDataValues(tempo_valores, true);
			loadSoloImprimirLegend(ud_id);
		}else if (dato.mapa_wms){
			var layers = map.getLayers();
			for (var i=0; i<layers.getLength();i++){
				var layer = layers.item(i);
				if (layer.get("dim"+ud_id)){
					map.removeLayer(layer);
					if (layer.get("pk")){
						$(".legend_"+layer.get("pk")).remove();
					}
				}
			}

			loadWMSLayers(ud_id);
		}

	}
}

function redrawLayers(){
	var layers = map.getLayers();
	for (var i=0; i<layers.getLength();i++){
		if (layers.item(i).get("vectorial")){
			layers.item(i).getSource().changed();
		}
	}
}

function updMapa(){
	$("#toc").empty();
	var udTerr = $("#selectMapaUdTerr").val();
	$(".udTerritorial").val(udTerr);
	$("select.udTerritorial").selectmenu("refresh");
	var layers = map.getLayers();
	while(layers.getLength()>0){

		map.removeLayer(layers.item(0));

	}
	for (var i=0; i<datos.length;i++){
		if (datos[i].ud_territorial.id==udTerr){
			dato=datos[i];
			break;
		}
	}
	var tempo=null;
	if (dato.ud_temporal){
		$(".udTemporalMapa").not("#selectMapaUdTemp"+dato.ud_temporal.id).val("");
		if ($("#selectMapaUdTemp"+dato.ud_temporal.id).val()==""){
			$("#selectMapaUdTemp"+dato.ud_temporal.id+" option:eq("+($("#selectMapaUdTemp"+dato.ud_temporal.id+" option").length-1)+")").attr("selected", "selected");
		}
		tempo = $("#selectMapaUdTemp"+dato.ud_temporal.id).val();
	}
	else{
		$(".udTemporalMapa").val("");
	}
	loadDataValues(tempo,false,true);


}

function setVisibleLayer(layerIdx) {
	var layer;
	var layers = map.getLayers();
	for (var i = 0; i < layers.getLength(); i++) {
		layer = layers.item(i);
		if (layer.get("pk") && (layer.get("pk") == layerIdx) ){
			break;
		}
	}
	if (layer){
		layer.setVisible($("#layer_" + layerIdx + " input").prop('checked'));
		if (layer.getVisible()){
			$(".legend_"+layerIdx).show();
		}
		else{
			$(".legend_"+layerIdx).hide();
		}
	}
}

function getEstilo(estilos, column){
	if (estilos.length==1){
		if (column){
			if (estilos[0].col_tabla_no_norm==column){
				return estilos[0];
			}
			else{
				return;
			}

		}
		else{
			return estilos[0];
		}
	}

	for (var i=0; i<estilos.length; i++){
		var estilo = estilos[i];

		if ((!column) || (estilos[i].col_tabla_no_norm==column)){
			if (coincideDimension(estilo,null,column!=null) && coincideTempo(estilo)){

				return estilo;
			}
		}
	}
}


function coincideDimension(mapa,dimNoVacia,paraTablaNoNorm){
	if (dimensiones.length>0){
		if ((paraTablaNoNorm && dato.dimensiones[0].excluir_en_tabla_no_norm)||(compareDimension(mapa.dimension1,dimensiones[0],dimNoVacia==1))){
			if (dimensiones.length>1){
				if ( (paraTablaNoNorm && dato.dimensiones[1].excluir_en_tabla_no_norm)||(compareDimension(mapa.dimension2,dimensiones[1],dimNoVacia==2))){
					if (dimensiones.length>2){
						if ((paraTablaNoNorm && dato.dimensiones[2].excluir_en_tabla_no_norm)||(compareDimension(mapa.dimension3,dimensiones[2],dimNoVacia==3))){

							if (dimensiones.length>3){
								if ((paraTablaNoNorm && dato.dimensiones[3].excluir_en_tabla_no_norm)||(compareDimension(mapa.dimension4,dimensiones[3],dimNoVacia==4))){
									return true;
								}
							}
							else{
								return true;
							}
						}
					}
					else{
						return true;
					}
				}
			}
			else{
				return true;
			}
		}
	}
	else{
		return true;
	}

	return false;
}
function compareDimension(dim,valor,noVacia,tablaNoNorm){
	if (dim){
		return (dim==valor);
	}
	else if(noVacia){
		return false;
	}
	else{
		return true;
	}
}

function zoomToFt(code) {
	selected_ft = code;
	redrawLayers();
	map.getView().fit( ol.extent.buffer( bboxList[code], 1000), map.getSize());
}


function initMapPage(){
	$("#printTool").removeClass("oculto");
	$(".unit-selector").removeClass("oculto");
	if (dato.tabla_datos || dato.tabla_no_normalizada){
		loadValues();
	}
	$( ".calendarDate" ).datepicker({changeYear:true,yearRange: "1950:+0"});
	initMap();
	initScroll();
	$(".scroll").click(function(event) {
		event.preventDefault();
		goToSection(this.hash);

	});

	loadChartComarcasFilter();

}

function getPopupDefecto(valor){

	return (dato.ud_medida ? dato.ud_medida+": ":"")+ valor ;
}
function getPopupPersonalizado(popup,propiedades){

	var valor_popup="";
	var posIniVar = popup.indexOf("##");
	var posFinVar=-2;
	while (posIniVar>=0){
		valor_popup+=popup.substring(posFinVar+2,posIniVar);
		posFinVar = popup.indexOf("##",posIniVar+1);
		var campo = popup.substring(posIniVar+2,posFinVar);
		var nombre_campo = campo;
		if (campo.indexOf("*")>0){
			nombre_campo = campo.split("*")[0];
		}
		else if (campo.indexOf("/")>0){
			nombre_campo = campo.split("/")[0];
		}

		try{
			valor_popup+=(propiedades[nombre_campo]&& (propiedades[nombre_campo]!="null" )?campo.replace(nombre_campo,propiedades[nombre_campo]):"");
		}
		catch(err){
			valor_popup+=(propiedades[nombre_campo]&& (propiedades[nombre_campo]!="null" )?campo.replace(nombre_campo,propiedades[nombre_campo]):"");
		}
		posIniVar =popup.indexOf("##",posFinVar+3);
	}
	valor_popup+=popup.substring(posFinVar+2);
	return valor_popup;
}

function showFeatures(layer,cql_filter){
	$.ajax({
		url: '/Visor2D?service=WFS&' +
		'version=1.0.0&request=GetFeature&typename='+layer+'&' +
		'outputFormat=application/json&srsname=EPSG:25830&' +
		'CQL_FILTER='+cql_filter,
		type: 'GET',

		success:  function(data){
			var format = new ol.format.GeoJSON();
			var features = format.readFeatures(data);
			if (features.length>0){
				var geom = features[0].getGeometry();

				var source = searchLayer.getSource();
				source.clear(true);
				features[0].setStyle(new ol.style.Style({
					image:new ol.style.Icon({src:img_path+'/chincheta.png'}),
					fill: new ol.style.Fill({
						color: 'rgba(0,0,0,0)'
					}),
					stroke: new ol.style.Stroke({
						color:'rgba(0,162,232,1)',
						width: 3
					})
				}));
				features[0].set("layer",layer);
				features[0].set("cql_filter",cql_filter);
				source.addFeature(features[0]);
				source.refresh();
			}	
			else{
				console.log("No se ha encontrado el objeto con la condicion "+cql_filter+" en la capa "+layer);


			}
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) {
			console.log("No se ha podido cargar el objeto en el mapa "+textStatus +"\n "+errorThrown);
		}
	});
}

function getCoordsFromMap(id_recurso){
	gettingCoordsRes=id_recurso;
	$( "#mapButton" ).trigger( "click" );
	
}