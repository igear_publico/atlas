var plot, plotBroken;

var chart_values, chart_ticks, chart_avg,chart_breakInterval, full_ticks,full_values;

var CHART_AVG_COLOR = "#000000";
var CHART_COLORS=["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#BDBDBD","#5DBBEA","#08519C","#83D17B","#006D2C","#FCCBBD","#A50F15","#E0AC82","#E28436","#E0E1FC","#7E63A3","#D9D9D9","#969696","#84C3FF","#048DEF","#C7E9C0","#2B7742","#FCBBA1","#C60A03","#FEEDDE","#FC5F16","#B8B8D6","#756BB1"];
var serieOrdenada;
var factor_corte=2;

function createChart(values, ticks, avg, break_interval) {

	chart_values = values;
	chart_ticks = ticks;
	chart_avg = avg;
	createBarChart(values, ticks, avg, break_interval);	 
}

function roundUp(valor, bottom){
	var valor = Math.ceil(valor);
	var cifras = valor.toString().length;
	var redondeo = Math.pow(10,Math.max(1,cifras-2));
	var redondeado =  parseInt(valor / redondeo)*redondeo + redondeo;
	if ((redondeado - valor)<(0.10*(redondeado-bottom))){
		redondeado = redondeado + redondeo;
	}
	return redondeado;
}

function getBreakInterval(serie){
	serie.sort(function (a, b) {
		  if (a > b) {
			    return 1;
			  }
			  if (a < b) {
			    return -1;
			  }
			  // a must be equal to b
			  return 0;
			});
	serieOrdenada=serie;
	var max = serie[serie.length-1];
	var break_up = Math.trunc(max * 0.9);
	var cifras = break_up.toString().length;
	if (cifras<=1){
		return;
	}
	var redondeo = Math.pow(10,Math.max(1,cifras-2));
	break_up = parseInt(break_up / redondeo)*redondeo;

	var i = serie.length-2;
	while ((i>0) && (serie[i]>=break_up)){
		i--;
	}
	if (i<=0){
		return null;
	}
	var min =  Math.min(0,serie[0]);
	var break_down = roundUp(serie[i],min);
	
	if ((break_up-break_down)*factor_corte >( (max-min))){
		chart_breakInterval = [break_down,break_up,roundUp(max,break_up)];
		$("#breakChartContainer").removeClass("oculto");
	}
	else{
		$("#breakChartContainer").addClass("oculto");
		chart_breakInterval = null;
	}
	return chart_breakInterval;
}

function changeBreakChart(){
	if ($("#breakChart").prop("checked")){
		createBarChart(chart_values, chart_ticks, chart_avg,chart_breakInterval);
	}
	else{
		createBarChart(chart_values, chart_ticks, chart_avg,null);
	}
}

function createBarChart(values,ticks,avg, breakInterval){

	if (values.length <= 0){
		return;
	}

	$("#mediaChart").text(" ("+Math.round(avg * 100) / 100+")");
	$("#chartBrokenPanel").css("display",(breakInterval ? "block":"none"));
	if (breakInterval){
		$("#chartPanel").addClass("reducedChart");
		
		$("#breakChart").prop("checked",true);
		$("#breakChart").checkboxradio('refresh');
	}
	else {
		$("#chartPanel").removeClass("reducedChart");

		if(plotBroken){ //antes había rotura y ahora no -> forzar qe se vuelva a crear
			plotBroken.destroy();
			plotBroken=null;
			
		}
	}
	$("#chartPanel").empty();
	$("#chartBrokenPanel").empty();
		
		if(plot) {
			plot.destroy();
			plot=null;
		}
		$("#chartPanel").css("width", $("#upperToolbar").width() * 0.9);
		plot = $.jqplot('chartPanel', [values], {
			// The "seriesDefaults" option is an options object that will
			// be applied to all series in the chart.
			canvasOverlay: {
				show: true,
				objects: [
				          {horizontalLine: {
				        	  name: 'Media',
				        	  y: avg,
				        	  lineWidth: 5,
							  linePattern:'dashed',
				        	  color: CHART_AVG_COLOR,
				        	  shadow: false,
				        	  show: true
				          }}]
			},
			seriesDefaults:{
				renderer:$.jqplot.BarRenderer,
				rendererOptions: {varyBarColor : true ,
					// Put a 30 pixel margin between bars.
					barMargin: 5}
			},
			highlighter: {
				show: true,
				tooltipAxes :'y',
				showMarker:false,
				tooltipContentEditor: function (str, seriesIndex, pointIndex) {
					return ticks[pointIndex]+':'+str ;
				}
			},

			
			axes: {
				// Use a category axis on the x axis and use our custom ticks.
				xaxis: {
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					renderer: $.jqplot.CategoryAxisRenderer,
					ticks: ticks,
					tickOptions: {
						angle: 90,
						fontSize: '8pt'

					}
				},
				// Pad the y axis just a little so bars can get close to, but
			// not touch, the grid boundaries.  1.2 is the default padding.
			  yaxis: {
				 // renderer:$.jqplot.LinearAxisRenderer,
	            	 labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
	                    label: (dato.ud_medida ? dato.ud_medida:""),
	                    max:(breakInterval ? breakInterval[0]:null),
	                    min: (serieOrdenada[0]>=0 ? 0:null)
	                  
	            }
			},
			 seriesColors: CHART_COLORS,
			 
				cursor:{
					show: true,
					zoom: true
				} 
		});
		if(plotBroken) {
			plotBroken.destroy();
		}
		if (breakInterval) {
			var maxValue =breakInterval[2];
			
			$("#chartBrokenPanel").css("width", $("#upperToolbar").width() * 0.9);
		 plotBroken = $.jqplot('chartBrokenPanel', [values], {
			// The "seriesDefaults" option is an options object that will
			// be applied to all series in the chart.
			
			seriesDefaults:{
				renderer:$.jqplot.BarRenderer,
				rendererOptions: {varyBarColor : true ,
					// Put a 30 pixel margin between bars.
					barMargin: 5}
			},
			highlighter: {
				show: true,
				tooltipAxes :'y',
				showMarker:false,
				tooltipContentEditor: function (str, seriesIndex, pointIndex) {
					return ticks[pointIndex]+':'+str ;
				}
			},

			
			axes: {
				// Use a category axis on the x axis and use our custom ticks.
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					 showTicks: false,        // wether or not to show the tick labels,
				        showTickMarks: false,    // wether or not to show the tick marks
					tickOptions: {
						 formatString: '',
						showMark:false

					}
				},
				// Pad the y axis just a little so bars can get close to, but
			// not touch, the grid boundaries.  1.2 is the default padding.
			  yaxis: {
				 // renderer:$.jqplot.LinearAxisRenderer,
				  labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                  label: " ",
                  max:maxValue,//si no se especifica el máximo no tiene en cuenta el mínimo :(
	                    min:breakInterval[1]/*,
	                    ticks:[0,25000,50000,75000,100000,600000,625000,650000,675000,700000]*/
	                  
	            }
			},
			 seriesColors: CHART_COLORS
		});
		}
		$("#chartLegend").css("display","inline");

		
}
function createChartComarca(comarca_idx) {
	
	if ((comarca_idx <0)||!(chart_ticks)) {
		return;
	}

	chart_values =  new Array();
	chart_ticks = new Array();
	var serie=new Array();;
	var sum = 0;
	var comarcaSelected = document.getElementById('chartSelectComarca').value;
	for (var i = 0; i<full_ticks.length; i++){
		//if( comarcaUnitList[chart_ticks[i]]==document.getElementById('chartSelectComarca').value){
		var currentCMuni = cMuniList[full_ticks[i].toUpperCase()];
		var comarcaMunicipio = comarcaAsociada[currentCMuni];
		if (comarcaMunicipio == comarcaSelected) {
			if (full_values[i][1]){
			chart_ticks.push(full_ticks[i]);
			full_values[i][0]=chart_ticks.length;
			chart_values.push(full_values[i]);
			sum +=full_values[i][1];
			serie.push(full_values[i][1]);
			/*if (munis.length >1){
				munis += ",";
			}*/
			
		//	munis += currentCMuni;
			}
		}
	}
	chart_avg = sum/chart_ticks.length;
	createBarChart(chart_values,  chart_ticks, chart_avg,getBreakInterval(serie));
}


function getMaxSerie(serie) {
	var maxval = 0;
	for (var i = 0; i<serie.length ; i++){
		maxval = Math.max(maxval, serie[i][1]);
	}
	return maxval;
}


function loadChart() {
	$("#mediaChart").text("");
		
		
			chart_values = new Array();
		chart_ticks = new Array();

		var namesList =  dUtList[dato.ud_territorial.etiqueta];
		
		var i = 0;
		var sum = 0;
		var serie=new Array();
		for (var j in namesList){
			if ((typeof valores[j] !='undefined')&& valores[j] && valores[j].valor){
			
				chart_ticks[i] = namesList[j];
			chart_values[i] = new Array();
			chart_values[i][0] = i+1;
			
			chart_values[i][1] = parseFloat(valores[j].valor);
			serie.push(chart_values[i][1]);
			sum += parseFloat(valores[j].valor);
			i++;
			}
		}
		if (dato.ud_territorial.id==3){
			$("#chartSelectComarcaDiv").removeClass("oculto");
			full_values = chart_values;
			full_ticks=chart_ticks;
			createChartComarca($("#chartSelectComarcaDiv").val());
		}
		else{
			$("#chartSelectComarcaDiv").addClass("oculto");
			createChart(chart_values, chart_ticks, sum / chart_ticks.length,getBreakInterval(serie));
		}
		
	

}
function loadChartComarcasFilter() {	
	// cargar comarcas en select del gráfico
	var isFirst = true;
	var codigos = Object.keys(dUtList["Comarcas"]);
	codigos.sort(function (a, b) {
		  if (dUtList["Comarcas"][a] > dUtList["Comarcas"][b]) {
			    return 1;
			  }
			  if (dUtList["Comarcas"][a] < dUtList["Comarcas"][b]) {
			    return -1;
			  }
			  // a must be equal to b
			  return 0;
			});
	for (var i=0; i<codigos.length; i++) {
		var item =codigos[i];
		$("#chartSelectComarca").append("<option " + (isFirst ? "selected " : "") + "id="+item+" value="+item+">"+dUtList["Comarcas"][item]+"</option>");
		isFirst = false;
	}

	$("#chartSelectComarca").selectmenu("refresh");


}

function getPrintCharts() {

	var xml = "";
	/*
	var activeTableTab = ($("#tabs").tabs("option", "active")>0);
	if (activeTableTab){
		$("#tabs").tabs("option", "active", 0);
	}
	*/
	
		
	var topp = $("#chartPanel .jqplot-yaxis-label").css("top");
	if (plot) {
		xml="<graficos>";
		 
		
		xml+="<avgLegend>"+urlAbsoluta("/lib/IDEAragon/themes/default/images/avg.png")+"</avgLegend>";
		
		if (topp) {
		$("#chartPanel .jqplot-yaxis-label").css("top", Math.max(parseFloat(topp.replace("px","")),0)+"px");
		}
	}

	if (plotBroken) {
		xml+="<barChart><grafico><laImagen>"+$('#chartBrokenPanel').jqplotToImageStr({})+"</laImagen></grafico></barChart>";
		
	}
	if (plot) {
		xml+="<barChart><grafico><laImagen>"+$('#chartPanel').jqplotToImageStr({})+"</laImagen></grafico></barChart></graficos>";
		
			$("#chartPanel .jqplot-yaxis-label").css("top",topp);
		
	}
	return xml;
}

function updGrafico(id_item){
	$(".udTerritorial").val($("#selectGraficoUdTerr").val());
	$("select.udTerritorial").selectmenu("refresh");
	updMapa(id_item);
}

function updTempoGrafico(id){
	$(".udTemporal"+id).val($("#selectGraficoUdTemp"+id).val());
	$("select.udTemporal"+id).selectmenu("refresh");
	updTempoMapa(id);
}

function updDimGrafico(id){
	$(".dimMapa"+id).val($("#selectGraficoDim"+id).val());
	$("select.dimMapa"+id).selectmenu("refresh");
	updDimMapa(id);
}