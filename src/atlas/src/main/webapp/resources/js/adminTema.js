var subtemas;
var subtemas_table;
var all_subtemas;
var tema;
var nuevo;


	function init(){

		mostrarEdicion();

	/*	tinymce.init({
			selector: '#temaEditResumen',
      menubar: false
		 });

*/
		subtemas_table = FooTable.init('#subtemas_table', {
			columns: [
				{ "name": "id", "title": "ID.","visible": false },
				{ "name": "titulo", "title": "Titulo","formatter": function(value, options, rowData){
					return " <a rel='external' href='"+contextPath+"/admin/subtema/"+rowData.id+"' >"+value+"</a>";
				}},
				{ "name": "orden", "title": "Orden"}
			]
			,
			editing: {
				alwaysShow: true,
				allowEdit: false,
				allowAdd: false,
				enabled: true,
				addRow: function(){
					mostrarSubtemas();
				},
				deleteRow: function(row){
					$("#subtema-"+row.value.id).remove();
					row.delete();
				},
				showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
				hideText: 'Cancelar',
				addText: 'Añadir subtema'
			}
		});
		subtemas_table.loadRows(tema.subtemas);


		all_subtemas = FooTable.init('#all_subtemas', {
			columns: [
				{ "name": "id", "title": "ID.","visible": false},
				{ "name": "titulo", "title": "Titulo"},
				{ "name": "nombre_tema", "title": "Tema", "breakpoints": "xs sm"},
				{ "name": "añadir", "title": "Añadir","formatter": function(value, options, rowData){
						return " <a onclick=\"asociarSubtema('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
					}
				}
			]
		});

		$("#addSubtema").popup();

	}


	function visualizar(){
		if (tema.etiqueta_atlas){
			window.open(contextPath+"/"+tema.etiqueta_atlas+"/info/"+tema.etiqueta);	
		}
		else{
			alert("Para poder ver el tema debe incluirlo en algún atlas");
		}
		
	}

	function previsualizar(){

		var cloneTema = Object.assign({}, tema);
		cloneTema.nombre = $("#temaEditNombre").val();
  //  cloneTema.resumen = sinEscapar(tinymce.activeEditor.getContent());
    cloneTema.icono = $("#temaEditIcono").val();
    cloneTema.tipo_icono = $("#tipoIcono").val();
    cloneTema.orden = $("#temaEditOrden").val();
    cloneTema.subtemas = [];

    for(var i = 0; i<subtemas_table.rows.all.length; i++){
      var row = subtemas_table.rows.all[i].val();
      row.editing=null;
      cloneTema.subtemas.push(row);
    }

		var message = JSON.stringify(cloneTema);
		message = message.replace(/""/g,"null");


		$.ajax({
    				url:  contextPath+"/admin/temaFragment/",
    				data: message,
    				processData: false,
    				type: 'POST',
    				contentType: 'application/json',
    				success: function(data){
    				  $("#content").html(data);
    				  mostrarVista();
    				},
    				error: function(error){
    					mostrarVista();
    				}
    			});

	}

	function guardar(){
		tema.nombre = $("#temaEditNombre").val();
		//tema.resumen = sinEscapar(tinymce.activeEditor.getContent());
		tema.icono = $("#temaEditIcono").val();
		tema.tipo_icono = $("#tipoIcono").val();
		tema.orden = $("#temaEditOrden").val();
		tema.subtemas = [];

		for(var i = 0; i<subtemas_table.rows.all.length; i++){
			var row = subtemas_table.rows.all[i].val();
			tema.subtemas.push(row);
		}
		var message = JSON.stringify(tema);
    message = message.replace(/""/g,"null");

		if(tema.id){
			//Editar tema
			$.ajax({
				url:  contextPath+"/admin/tema/"+tema.id+"?token="+tokenId,
				data: message,
				processData: false,
				contentType: 'application/json',
				type: 'PUT',
				success: function(data){
					alert("El tema se ha guardado correctamente");
					location.reload();
				},
				error: function(error){
					//$.mobile.changePage("#popupError");
					alert("No ha sido posible realizar los cambios");
				}
			});
		}else{
			//Crear tema
			$.ajax({
				url:  contextPath+"/admin/tema/"+"?token="+tokenId,
				data: message,
				processData: false,
				contentType: 'application/json',
				type: 'POST',
				success: function(data){
					alert("El tema se ha guardado correctamente");
					location.href=contextPath+'/admin/tema/'+data.id;
				},
				error: function(error){
					//$.mobile.changePage("#popupError");
					alert("No ha sido posible realizar los cambios");
				}
			});
		}
	}


	function mostrarSubtemas(){
			var url = contextPath+"/admin/subtemas/list";

			$.ajax({
				url:  url,
				processData: false,
				contentType: 'application/json',
				type: 'GET',
				success: function(data){
					subtemas = data.subtemas;
					subtemas = subtemas.filter(s => !estaIncluido(s.id));
					all_subtemas.loadRows(subtemas);
					$( "#addSubtema" ).popup("open");
				},
				error: function(error){
					console.log(error)
				}
			});
	}


	function estaIncluido(id){
			for(var i = 0; i<subtemas_table.rows.all.length; i++){
  			var row = subtemas_table.rows.all[i].val();
  			if(row.id == id){
  			    return true;
  			}
  		}
  		return false;
	}

	function asociarSubtema(id){
		for(var i = 0; i<subtemas.length;i++){
			if(subtemas[i].id == id){
				var subtema = Object.assign({}, subtemas[i]);
				subtema.nombre =subtema.nombre_tema
				subtemas_table.rows.add(subtema);
				$( "#addSubtema" ).popup("close");
			}
		}
	}

	function cancel(){
		location.href=contextPath+'/admin/temas';
	}
	
	function changeIcon(){
		if ($("#tipoIcono").val()=='fa'){
			$("#fa-icon").removeClass("oculto");
			$("#fa-icon").addClass($("#temaEditIcono").val());
			$("#img-icon").addClass("oculto");
		}
		else{
			$("#fa-icon").removeClass();
			$("#fa-icon").addClass("oculto");
			
			$("#img-icon").attr("src",$("#temaEditIcono").val());
			$("#img-icon").removeClass("oculto");
		}
	}