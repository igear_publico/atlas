var estilos_table;
var estilo;
var all_estilos;
var estado;
var clases_table;
var estilos;
var currentRowEstilo;

function initEstilo(){
	

    estilo = {};
    estilo.estilo = {};
    estilo.estilo.clase = {};
   var columnas= [
              { "name": "id", "title": "ID.","visible": false },
              { "name": "etiqueta", "title": "Etiqueta"},
              { "name": "tempo", "title": "Tiempo","visible": dato.ud_temporal!=null}];
   if (dato.dimensiones){
	   for (var i=0; i<dato.dimensiones.length; i++){
		   var dimension = dato.dimensiones[i];
		   columnas.push( { "name": "dimension"+dimension.orden, "title": dimension.dimension.titulo, "breakpoints": "xs sm" });
	   }
   }
   var reload_data=false;
   if (estilos_table){
	   estilos_table.destroy();
	   $('#estilos_table').empty();
	   reload_data=true;
	}  
      estilos_table = FooTable.init('#estilos_table', {
        columns: columnas        ,
        editing: {
          alwaysShow: true,
          allowEdit: true,
          enabled: true,

          addRow: function(){
            currentRowEstilo = null;
            mostrarEstilos();
            //abrirFormularioEstilos();
          },
          editRow: function(row){
              currentRowEstilo = row;
              abrirFormularioEditarEstilo();
          },
          deleteRow: function(row){
            //Hay que borrar también del objeto dato
            if(row.value.id){
                var index;
                for(var i = 0; i<dato.estilos.length;i++){
                  if(dato.estilos[i].id == row.value.id){
                      index = i;
                  }
                }
                dato.estilos.splice(index, 1);
            }
            row.delete();
          },
          showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
          hideText: 'Cancelar',
          addText: 'Añadir estilo'
        }
      });
if (reload_data){
	estilos_table.loadRows(dato.estilos);
}

      all_estilos = FooTable.init('#all_estilos', {
        columns: [
        { "name": "id", "title": "ID.", "breakpoints": "xs sm" },
        { "name": "etiqueta", "title": "Etiqueta"},
        { "name": "añadir", "title": "Añadir","style": {"text-align": "right"}, "formatter": function(value, options, rowData){
          return " <a onclick=\"asociarEstilo('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
        }
      }
        ],
          editing: {
            alwaysShow: true,
            allowEdit: false,
            allowDelete: false,
            enabled: true,
            allowView: true,
      		viewRow: function(row){
      		  
      		abrirFormularioCrearEstilo(row);
      		},
            addRow: function(){
              abrirFormularioCrearEstilo();
            },
            showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
            hideText: 'Cancelar',
            addText: 'Añadir nuevo estilo'
          }
      });
      

      if (clases_table){
    	  clases_table.destroy();
   	   $('#clases_table').empty();

   	}  
 

            clases_table = FooTable.init('#clases_table', {
              columns: [
                { "name": "id", "title": "ID.","visible": false },
                { "name": "color", "title": "Color", "type":"html"},
                { "name": "valor", "title": "Valor", "type":"html"},
                { "name": "valor_min", "title": "Valor min.", "type":"html"},
                { "name": "valor_max", "title": "Valor max.", "type":"html"}
               
              ]
              ,
              editing: {
                alwaysShow: true,
                allowEdit: false,
                enabled: true,
                addRow: function(){
                  var row = {}
                  row.color = getHTMLInput(null,"color");
                  row.valor = getHTMLInput(null,"text");
                  row.valor_max = getHTMLInput(null,"number");
                  row.valor_min = getHTMLInput(null,"number");
                  clases_table.rows.add(row);
                },
                deleteRow: function(row){
                  row.delete();
                },
                showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
                hideText: 'Cancelar',
                addText: 'Añadir Clase'
              }
            });


}

function configurarFormularioEstilo(){
	var hay_datos_relacion = false;
	if ($("#datoEditUdTemporal").val()){
		$("#tiempoEstiloContainer").show();
		hay_datos_relacion = true;
	}
	else{
		
		$("#tiempoEstiloContainer").hide();
	}
	 $(".dimensionEstiloContainer").hide();
	if (dato.dimensiones){
		for (var i=0; i<dato.dimensiones.length; i++){
			hay_datos_relacion = true;
			$("#dimension"+dato.dimensiones[i].orden+"EstiloContainer").show();
			$("#dimension"+dato.dimensiones[i].orden+"EstiloContainer label").text(dato.dimensiones[i].dimension.titulo);
		}
	}
	return hay_datos_relacion;
}
function abrirFormularioEditarEstilo(){
	configurarFormularioEstilo();
		//Por defecto, mostrar sólo los datos de la relación y un botón para ampliar
		$( "#datosEntidadEstilo").hide();
		$( "#datosRelacionEstilo").show();
		$( "#edicionAvanzadaEstilo").show();
		$( "#advertenciaEstilo").hide();


    for(var i = 0; i<dato.estilos.length; i++){
        if(dato.estilos[i].id == currentRowEstilo.value.id){
          estilo = dato.estilos[i];
        }
    }

    //Se rellena el formulario con los datos del estilo
    $("#dimension1Estilo").val(estilo.dimension1);
    $("#dimension2Estilo").val(estilo.dimension2);
    $("#dimension3Estilo").val(estilo.dimension3);
    $("#dimension4Estilo").val(estilo.dimension4);
    $("#tiempoEstilo").val(estilo.tempo);
    $("#iconoEstilo").val(estilo.estilo.icono);
    $("#etiquetaEstilo").val(estilo.estilo.etiqueta);
    $("#chinchetaEstilo").prop('checked', estilo.estilo.chincheta).checkboxradio('refresh');



    //Se inicia la tabla con las clases del estilo
    var rows = [];
    for(var j = 0; j<estilo.estilo.clases.length; j++){
        var row = {};
        row.id = estilo.estilo.clases[j].id;
        row.color = getHTMLInput(estilo.estilo.clases[j].color,"color");
        row.valor = getHTMLInput(estilo.estilo.clases[j].valor,"text");
        row.valor_max = getHTMLInput(estilo.estilo.clases[j].valor_max,"number");
        row.valor_min = getHTMLInput(estilo.estilo.clases[j].valor_min,"number");
        rows.push(row);
    }
    clases_table.loadRows(rows);


    //Se actualiza el estado
    estado = "EDITAR";

    //Se abre el formulario
    $( "#addNewEstilo" ).popup("open");
}



function abrirFormularioCrearEstilo(row){
	configurarFormularioEstilo();
		//Se mustrar sólo los datos de la entidad
		$( "#datosEntidadEstilo").show();
		$( "#datosRelacionEstilo").hide();
		$( "#advertenciaEstilo").hide();
		$( "#edicionAvanzadaEstilo").hide();

		//Se vacía el objeto para partir de cero
    estilo = {};
    estilo.estilo = {};
    estilo.estilo.clase = {};


    //Se vacía el formulario
    $("#dimension1Estilo").val('');
    $("#dimension2Estilo").val('');
    $("#dimension3Estilo").val('');
    $("#dimension4Estilo").val('');
    $("#tiempoEstilo").val('');
    if (row){
    	 for(var i = 0; i<estilos.length; i++){
    	        if(estilos[i].id == row.value.id){
    	        		  $.extend( true, estilo,estilos[i]);		
    				
    	          
    	        }
    	    }
    	  $("#iconoEstilo").val(estilo.icono);
    	    $("#etiquetaEstilo").val(estilo.etiqueta);
    	    $("#chinchetaEstilo").prop('checked', estilo.chincheta).checkboxradio('refresh');

    	    $.ajax({
    	        url:  contextPath+"/admin/estilo/"+estilo.id+"/clases",
    	        processData: false,
    	        contentType: 'application/json',
    	        type: 'GET',
    	        success: function(data){
    	        	estilo.id=null;
    	           estilo.clases = data.clases;
    	           //Se inicia la tabla con las clases del estilo
    	    	    var rows = [];
    	    	    for(var j = 0; j<estilo.clases.length; j++){
    	    	        var row = {};
    	    	        row.id = null;
    	    	        row.color = getHTMLInput(estilo.clases[j].color,"color");
    	    	        row.valor = getHTMLInput(estilo.clases[j].valor,"text");
    	    	        row.valor_max = getHTMLInput(estilo.clases[j].valor_max,"number");
    	    	        row.valor_min = getHTMLInput(estilo.clases[j].valor_min,"number");
    	    	        rows.push(row);
    	    	    }
    	    	    clases_table.loadRows(rows);
    	        },
    	        error: function(error){
    	          //$.mobile.changePage("#popupError");
    	          alert("No ha sido posible realizar los cambios");
    	        }
    	      });

    	  

    }
    else{
    $("#iconoEstilo").val('');
    $("#etiquetaEstilo").val('');
    $("#chinchetaEstilo").prop('checked', false).checkboxradio('refresh');
    clases_table.loadRows([]);
    }
		//Se actualiza el estado
    estado = "CREAR";


    //Se abre el formulario
    $( "#addNewEstilo" ).popup("open");
}


function asociarEstilo(id){
	var abrir_form = configurarFormularioEstilo();

		for(var i = 0; i<estilos.length;i++){
			if(estilos[i].id == id){
				//Se guarda la fila seleccionada en el objeto
				var est = estilos[i];
				estilo.estilo = est;


				//Se muestra sólo los datos de la relación
        $( "#datosEntidadEstilo").hide();
        $( "#datosRelacionEstilo").show();
        $( "#advertenciaEstilo").hide();
        $( "#edicionAvanzadaEstilo").hide();


				//Se rellena el formulario (Aunque no se muestre)
        $("#etiquetaEstilo").val(est.etiqueta);
        $("#iconoEstilo").val(est.icono);
        $("#chinchetaEstilo").prop('checked', est.chincheta).checkboxradio('refresh');


        //Los campos de la relación se dejan vacios
        $("#dimension1Estilo").val('');
        $("#dimension2Estilo").val('');
        $("#dimension3Estilo").val('');
        $("#dimension4Estilo").val('');
        $("#tiempoEstilo").val('');
        clases_table.loadRows([]);

				//Se actualiza el estado
        estado = "ASOCIAR";

          //Se abre el formulario
        if (abrir_form){
        $( "#addNewEstilo" ).popup("open");
        }
        else{
        	guardarEstilo();
        }

			}
		}

}



function edicionAvanzadaEstilo(){
	$("#datosRelacionEstilo").hide();
		$( "#datosEntidadEstilo").show();
		$( "#advertenciaEstilo").show();
		$( "#edicionAvanzadaEstilo").hide();
		$( "#addNewEstilo" ).popup("open");
}





function guardarEstilo(){

		//Se guardan los datos del formulario en el objeto
		estilo.dimension1 = $("#dimension1Estilo").val();
		estilo.dimension2 = $("#dimension2Estilo").val();
		estilo.dimension3 = $("#dimension3Estilo").val();
		estilo.dimension4 = $("#dimension4Estilo").val();
		estilo.tempo = $("#tiempoEstilo").val();
		estilo.estilo.chincheta = $("#chinchetaEstilo").prop("checked");
    estilo.estilo.icono = $("#iconoEstilo").val();
    estilo.estilo.etiqueta = $("#etiquetaEstilo").val();

    estilo.estilo.clases = [];
    for(var i = 0; i<clases_table.rows.all.length; i++){
      var row = clases_table.rows.all[i].val();
      var clase = {};
      if(row.id != null){
          clase.id = row.id;
      }
      clase.color = row.color[0].firstChild.value;
      clase.valor = row.valor[0].firstChild.value;
      clase.valor_max = row.valor_max[0].firstChild.value;
      clase.valor_min = row.valor_min[0].firstChild.value;
      estilo.estilo.clases.push(clase);
    }



		if(estado == "CREAR"){
					var message = JSON.stringify(estilo.estilo);
          message = message.replace(/""/g,"null");
					//Se envía petición para crear el objeto
					$.ajax({
            url:  contextPath+"/admin/estilo/"+"?token="+tokenId,
            data: message,
            processData: false,
            contentType: 'application/json',
            type: 'POST',
            success: function(data){
            	alert("El estilo se ha guardado correctamente");
	             //Se añade a la lista de todos los estilos
	             var rows = all_estilos.rows.all.map( function(r){return r.value});
	             var est = data;
	             rows.unshift(est);
	             estilos.push(est);
	             all_estilos.loadRows(rows);
            },
            error: function(error){
              alert("No ha sido posible realizar los cambios");
            }
          });


		}else if((estado == "EDITAR")){
				//Se actualiza sólo la tabla de estilos en el dato
        actualizarTablaEstilo(estilo);
        volverDato();
		}else if((estado == "ASOCIAR")){
				//Hay que pedir estiloClases, porque en la lista con todos los estilos no está
				obtenerEstiloClases(estilo);
				volverDato();
		}
		$( "#addNewEstilo" ).popup("close");
}


function mostrarEstilos(){
				var url = contextPath+"/admin/estilos/";

  			$.ajax({
  				url:  url,
  				processData: false,
  				contentType: 'application/json',
  				type: 'GET',
  				success: function(data){
  					estilos = data.estilos;
  					//Cargar la tabla con todos los datos
  					all_estilos.loadRows(estilos);

  					//Mostrar tabla con todos los estilos y ocultar el dato
  					$("#adminItem").hide();
  					$("#adminDato").hide();
            $("#addEstilo").show();
            $("#viewButtons").hide();
            $("#editButtons").hide();
            $("#datoButtons").hide();
            $("#returnDatoButtons").show();
  				},
  				error: function(error){
  					console.log(error)
  				}
  			});
}



function obtenerEstiloClases(estilo){
    $.ajax({
      url:  contextPath+"/admin/estilo/"+estilo.estilo.id+"/clases",
      processData: false,
      contentType: 'application/json',
      type: 'GET',
      success: function(data){
         estilo.estilo.clases = data.clases;
         actualizarTablaEstilo(estilo);
      },
      error: function(error){
        //$.mobile.changePage("#popupError");
        alert("No ha sido posible realizar los cambios");
      }
    });
}
function actualizarTablaEstilo(estilo){
	// hay que clonar la dimension, sino al añadir nuevos wms se modifican también los anteriores
	  var nuevo_estilo=new Object();
	  $.extend( true, nuevo_estilo,estilo);

	  nuevo_estilo.etiqueta = nuevo_estilo.estilo.etiqueta;	
	if(currentRowEstilo){
			
			
			currentRowEstilo.val(nuevo_estilo);
			for(var i = 0; i<dato.estilos.length; i++){
          if(dato.estilos[i].id == currentRowEstilo.value.id){
            dato.estilos[i] = nuevo_estilo;
          }
      }
		}else{
			//Crear recurso
			estilos_table.rows.add(nuevo_estilo);
			 dato.estilos.push(nuevo_estilo)
		}
}


function getHTMLInput(value,type){
	if(value!= null){
			return "<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'><input type='"+type+"' value='"+value+"'></input></div>";
	}else{
			return "<div class='ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset'><input type='"+type+"'></input></div>";
	}
}

function changeChincheta(){
	if ($("#chinchetaEstilo").prop("checked")){
		$(".notChincheta").addClass("oculto");
		$(".onlyChincheta").removeClass("oculto");
	}
	else{
		$(".onlyChincheta").addClass("oculto");

		$(".notChincheta").removeClass("oculto");
	}
}