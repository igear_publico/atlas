var subtema;
var nuevo;
var items;
var items_table;
var row_item;
var max_orden=1;
	function init(){
		mostrarEdicion();
		initSelect();

		items_table = FooTable.init('#items_table', {
      columns: [
        { "name": "id", "title": "ID.","visible": false},
        { "name": "titulo", "title": "Titulo","formatter": function(value, options, rowData){
			return " <a rel='external' href='"+contextPath+"/admin/item/"+rowData.id+"' >"+value+"</a>";
		}}        ,
		{ "name": "orden", "title": "Orden"}
      ]
      ,
      editing: {
        alwaysShow: true,
        allowEdit: true,
        enabled: true,
        addRow: function(){
          mostrarItems();
        },
		editRow: function(row){
			row_item=row;
		
			$("#orden").val(row.val().orden);
			$( "#editOrdenItem" ).popup("open");
		},
        deleteRow: function(row){
          //$("#item-"+row.value.id).remove();
          row.delete();
        },
        showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
        hideText: 'Cancelar',
        addText: 'Añadir item'
      }
    });


    items_table.loadRows(subtema.items);
    all_items = FooTable.init('#all_items', {
      columns: [
        { "name": "id", "title": "ID.","visible": false },
        { "name": "titulo", "title": "Titulo"},
        { "name": "añadir", "title": "Añadir","formatter": function(value, options, rowData){
            return " <a onclick=\"asociarItem('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
          }
        }
      ]
    });
    $("#addItem").popup();
    $("#editOrdenItem").popup();
	}

	function initSelect(){
		$.mobile.document
        .on( "listviewcreate", "#filter-menu-menu", function( e ) {
            var input,
                listbox = $( "#filter-menu-listbox" ),
                form = listbox.jqmData( "filter-form" ),
                listview = $( e.target );
            if ( !form ) {
                input = $( "<input data-type='search'></input>" );
                form = $( "<form></form>" ).append( input );
                input.textinput();
                $( "#filter-menu-listbox" )
                    .prepend( form )
                    .jqmData( "filter-form", form );
            }
            listview.filterable({ input: input });
        })
        .on( "pagebeforeshow pagehide", "#filter-menu-dialog", function( e ) {
            var form = $( "#filter-menu-listbox" ).jqmData( "filter-form" ),
                placeInDialog = ( e.type === "pagebeforeshow" ),
                destination = placeInDialog ? $( e.target ).find( ".ui-content" ) : $( "#filter-menu-listbox" );
            form
                .find( "input" )
                .textinput( "option", "inset", !placeInDialog )
                .end()
                .prependTo( destination );
        });

	}
	
	function visualizar(){
		if (subtema.tema){
		
				window.open(contextPath+"/"+subtema.etiqueta_atlas+"/info/"+subtema.tema.etiqueta+"/"+subtema.etiqueta);	
		
		}
		else{
			alert("Para poder ver el subtema debe incluirlo en algún tema");
		}
	}
	function previsualizar(){



			var cloneSubtema = Object.assign({}, subtema);


			cloneSubtema.titulo = $("#subtemaEditTitulo").val();
			cloneSubtema.orden = $("#subtemaEditOrden").val();
      cloneSubtema.items = [];

      for(var i = 0; i<items_table.rows.all.length; i++){
        var row = items_table.rows.all[i].val();
        cloneSubtema.items.push(row);
      }
      var selected = $("#filter-menu option:selected").val()
      if(selected == "---"){
        cloneSubtema.tema = {};
      }else{
        cloneSubtema.tema = JSON.parse(selected);
      }


			var cloneTema = Object.assign({}, cloneSubtema.tema);
			cloneTema.subtemas = [];
			cloneTema.subtemas.push(cloneSubtema);

		var message = JSON.stringify(cloneTema);
    message = message.replace(/""/g,"null");
		$.ajax({
    				url:  contextPath+"/admin/temaFragment/",
    				data: message,
    				processData: false,
    				type: 'POST',
    				contentType: 'application/json',
    				success: function(data){
    				  $("#content").html(data);
    				  mostrarVista();
    				},
    				error: function(error){
    					mostrarVista();
    				}
    			});
	}


		function mostrarItems(){
  			var url = contextPath+"/admin/items/list";

  			$.ajax({
  				url:  url,
  				processData: false,
  				contentType: 'application/json',
  				type: 'GET',
  				success: function(data){
  					items = data.items;
  					items = items.filter(s => !estaIncluido(s.id));
  					all_items.loadRows(items);
  					$( "#addItem" ).popup("open");
  				},
  				error: function(error){
  					console.log(error)
  				}
  			});
  	}


  	function estaIncluido(id){
  			for(var i = 0; i<items_table.rows.all.length; i++){
    			var row = items_table.rows.all[i].val();
    			if(row.id == id){
    			    return true;
    			}
    		}
    		return false;
  	}

  	function asociarItem(id){
  		for(var i = 0; i<items.length;i++){
  			if(items[i].id == id){
  			
  				var item = items[i];
  				max_orden++;
				items[i].orden =max_orden;
				items_table.rows.add(items[i]);
  				$( "#addItem" ).popup("close");
  			}
  		}
  	}



	function guardar(){
		subtema.titulo = $("#subtemaEditTitulo").val();
		subtema.orden = $("#subtemaEditOrden").val();
		subtema.items = [];

		for(var i = 0; i<items_table.rows.all.length; i++){
			var row = items_table.rows.all[i].val();
			var subtema_x_item=new Object();
			subtema_x_item.id=row.id_relation;
			subtema_x_item.id_item=row.id;
			subtema_x_item.orden=row.orden;
			subtema_x_item.id_subtema=subtema.id;
			subtema.items.push(subtema_x_item);
		}
		
		
		
		subtema.tema=null;
		
		var selected = $("#filter-menu option:selected").val()
		if(selected == "---"){
			subtema.id_tema = null;
		}else{
			subtema.id_tema = selected;
		}

		var message = JSON.stringify(subtema);
    message = message.replace(/""/g,"null");

		if(subtema.id){
			//Editar subtema
			$.ajax({
				url:  contextPath+"/admin/subtema/"+subtema.id+"?token="+tokenId,
				data: message,
				processData: false,
				contentType: 'application/json',
				type: 'PUT',
				success: function(data){
					alert("El subtema se ha guardado correctamente");
					location.reload();
				},
				error: function(error){
					//$.mobile.changePage("#popupError");
					alert("No ha sido posible realizar los cambios");
				}
			});
		}else{
			//Crear subtema
			$.ajax({
				url:  contextPath+"/admin/subtema/"+"?token="+tokenId,
				data: message,
				processData: false,
				contentType: 'application/json',
				type: 'POST',
				success: function(data){
					alert("El subtema se ha guardado correctamente");
					location.href=contextPath+'/admin/subtema/'+data.id;
				},
				error: function(error){
					//$.mobile.changePage("#popupError");
					alert("No ha sido posible realizar los cambios");
				}
			});
		}
	}
	function guardarOrden(){
		var values = row_item.val();
		values.orden = $("#orden").val();
		row_item.val(values); 
		max_orden = Math.max(max_orden,values.orden);
		$( "#editOrdenItem" ).popup("close");
	}

	function cancel(){
		location.href=contextPath+'/admin/subtemas';
	}