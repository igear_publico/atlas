var tokenId;
	function mostrarEdicion(){
		$("#itemEdit").show();
		$("#editButtons").show();
		$(".editButtons").show();
		$("#content").hide();
		$("#viewButtons").hide();
		if($("textarea").length>0){
			$("textarea").height( $("textarea")[0].scrollHeight );
		}
	}

	function mostrarVista(){
		$("#itemEdit").hide();
		$("#editButtons").hide();
		$(".editButtons").hide();
		$("#content").show();
		$("#viewButtons").show();
	}

	function cancel(){
		location.href=contextPath+'/admin/';
	}

	function volver(){
		location.href=contextPath+'/admin/';
	}
	function goToInicio(){
		location.href=contextPath+'/admin/login';
	}

	function sinEscapar(text){
		if(text){
				text = text.replace(/&lt;/g,"<");
				text = text.replace(/&gt;/g,">");
				text = text.replace(/&quot;/g,"\"");
				text = text.replace(/&amp;/g,"&");
		}
		return text;
	}

	function estaIncluido(table,id){
			for(var i = 0; i<table.rows.all.length; i++){
  			var row = table.rows.all[i].val();
  			if(row.id == id){
  			    return true;
  			}
  		}
  		return false;
	}


	function toJSON(seccion){
		var json=new Object();
		
		$("#"+seccion+" input, #"+seccion+" select, #"+seccion+" textarea").not(".notInJSONCopy").each(function(index){
			var id = $( this )[0].id;
			if ($("#"+id).attr("type")=="checkbox"){
				json[id]=$("#"+id).prop("checked");
			}
			else{
				json[id]=$("#"+id).val();
			}
		});
		$("#"+seccion+" select").not(".notInJSONCopy").each(function(index){
			var id = $( this )[0].id;
			json[id]=$("#"+id).val();
			json[id+"_seloption"]=$("#filter-menu option:selected").text();
			
		});
		$("#"+seccion+" span.includeInJSONCopy").not(".notInJSONCopy").each(function(index){
			var id = $( this )[0].id;
			json[id]=$("#"+id).text();
			
			
		});
		$("#"+seccion+" table.footable").not(".notInJSONCopy").each(function(index){
			var filas = new Array();
			var id = $( this )[0].id;
			var tabla = eval(id);
			var keepIds = $("#"+id).hasClass("keepIdsInJSONCopy");
			var sonInput = $("#"+id).hasClass("cellsInput");
			for (var i=0; i<tabla.rows.all.length; i++){
				var fila = new Object();
				if (sonInput){
					var campos =Object.keys(tabla.rows.all[i].value);
					for (var j=0; j<campos.length;j++){
						fila[campos[j]]=new Object();
						try{
						fila[campos[j]].type = tabla.rows.all[i].value[campos[j]][0].firstChild.type;
						fila[campos[j]].value = tabla.rows.all[i].value[campos[j]][0].firstChild.value;
						}
						catch(err){
							// no es input
							fila[campos[j]].value = tabla.rows.all[i].value[campos[j]];
						}
					}
				}
				else{
					fila = JSON.parse(JSON.stringify(tabla.rows.all[i].value)); // clonado
				}
				fila.editing=null;
				if (!keepIds){
					fila.id=null;
				}
				fila.id_relation=null;  // en principio nunca habrá 
				filas.push(fila);
			}
			json[id]=filas;
		});
		return JSON.stringify(json);
		
	}
	
	function fillFromJSON(seccion,json){
		var datos = JSON.parse(json);
		var campos = Object.keys(datos);
		for (var i=0; i<campos.length; i++){
			var campo = campos[i];
			if (!$("#"+campo).hasClass("notInJSONPaste")){
				if ($("#"+campo).hasClass("footable")){
					var filas = datos[campo];
					var tabla = eval(campo);
					var sonInput = $("#"+campo).hasClass("cellsInput");
					if (sonInput){
						 var rows = [];
						for (var k=0; k<filas.length;k++){
							var row = new Object();
							var campos =Object.keys(filas[k]);
							for (var j=0; j<campos.length;j++){
								try {
								row[campos[j]] = getHTMLInput(filas[k][campos[j]].value,filas[k][campos[j]].type);
								}
								catch(err){
									// no es input
									row[campos[j]]=filas[k][campos[j]];
								}
								
							}
							rows.push(row);
						}
						tabla.loadRows(rows);
					}
					else{
						tabla.loadRows(filas);
						if (campo=="datos_table"){  // ad-hoc para guardar los datos del item
							item.datos=filas;
						}
					}
				}
				else if ($("#"+campo).attr("type")=="checkbox"){
					$("#"+campo).prop("checked",datos[campo]).checkboxradio('refresh');
				}

				else if (tinymce.get(campo)){
					tinymce.get(campo).setContent(datos[campo]);
				}
				else{
					$("#"+campo).val(datos[campo]);
					if ($("#"+campo+" option").length>0){
						$("#"+campo+"_paste").text(datos[campo+"_seloption"]);
						try{
							$("#"+campo).selectmenu("refresh");
						}
						catch(err){

						}
					}
				}
			}
			else{

				if (Array.isArray(datos[campo])){
					$("#"+campo+"_paste").empty();
					$("#"+campo+"_paste").append("<ul></ul>");
					for (var j=0; j< datos[campo].length; j++){
						$("#"+campo+"_paste ul").append("<li>"+JSON.stringify(datos[campo][j])+"</li>");
					}
				}
				else{
					$("#"+campo+"_paste").text(JSON.stringify(datos[campo]));	
				}
				
			}
		}
		$(".showInPaste").removeClass("oculto");
		if (seccion=="adminDato"){// ad-hoc para mostrar/ocultar elementos en fucnión de valores checkbox en dato
			configDato();
		}
		else if (seccion=="itemEdit"){
			try{
			updUdTempComentario();
			}
			catch(err){
				// no es item
			}
		}
	}

	function copiar(seccion){
		var json = toJSON(seccion);
		console.log(json);
		navigator.clipboard.writeText(json);
	}
	
	function pegar(seccion,json){
		
		var json = $("#"+json).val()
				 

		fillFromJSON(seccion,json);
	}
	
	function superAdmin(){
		$(".superAdmin textarea").val("");
		$(".superAdmin").removeClass("oculto");
	}
	

	function compruebaUsr() {
		showLoading(true);
		var newUsuario = document.getElementById('login').value;
		var newPasswd = document.getElementById('passwd').value;

		if ((newUsuario == "") || (newPasswd == "")) {
			alert("Debe introducir obligatoriamente los campos de nombre de usuario y contrase&ntilde;a.");
			
		} else {
			var url = '/gestorUsuarios/gestorUsuariosIGEAR?REQUEST=LOGIN&us=' + newUsuario + '&cl=' + encodeURIComponent(encryptPwd(newPasswd));
			$("#btnEnviar").button( "disable");

			$.ajax({
				url: url,
				type: 'GET',
				success: function(data) {
							tokenId=data;	
							writeLoginCookieIGEAR(data);
							hasAdminRol(true);
							
				},
				error: function(data, textStatus, errorThrown) {
					showLoading(false);
					if (textStatus =='timeout'){
					
							alert("Se ha superado el tiempo de espera para obtener respuesta sobre la operación solicitada");
						
					}
					else if (data.status==403){
						alert('Usuario y contraseña no válidos')
						
					}
					else{
					alert("No se ha podido validar su usuario. Por favor, inténtelo más adelante.");
					console.log("No se pudo validar el usuario. Respuesta servicio:"+data.responseText);
					}
					$("#btnEnviar").button( "enable");
					
				}
			});
		}
	}
	
	function showLoading(show) {
		if (show){
			$("#loadingOverlay").show();
		}
		else{
			$("#loadingOverlay").hide();
		}
	}
	
	function setLogged(newUsuario){

			document.getElementById("nomUsrConnected").innerHTML = "Conectado como usuario " + newUsuario;
			document.getElementById("connection").innerHTML = "Cerrar sesión";
			document.getElementById("connection").title = "Cerrar sesión";


		$("#login").css("display","none");
		$("#logout").css("display","block");

	}
	
	function hasAdminRol(goToAdmin){
		var url = '/gestorUsuarios/gestorUsuariosIGEAR?REQUEST=HASROL&TOKEN=' + tokenId+"&rol=ROL_G_ATLAS";
		var tiene=false;
		$.ajax({
			url: url,
			type: 'GET',
			async: false,
			success: function(data) {
				console.log("El usuario tiene permiso de administración");
				if (goToAdmin){
					volver();
				}
				else{
					showLoading(false);
				}
						
					
			},
			error: function(data, textStatus, errorThrown) {
				showLoading(false);
				if (data.status==403){
				alert("El usuario no tiene permiso de administración del atlas");
				}
				else{
					alert("No se pudo comprobar el rol el usuario. ");
				console.log("No se pudo comprobar el rol el usuario. Respuesta servicio:"+data.responseText);
				}
				doLogoutIGEAR();
			}
		});
	
	}
