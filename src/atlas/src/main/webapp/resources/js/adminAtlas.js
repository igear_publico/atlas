var temas;
var temas_table;
var all_temas;
var row_tema;
var max_orden=1;

	

	function init(){
		mostrarEdicion();

		tinymce.init({
			selector: '#atlasEditResumen',
      menubar: false
		});


		temas_table = FooTable.init('#temas_table', {
			columns: [
				{ "name": "id", "title": "ID.","visible": false},
				{ "name": "id_relation", "title": "id_relation","visible": false},
				{ "name": "nombre", "title": "Nombre","formatter": function(value, options, rowData){
					return " <a rel='external' href='"+contextPath+"/admin/tema/"+rowData.id+"' >"+value+"</a>";
				}},
				{ "name": "orden", "title": "Orden"},
			]
			,
			editing: {
				alwaysShow: true,
				allowEdit: true,
				enabled: true,
				addRow: function(){
					mostrarTemas();
				},
				editRow: function(row){
					row_tema=row;
				
					$("#orden").val(row.val().orden);
					$( "#editOrdenTema" ).popup("open");
				},
				deleteRow: function(row){
					$("#tema-"+row.value.id).remove();
					row.delete();
				},
				showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
				hideText: 'Cancelar',
				addText: 'Añadir tema'
			}
		});

		temas_table.loadRows(atlas.temas);
		if (atlas.temas.length>0){
			max_orden = atlas.temas[atlas.temas.length-1].orden;
		}

		all_temas = FooTable.init('#all_temas', {
			columns: [
				{ "name": "id", "title": "ID.","visible": false},
				{ "name": "nombre", "title": "Nombre"},
				{ "name": "añadir", "title": "Añadir","formatter": function(value, options, rowData){
						return " <a onclick=\"asociarTema('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
					}
				}
			]
		});


		$("#addTema").popup();
		$("#editOrdenTema").popup();

	}



	function visualizar(){
		window.open(contextPath+"/"+atlas.etiqueta);
	}

	function previsualizar(){
		$("#atlasTitulo").text($("#atlasEditTitulo").val());
		$("#atlasResumen").html(sinEscapar(tinymce.activeEditor.getContent()));
		mostrarVista();
	}


	function guardar(){
		atlas.titulo = $("#atlasEditTitulo").val();
		atlas.resumen = sinEscapar(tinymce.activeEditor.getContent());
		atlas.etiqueta = $("#atlasEditEtiqueta").val();
		atlas.temas = [];

		for(var i = 0; i<temas_table.rows.all.length; i++){
			var row = temas_table.rows.all[i].val();
			var atlas_x_tema=new Object();
			atlas_x_tema.id=row.id_relation;
			atlas_x_tema.id_tema=row.id;
			atlas_x_tema.orden=row.orden;
			atlas_x_tema.id_atlas=atlas.id;
			atlas.temas.push(atlas_x_tema);
		}


		var message = JSON.stringify(atlas);
		message = message.replace(/""/g,"null");

		if(atlas.id){
			//Editar atlas
			$.ajax({
				url:  contextPath+"/admin/atlas/"+atlas.id+"?token="+tokenId,
				data: message,
				processData: false,
				contentType: 'application/json',
				type: 'PUT',
				success: function(data){
					alert("Se han guardado los cambios correctamente");
					location.reload();
				},
				error: function(error){
					//$.mobile.changePage("#popupError");
					alert("No ha sido posible realizar los cambios");
				}
			});
		}else{
			//Crear atlas
			$.ajax({
				url:  contextPath+"/admin/atlas/?token="+tokenId,
				data: message,
				processData: false,
				contentType: 'application/json',
				type: 'POST',
				success: function(data){
					alert("El atlas se ha guardado correctamente");
					location.href=contextPath+'/admin/atlas/'+data.id;
				},
				error: function(error){
					//$.mobile.changePage("#popupError");
					alert("No ha sido posible realizar los cambios");
				}
			});
		}
	}


	function estaIncluido(id){
			for(var i = 0; i<temas_table.rows.all.length; i++){
  			var row = temas_table.rows.all[i].val();
  			if(row.id == id){
  			    return true;
  			}
  		}
  		return false;
	}

	function mostrarTemas(){


			var url = contextPath+"/admin/temas/list";

			/**
			if(atlas.id){
				url = "/atlas/admin/atlas/"+atlas.id+"/temas/"
			}*/
			$.ajax({
				url:  url,
				processData: false,
				contentType: 'application/json',
				type: 'GET',
				success: function(data){
					temas = data.temas;
					temas = temas.filter(t => !estaIncluido(t.id));
					all_temas.loadRows(temas);
					$( "#addTema" ).popup("open");
				},
				error: function(error){
					console.log(error)
				}
			});
	}



	function guardarOrden(){
		var values = row_tema.val();
		values.orden = $("#orden").val();
		row_tema.val(values); 
		max_orden = Math.max(max_orden,values.orden);
		$( "#editOrdenTema" ).popup("close");
	}
	function asociarTema(id){
		for(var i = 0; i<temas.length;i++){
			if(temas[i].id == id){
				max_orden++;
				temas[i].orden =max_orden;
				temas_table.rows.add(temas[i]);
				var tema = temas[i];
				$("#lista-temas").append("<li id='tema-"+tema.id+"'><a rel='external'  class='ui-link'>"+(tema.tipo_icono=='fa'?"<i class='"+tema.icono+"'></i>":"<i src='"+tema.icono+"'></i>")+"<div class='item-tema'>"+tema.nombre+"</div></a></li>");
				$( "#addTema" ).popup("close");
			}
		}
	}
