var item;
var nuevo;
var recurso = {};
var datos;
var dato;
var fuentes_table;
var recursos_table;
var datos_table;
var subtemas_table;
var all_subtemas;
var currentRow;
var fuentes;
var uds_terr;
var uds_temp;
var all_ud_terr;
var all_uds_temp;
var asociando;


function init(){


	var linkElement = document.createElement("link");
	linkElement.rel = "stylesheet";
	linkElement.href = "/lib/jquery/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.css"; //Replace here
	document.head.appendChild(linkElement);

	dato = {};
	dato.mapas_wms = [];
	dato.estilos = [];
	dato.dimensiones = [];






	mostrarEdicion();

	tinymce.init({
		selector: '#itemEditComentario',
		menubar: false,
		entity_encoding: "raw"
	});


	tinymce.init({
		selector: '#fuenteEdit',
		height : "300",
		menubar: false
	});

	if(!item.fuente.id){
		$("#botonEditarFuente").hide();
	}


	recursos_table = FooTable.init('#recursos_table', {
		columns: [
		          { "name": "id", "title": "ID.","visible": false,},
		          { "name": "descripcion", "title": "Descripcion"},
		          { "name": "url", "title": "url.","visible": false},
		          { "name": "id_tipo", "title": "tipo.","visible": false },
		          { "name": "opciones", "title": "opciones.","visible": false },
		          { "name": "opciones_etiquetas", "title": "opciones_etiquetas.","visible": false }
		          ]
	,
	editing: {
		alwaysShow: true,
		allowEdit: true,
		enabled: true,
		addRow: function(){
			currentRow = null;
			abrirFormularioRecurso();
		},
		editRow: function(row){
			currentRow = row;
			abrirFormularioRecurso();
		},
		deleteRow: function(row){
			$("#recurso-"+row.value.id).remove();
			row.delete();
		},
		showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
		hideText: 'Cancelar',
		addText: 'Añadir recurso'
	}
	});
	recursos_table.loadRows(item.recursos);

	ud_temporales_table = FooTable.init('#ud_temporales_table', {
		columns: [
		          { "name": "id", "title": "Ud. temporal" ,"visible":false      },
		          { "name": "etiqueta", "title": "Ud. temporal"},
		          { "name": "formato_datos", "title": "Formato datos","breakpoints": "xs sm" },
		          { "name": "formato_presentacion", "title": "Formato presentación","breakpoints": "xs sm" }
		          
		          ]
	,
	editing: {
		 alwaysShow: true,
         allowEdit: false,
         enabled: true,
         addRow: function(){
        		$( "#vincularUdTemp" ).popup("open");
         },
         deleteRow: function(row){
           row.delete();
         },
		showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
		hideText: 'Cancelar',
		addText: 'Añadir ud. temporal'
	}
	});

	    ud_temporales_table.loadRows(item.uds_temporales);


	subtemas_table = FooTable.init('#subtemas_table', {
		columns: [
		          { "name": "id", "title": "ID.","visible": false },
		          { "name": "titulo", "title": "Titulo","formatter": function(value, options, rowData){
		        	  return " <a rel='external' href='"+contextPath+"/admin/subtema/"+rowData.id+"' >"+value+"</a>";
		          }},
		          { "name": "nombre_tema", "title": "Tema"}
		          ]
	/*,
			editing: {
				alwaysShow: true,
				allowEdit: false,
				enabled: true,
				addRow: function(){
					mostrarSubtemas();
				},
				deleteRow: function(row){
					row.delete();
				},
				showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
				hideText: 'Cancelar',
				addText: 'Añadir subtema'
			}*/
	});
	subtemas_table.loadRows(item.subtemas);


	fuentes_table = FooTable.init('#fuentes_table', {
		columns: [
		          { "name": "id", "title": "ID.","visible": false },
		          { "name": "txt", "title": "Texto"},
		          { "name": "añadir", "title": "Añadir","formatter": function(value, options, rowData){
		        	  return " <a onclick=\"asociarFuente('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
		          }
		          }
		          ],
		          editing: {
		        	  alwaysShow: true,
		        	  allowEdit: false,
		        	  allowDelete: false,
		        	  enabled: true,
		        	  addRow: function(){
		        		  editarFuente(true);
		        	  },
		        	  showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
		        	  hideText: 'Cancelar',
		        	  addText: 'Añadir nueva fuente'
		          }
	});

	all_subtemas = FooTable.init('#all_subtemas', {
		columns: [
		          { "name": "id", "title": "ID.","visible": false},
		          { "name": "titulo", "title": "Titulo"},
		          { "name": "nombre_tema", "title": "Tema","breakpoints": "xs sm"},
		          { "name": "añadir", "title": "Añadir","formatter": function(value, options, rowData){
		        	  return " <a onclick=\"asociarSubtema('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
		          }
		          }
		          ]
	});

	$("#addSubtema").popup();



	datos_table = FooTable.init('#datos_table', {
		columns: [
		          { "name": "id", "title": "ID.","visible": false },
		          { "name": "mapa_vectorial", "title": "ID.","visible": false},
		          { "name": "mapa_vectorial_orden", "title": "ID.","visible": false},
		          { "name": "mapa_wms", "title": "ID.","visible": false},
		          { "name": "grafico", "title": "ID.","visible": false},
		          { "name": "ud_medida", "title": "ID.","visible": false},
		          { "name": "tabla_conex", "title": "ID.","visible": false},
		          { "name": "tabla_datos", "title": "Tabla de datos","breakpoints": "xs sm"},
		          { "name": "tabla_filtro", "title": "Filtro","breakpoints": "xs sm"},
		          { "name": "id_ud_temporal", "title": "ID.","visible": false},
		          { "name": "id_ud_territorial", "title": "ID.","visible": false},
		          { "name": "ud_terr_label", "title": "Ud. Territorial"},
		          { "name": "ud_temp_label", "title": "Ud. Temporal"},
		          { "name": "error", "title": "Avisos/Errores","formatter": function(value, options, rowData){
		        	  return (value ? "<i title=\""+value+"\" class=\"fa fa-exclamation-triangle\"></i>":"");
		          }
		          }
		          ]
	,
	editing: {
		alwaysShow: true,
		allowEdit: true,
		enabled: true,
		allowView: true,
		viewRow: function(row){
			currentRow = row;
			abrirFormularioDato(true);
		},
		addRow: function(){
			currentRow = null;
			abrirFormularioDato();
		},
		editRow: function(row){
			currentRow = row;
			abrirFormularioDato();
		},
		deleteRow: function(row){
			if(row.value.id){
				var index;
				for(var i = 0; i<item.datos.length;i++){
					if(item.datos[i].id == row.value.id){
						index = i;
					}
				}
				item.datos.splice(index, 1);
			}
			row.delete();
		},
		showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
		hideText: 'Cancelar',
		addText: 'Añadir Dato'
	}
	});

	for (var i=0; i<item.datos.length; i++){
		
		item.datos[i].error = getErroresDato(item.datos[i]);
		var dato = item.datos[i];
		if (dato.ud_temporal){
			dato.id_ud_temporal=dato.ud_temporal.id;
			dato.ud_temp_label=dato.ud_temporal.etiqueta;
		}
		if (dato.ud_territorial){
			dato.id_ud_territorial=dato.ud_territorial.id;
			dato.ud_terr_label=dato.ud_territorial.etiqueta;
		}
	}
	datos_table.loadRows(item.datos);



	initDimension();

//	initMapa();



}


function visualizar(){
	if (item.etiqueta_subtema){
			
		if (item.etiqueta_atlas ){
					window.open(contextPath+"/"+item.etiqueta_atlas+"/info/"+item.etiqueta_tema+"/"+item.etiqueta_subtema+"/"+item.etiqueta);
					
				}
		else{
			alert("Para poder ver el item debe incluirlo en algún subtema perteneciente a algún tema inlcuído en al menos un atlas");
		}	
	}
	else{
		alert("Para poder ver el item debe incluirlo en algún subtema");
	}
	
}
function previsualizar(){
	var cloneItem = Object.assign({}, item);
	cloneItem.titulo = $("#itemEditTitulo").val();
	cloneItem.comentario = sinEscapar(tinymce.get('itemEditComentario').getContent());
	cloneItem.es_sql = $("#itemEditEsSql").prop("checked");
	cloneItem.destacar = $("#itemEditDestacar").prop("checked");
	cloneItem.fuente = {}
	cloneItem.fuente.txt = $("#itemEditFuente").html()

	cloneItem.recursos = [];

	cloneItem.subtemas = [];


	for(var i = 0; i<recursos_table.rows.all.length; i++){
		var row = recursos_table.rows.all[i].val();
		cloneItem.recursos.push(row);
	}

	var message = JSON.stringify(cloneItem);
	message = message.replace(/""/g,"null");
	$.ajax({
		url:  contextPath+"/admin/itemFragment/",
		data: message,
		processData: false,
		type: 'POST',
		contentType: 'application/json',
		success: function(data){
			$("#content").html(data).trigger( "create" );
			mostrarVista();
			initMapPage();
		},
		error: function(error){
			mostrarVista();
		}
	});

}

function guardar(){
	item.titulo = $("#itemEditTitulo").val();
	item.comentario = sinEscapar(tinymce.get('itemEditComentario').getContent());
	item.es_sql = $("#itemEditEsSql").prop("checked");
	item.destacar = $("#itemEditDestacar").prop("checked");
	item.fuente.txt = $("#itemEditFuente").html()
	item.recursos = [];


	for(var i = 0; i<recursos_table.rows.all.length; i++){
		var row = recursos_table.rows.all[i].val();
		item.recursos.push(row);
	}

	item.subtemas = [];

	for(var i = 0; i<subtemas_table.rows.all.length; i++){
		var row = subtemas_table.rows.all[i].val();
		row.tema=null;
		item.subtemas.push(row);
	}

	item.uds_temporales = [];
    for(var i = 0; i<ud_temporales_table.rows.all.length; i++){
      var row = ud_temporales_table.rows.all[i].val();
      row.editing=null;
          item.uds_temporales.push(row);
    }
	console.log(item);

	var message = JSON.stringify(item);
	message = message.replace(/""/g,"null");
	if(item.id){
		//Editar item
		$.ajax({
			url:  contextPath+"/admin/item/"+item.id+"?token="+tokenId,
			data: message,
			processData: false,
			contentType: 'application/json',
			type: 'PUT',
			success: function(data){
				alert("El item se ha guardado correctamente");
				location.reload();
		//		previsualizar();
			},
			error: function(error){
				//$.mobile.changePage("#popupError");
				alert("No ha sido posible realizar los cambios");
			}
		});
	}else{
		//Crear item
		$.ajax({
			url:  contextPath+"/admin/item/"+"?token="+tokenId,
			data: message,
			processData: false,
			contentType: 'application/json',
			type: 'POST',
			success: function(data){
				alert("El item se ha guardado correctamente");
				location.href=contextPath+'/admin/item/'+data.id;
			//	previsualizar();
			},
			error: function(error){
				//$.mobile.changePage("#popupError");
				alert("No ha sido posible realizar los cambios");
			}
		});
	}
}

function editarFuente(esNuevo){
	nuevo = esNuevo;
	if(nuevo){
		tinymce.get('fuenteEdit').setContent('');
		$( "#advertenciaFuente").hide();
	}else{
		tinymce.get('fuenteEdit').setContent(item.fuente.txt);
		$( "#advertenciaFuente").show();
	}
	$( "#editFuente" ).popup("open", {positionTo: 'window'});
}

function guardarFuente(){
	if(nuevo){
		var fuente = {};
		fuente.txt = sinEscapar(tinymce.get('fuenteEdit').getContent());

		var message = JSON.stringify(fuente);
		message = message.replace(/""/g,"null");
		$.ajax({
			url:  contextPath+"/admin/fuente/"+"?token="+tokenId,
			data: message,
			processData: false,
			contentType: 'application/json',
			type: 'POST',
			success: function(data){
				alert("La fuente se ha guardado correctamente");
				item.fuente = data;
				$("#itemEditFuente").html(data.txt)
				volverItem();
			},
			error: function(error){
				//$.mobile.changePage("#popupError");
				alert("No ha sido posible realizar los cambios");
			}
		});

	}else{
		item.fuente.txt = sinEscapar(tinymce.get('fuenteEdit').getContent());
		$("#itemEditFuente").html(item.fuente.txt);
	}
	$("#botonEditarFuente").show();
	$( "#editFuente" ).popup("close");
}

function mostrarFuentes(){

	var url = contextPath+"/admin/fuentes/";

	$.ajax({
		url:  url,
		processData: false,
		contentType: 'application/json',
		type: 'GET',
		success: function(data){
			fuentes = data.fuentes;
			fuentes_table.loadRows(fuentes);
			$("#adminItem").hide();
			$("#addFuente").show();

			$("#viewButtons").hide();
			$("#editButtons").hide();
			$("#returnButtons").show();
		},
		error: function(error){
			console.log(error)
		}
	});
}

function asociarFuente(id){
	for(var i = 0; i<fuentes.length;i++){
		if(fuentes[i].id == id){
			item.fuente = fuentes[i];
			$("#itemEditFuente").html(fuentes[i].txt);
			$("#botonEditarFuente").show();
			volverItem();
		}
	}
}



function guardarRecurso(){
	recurso.id_tipo = $("#tipoRecurso").val();
	recurso.url = $("#urlRecurso").val();
	recurso.descripcion = $("#descripcionRecurso").val();
	//recurso.id_item = item.id;
	if(currentRow){
		//Editar recurso
		currentRow.val(recurso);
	}else{
		//Crear recurso
		recursos_table.rows.add(recurso);
	}
	$( "#addRecurso" ).popup("close");
}

function abrirFormularioRecurso(){
	if(currentRow){
		$("#descripcionRecurso").val(currentRow.value.descripcion);
		$("#urlRecurso").val(currentRow.value.url);
		$("#tipoRecurso").val(currentRow.value.id_tipo);
	}else{
		recurso = {};
		$("#descripcionRecurso").val('');
		$("#urlRecurso").val('');
		$("#tipoRecurso").val('');
	}
	$("#tipoRecurso").selectmenu("refresh")
	$( "#addRecurso" ).popup("open");
}



function abrirFormularioDato(esCopia){
	mostrarEdicionDato();


	if(currentRow){

		for(var i = 0; i<item.datos.length; i++){
			if(item.datos[i].id == currentRow.value.id){
				if (esCopia){
					dato=new Object();
					  $.extend( true, dato,item.datos[i]);		
				}
				else{
				dato = item.datos[i];
				}
			}
		}
		if (esCopia){
			currentRow=null;
			$("#datoId").val('');
			 dato.id=null;
		}
		else{
			$("#datoId").val(dato.id);	
		}
		
		$("#ud_medida").val(dato.ud_medida);
		$("#mapa_vectorial_orden").val(dato.mapa_vectorial_orden);
		$("#datoEditTablaDatos").val(dato.tabla_datos);
		$("#datoEditTablaFiltro").val(dato.tabla_filtro);
		$("#datoEditTablaConexion").val(dato.tabla_conex);
		$("#datoEditVectorial").prop('checked', dato.mapa_vectorial).checkboxradio('refresh');
		$("#datoEditWMS").prop('checked', dato.mapa_wms).checkboxradio('refresh');
		$("#datoEditNormalizada").prop('checked', dato.tabla_normalizada).checkboxradio('refresh');
		$("#grafico").prop('checked', dato.grafico).checkboxradio('refresh');


		if(dato.ud_temporal){
			$("#datoEditUdTemporal").val($("#udTemp_"+dato.ud_temporal.id).val());
		}
		if(dato.ud_territorial){
			$("#datoEditUdTerritorial").val($("#udTerr_"+dato.ud_territorial.id).val());
		}


		$("#datoEditUdTerritorial").trigger("change");
		

		loadDimensiones(esCopia);
		initMapa();
		initMapaWMSAux();
		$("#datoEditUdTemporal").trigger("change");
		configDato();
		if (dato.mapas_wms){
			var mapas_imprimir=new Array();
			var mapas_wms_var=new Array();
			var mapas_aux=new Array();
			for (var i=0; i<dato.mapas_wms.length;i++){
				var wms = dato.mapas_wms[i];
				if (esCopia){
					wms.id=null;
				}
				wms.capas=wms.mapa.capas;
				wms.etiqueta=wms.mapa.etiqueta;
				if (wms.solo_imprimir){
					mapas_imprimir.push(wms);
				}
				else if(wms.tempo || wms.dimension1|| wms.dimension2|| wms.dimension3|| wms.dimension4){
					mapas_wms_var.push(wms);
				}
				else{
					mapas_aux.push(wms);
				}
					
			}
			mapaswms_imprimir_table.loadRows(mapas_imprimir);
				mapaswms_table.loadRows(mapas_wms_var);
				mapaswms_aux_table.loadRows(mapas_aux);
		}
		
		if (dato.estilos){
			for (var i=0;i<dato.estilos.length;i++){
				dato.estilos[i].etiqueta = dato.estilos[i].estilo.etiqueta;
				if (esCopia){
					dato.estilos[i].id=null;
				}
			}
		}
		estilos_table.loadRows(dato.estilos);
		
		
	}else{
		dato = {};
		dato.mapas_wms = [];
		dato.estilos = [];
		dato.dimensiones = [];
		$("#datoId").val('');
		$("#datoEditTablaDatos").val('');
		$("#datoEditTablaFiltro").val('');
		$("#datoEditTablaConexion").val('');
		$("#datoEditVectorial").prop('checked', false).checkboxradio('refresh');
		$("#datoEditWMS").prop('checked', false).checkboxradio('refresh');
		$("#datoEditNormalizada").prop('checked', false).checkboxradio('refresh');
		$("#datoEditUdTemporal").val('');
		$("#datoEditUdTerritorial").val('');
		initMapa();
		initMapaWMS();
		initMapaWMSImprimir();
		initMapaWMSAux();
		configDato();
	}

}

function configDato(){
	changeVectorial();
	changeWMS();
}
function getErroresDato(dato){
	var error="";
	if (dato.mapa_vectorial){
		if (!dato.mapa_vectorial_orden){
			error+="Está marcado como mapa vectorial y el orden está vacío. ";
		}
		if (dato.estilos.length<=0){
			error+="Está marcado como mapa vectorial pero no hay estilo definido para pintar el mapa. ";
		}
		else if (dato.mapas_wms){
			var solo_imprimir=false;
			for (var i=0; i<dato.mapas_wms.length; i++){
				if (dato.mapas_wms[i].solo_imprimir){
					solo_imprimir=true;
					break;
				}
			}
			if (!solo_imprimir){
				error+="El mapa está configurado en vectorial pero no hay ningún WMS definido para la impresión y la leyenda. ";
			}
		}
		if (!dato.tabla_conex || !dato.tabla_datos){
			error+="Está marcado como mapa vectorial pero no se ha definido la conexión y la tabla de la que obtener los datos. ";
		}
	}
	if (dato.grafico){
		if (!dato.tabla_conex || !dato.tabla_datos){
			error+="Está configurado para que se incluya gráfico pero no se ha definido la conexión y la tabla de la que obtener los datos. ";
		}
	}
	if (dato.tabla_normalizada){
		if (!dato.tabla_conex || !dato.tabla_datos){
			error+="Está configurado para que se incluya tabla normalizada pero no se ha definido la conexión y la tabla de la que obtener los datos. ";
		}
	}
	return error;
}
function guardarDato(){
	dato.ud_medida = $("#ud_medida").val();
	dato.mapa_vectorial_orden = $("#mapa_vectorial_orden").val();
	dato.tabla_datos = $("#datoEditTablaDatos").val();
	dato.tabla_filtro = $("#datoEditTablaFiltro").val();
	dato.tabla_conex = $("#datoEditTablaConexion").val();
	dato.mapa_vectorial = $("#datoEditVectorial").prop("checked");

	dato.mapa_vectorial = $("#datoEditVectorial").prop("checked");
	dato.mapa_wms = $("#datoEditWMS").prop("checked");
	dato.tabla_normalizada = $("#datoEditNormalizada").prop("checked");
	dato.grafico = $("#grafico").prop("checked");
	dato.ud_temporal = $("#datoEditUdTemporal").val() != '' ? JSON.parse($("#datoEditUdTemporal").val()) : null;
	dato.ud_territorial = $("#datoEditUdTerritorial").val() != '' ? JSON.parse($("#datoEditUdTerritorial").val()) : null;
	dato.error = getErroresDato(dato);
	if (dato.ud_temporal){
		dato.id_ud_temporal=dato.ud_temporal.id;
		dato.ud_temp_label=dato.ud_temporal.etiqueta;
	}
	if (dato.ud_territorial){
		dato.id_ud_territorial=dato.ud_territorial.id;
		dato.ud_terr_label=dato.ud_territorial.etiqueta;
	}
   
	if(currentRow){
		//Editar dato
		currentRow.val(dato);

		for(var i = 0; i<item.datos.length; i++){
			if(item.datos[i].id == currentRow.value.id){
				item.datos[i] = dato;
			}
		}
	}else{
		//Crear recurso
		datos_table.rows.add(dato);
		item.datos.push(dato)
	}
	volverItem();
}

function abrirFormularioUdTerritorial(){
	$("#etiquetaUdTerritorial").val('');
	$("#geojsonUdTerritorial").val('');
	$("#campoCodigoUdTerritorial").val('');
	$("#srsUdTerritorial").val('');
	$( "#addUTerritorial" ).popup("open");

}


function guardarUdTerritorial(){
	console.log("guardarUdTerritorial")

	var terr = {};

	terr.etiqueta = $("#etiquetaUdTerritorial").val();
	terr.geojson = $("#geojsonUdTerritorial").val();
	terr.campo_codigo = $("#campoCodigoUdTerritorial").val();
	terr.srs = $("#srsUdTerritorial").val();

	var message = JSON.stringify(terr);
	message = message.replace(/""/g,"null");

	console.log(message)
	$.ajax({
		url:  contextPath+"/admin/udterritorial/"+"?token="+tokenId,
		data: message,
		processData: false,
		contentType: 'application/json',
		type: 'POST',
		success: function(data){
			alert("La unidad territorial se ha guardado correctamente");
			terr = data;
			$("#datoEditUdTerritorial").append("<option value='"+JSON.stringify(terr)+"'>"+terr.etiqueta+" </option>");
			$("#datoEditUdTerritorial").val(JSON.stringify(terr))
			$("#datoEditUdTerritorial").trigger("change");
		},
		error: function(error){
			//$.mobile.changePage("#popupError");
			alert("No ha sido posible realizar los cambios");
		}
	});

	$( "#addUTerritorial" ).popup("close");
}



function abrirFormularioUdTemporal(){
	$("#etiquetaUdTemporal").val('');
	$("#formatoDatosUdTemporal").val('');
	$("#formatoPresentacionUdTemporal").val('');
	$( "#addUTemporal" ).popup("open");

}


function guardarUdTemporal(){


	var temp = {};

	temp.etiqueta = $("#etiquetaUdTemporal").val();
	temp.formato_datos = $("#formatoDatosUdTemporal").val();
	temp.formato_presentacion = $("#formatoPresentacionUdTemporal").val();


	var message = JSON.stringify(temp);
	message = message.replace(/""/g,"null");

	$.ajax({
		url:  contextPath+"/admin/udtemporal/"+"?token="+tokenId,
		data: message,
		processData: false,
		contentType: 'application/json',
		type: 'POST',
		success: function(data){
			alert("La unidad temporal se ha guardado correctamente");
			temp = data;
			$("#datoEditUdTemporal").append("<option value='"+JSON.stringify(temp)+"'>"+temp.etiqueta+" </option>");
			$("#datoEditUdTemporal").val(JSON.stringify(temp))
			$("#datoEditUdTemporal").trigger("change");
		},
		error: function(error){
			//$.mobile.changePage("#popupError");
			alert("No ha sido posible realizar los cambios");
		}
	});



	$( "#addUTemporal" ).popup("close");
}


function mostrarEdicionDato(){
	$("#adminItem").hide();
	$("#adminDato").show();

	$("#viewButtons").hide();
	$("#editButtons").hide();
	$("#datoButtons").show();
}

function volverItem(){
	$("#adminItem").show();
	$("#adminDato").hide();
	$("#datoButtons").hide();
	$("#returnButtons").hide();
	$("#addFuente").hide();
	mostrarEdicion();
}

function volverDato(){
	$("#datoButtons").hide();
	$("#returnDatoButtons").hide();
	$("#addDimension").hide();
	$("#addEstilo").hide();
	$("#addMapa").hide();
	mostrarEdicionDato();
}


function guardarItem(){

	for(var i = 0; i<item.datos.length; i++){
		if(item.datos[i].id == currentRow.value.id){
			item.datos[i] = dato;
			item.datos[i].error = getErroresDato(item.datos[i]);
		}
	}
	
	if(currentRow){
		//Editar dato
		currentRow.val(dato);
	}else{
		//Crear recurso
		datos_table.rows.add(dato);
	}

	volverItem();

}

function changeVectorial(){
	if ($("#datoEditVectorial").prop("checked")){
		initEstilo();
		$(".onlyVectorial").removeClass("oculto");
		$(".onlyBBDD").removeClass("oculto");
	}
	else{
		$(".onlyVectorial").addClass("oculto");
		changeBBDD();
	}
}

function changeBBDD(){
	if (!$("#datoEditVectorial").prop("checked") && !$("#datoEditNormalizada").prop("checked") && !$("#grafico").prop("checked")){
		$(".onlyBBDD").addClass("oculto");
	}
	else{
		$(".onlyBBDD").removeClass("oculto");
	}
}

function changeWMS(){
	if ($("#datoEditWMS").prop("checked")){
		
		$(".onlyWMS").removeClass("oculto");

	}
	else{
		$(".onlyWMS").addClass("oculto");
	}
}

function changeUdTempo(){
	if ($("#datoEditUdTemporal").val()){
		
		$(".onlyTempoDim").removeClass("oculto");
	}
	else if(!dato.dimensiones || dato.dimensiones.length==0){
		$(".onlyTempoDim").addClass("oculto");
	}
	updateTablesTempoDim();
}

function updateTablesTempoDim(){
	initEstilo();
	initMapaWMS();
	initMapaWMSImprimir();
	if (dato.mapas_wms){
		var mapas_imprimir=new Array();
		var mapas_wms_var=new Array();
	
		for (var i=0; i<dato.mapas_wms.length;i++){
			var wms = dato.mapas_wms[i];
			wms.capas=wms.mapa.capas;
			wms.etiqueta=wms.mapa.etiqueta;
			if (wms.solo_imprimir){
				mapas_imprimir.push(wms);
			}
			else if(wms.tempo || wms.dimension1|| wms.dimension2|| wms.dimension3|| wms.dimension4){
				mapas_wms_var.push(wms);
			}
				
		}
		mapaswms_imprimir_table.loadRows(mapas_imprimir);
			mapaswms_table.loadRows(mapas_wms_var);
	
	}
}

function cancel(){
	location.href=contextPath+'/admin/items';
}

function updUdTempComentario(){
if ($("#itemEditEsSql").prop("checked")){
		
		$("#udsTempItem").removeClass("oculto");

	}
	else{
		$("#udsTempItem").addClass("oculto");
	}
}

function asociarUdTemp(){
	
	ud_temporales_table.rows.add(JSON.parse($("#itemUdTemporal").val()));
	$( "#vincularUdTemp" ).popup("close"); 
}