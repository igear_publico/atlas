var printMapWidth = 975;
var printMapHeight = 1050;
var xminOverview = 530000;
var ymaxOverview = 4760000;
var xmaxOverview = 855000;
var yminOverview = 4410000;
var server = "//" + document.location.host;
var printAppUrl4Legend = server+"/atlas/";
printActionURL = "/Mapa/";
printActionXSL = "/app/jboss/Mapa/templates/informeAtlasIDEAragon.xsl";
//printActionXSL = "/home/idearium_eu/Mapa/templates/informeAtlasIDEAragon.xsl";
printActionNoOficialXSL = "/app/jboss/Mapa/templates/informeAtlasIDEAragonNoOficial.xsl";

function getLegendForPrint() {
	var str = "";
	var listLegend = $("#toc li");

	for (var j = 0; j < listLegend.length; j++) {
		var leyId = listLegend[j].id;
		if ($("#"+leyId).hasClass("legend")){
			var leyenda = $("#"+leyId+ " img")[0];
			str += "<leyenda><laUrl>";
			if (leyenda.getAttribute("src").startsWith("http")|| leyenda.getAttribute("src").startsWith("/")) {
				str += urlAbsoluta(leyenda.getAttribute("src"));
			}
			else{
				str += urlAbsoluta(printAppUrl4Legend + leyenda.getAttribute("src"));
			}
			/*} else if (listLegend[j].getAttribute("src").startsWith("/")) {
            str += "//" + document.location.host + listLegend[j].getAttribute("src");
        } else {
            str += printAppUrl4Legend + listLegend[j].getAttribute("src");
        }*/
			str += "</laUrl>";

			str += "</leyenda>";
		}
		else{
			str += "<leyenda><texto>" + $("#"+leyId).text() + "</texto></leyenda>";
		}
	}

	return str;
}
function getMapasToPrint(){
	 var newBboxPrint = checkDeformation(printMapWidth, printMapHeight);
	var mapas="";
	
	for (var i=dato.mapas_wms.length-1; i>=0; i--){

		var wms = dato.mapas_wms[i];

		if (coincideDimension(wms) && coincideTempo(wms)){

			if (wms.solo_imprimir || wms.siempre_visible || $("#layer_"+wms.id+" input").prop('checked')){
				var cql_filter=null;
				
				if (wms.solo_imprimir && !wms.tempo && dato.ud_temporal){
					cql_filter="tempo="+ $("#selectMapaUdTemp"+dato.ud_temporal.id).val();
				}
					
				mapas+=getWMSToPrint(wms.mapa,cql_filter,newBboxPrint);
				
			}
			
		}
	}
	return mapas;
}


function getWMSToPrint(wms,cql_filter,newBboxPrint){
	   var default_params = "&SERVICE=WMS&VERSION="+(wms.version ? wms.version: '1.1.1')+"&REQUEST=GetMap&BBOX=" + newBboxPrint + "&WIDTH=" + printMapWidth + "&HEIGHT=" + printMapHeight;
	    var default_srs = "&SRS=EPSG%3A25830";
	    var default_styles = "&STYLES="+(wms.estilo ? wms.estilo :"");
	    var restoWMS = default_params + default_srs + "&TRANSPARENT=TRUE&FORMAT="+(wms.formato ? wms.formato : 'image/png') + default_styles;
	    var wmsURL = wms.url+ "?LAYERS=" + wms.capas + restoWMS+(wms.wmst ? "&TIME="+ formatDate($("#selectMapaUdTemp"+dato.ud_temporal.id).datepicker( "getDate" )):"")+
	    (cql_filter ? "&CQL_FILTER="+ cql_filter:"");
        return "<mapa><laUrl>" + urlAbsoluta(wmsURL) + "</laUrl></mapa>";
}

function getFiltrosSeccion(seccion_filtros){
	  var filtroSeccion="";
	    $("#"+seccion_filtros+" li:not(.oculto) select").each(function(index){
			var id = $( this )[0].id;
			if ($("#"+id).val()){
				if (filtroSeccion.length>0){
					filtroSeccion+= " - ";
				}
				filtroSeccion+= $("#"+id+ " option:selected").text();
			}
			
		});
		if (filtroSeccion.length>0){
			filtroSeccion= " ("+filtroSeccion+")";
		}
		return filtroSeccion;
}
function getDataToPrint(printModificados) {
   

    //var titulo = document.getElementById("titleForPrint").value;
    var titulo = $(".detalle .titulo").text();
//	var subtitulo =  (dato.ud_territorial ?" ("+ $("#selectMapaUdTerr option:selected").text()+")":"");
    var leyenda = getLegendForPrint();
    var comentario = $("#comentario-text").html();
    var filtroMapa="<filtrosMapa>"+getFiltrosSeccion("filtros-mapa")+"</filtrosMapa>";
  var mapas = filtroMapa+getMapasToPrint();
    //var comentario = $("#option0_description").html().substr(limitPDF,limitPDF2);
    var filtroComentario="<filtrosComentario>"+getFiltrosSeccion("filtros-comentario")+"</filtrosComentario>";
   
   

  //  var sigla = getCurrentUnidadTerritorialSigla();
    var queryXML = "" +
        "<fecha>" + getFechaActual() + "</fecha>" +
        "<titulo>" + titulo /*+ subtitulo*/ +"</titulo>" +
        (comentario && comentario.length>0 ?filtroComentario+
        "<comentario>" + comentario.replace(/–/g, "-") + "</comentario>":"")+ //se hace el reemplazo solo en comentario si no se rompe la tabla
        //+ "<comentario><p>" + comentario + "</p></comentario>"
        
        leyenda +
       mapas;
    if (dato.grafico){
    	 var filtroGraficos="<filtrosGraficos>"+getFiltrosSeccion("filtros-graficos")+"</filtrosGraficos>";
    	queryXML+=getPrintCharts()+filtroGraficos;
    }
    if (dato.ud_temporal){
    	var tempo;

    	if (dato.ud_temporal.id==4){
    		tempo = formatDate($("#selectMapaUdTemp4").datepicker( "getDate" ));
    	}
    	else{
    		tempo = $("#selectMapaUdTemp"+dato.ud_temporal.id+" option:selected").text();
    	}
    	queryXML += "<referenceYear>" + tempo + "</referenceYear>";
    }
    if (dato.ud_territorial){
    	 queryXML += "<territorialUnit>" + $("#selectMapaUdTerr option:selected").text() + "</territorialUnit>";
    }
    //+ "<mapa><laUrl>"+getPrintScalebar(newBboxPrint, printMapWidth, printMapHeight) + "</laUrl></mapa>"
    //+ "<situacionAragon><laUrl>"+getPrintOverview(newBboxPrint)+"</laUrl><width></width><height></height><top></top><left></left></situacionAragon>";

  /*  var $datosTab;
    if (printModificados){
        // Se imprimen los datos modificados
        var $datosTab = $("#dataTable").clone();

		// Se elimina el icono de supresión del cambio
		$datosTab.find('input[type="image"]').remove();
		$datosTab = $datosTab.html();
		queryXML+=getPrintCharts();
    }
    else if ($dataTableOriginal){
        // Se imprimen los datos originales
        $datosTab = $dataTableOriginal.clone().html();
       
    }

    if (haySITA) {
    	 queryXML+=getPrintCharts();
        queryXML += "<tabla>" + $datosTab + "</tabla>";
        if(!hayDIT_VALORES_SALUD){
        queryXML += "<territorialUnit>" + $("#unidadTerritorialComboMap").val() + "</territorialUnit>";
        }
        queryXML += "<referenceYear>" + getCurrentYear() + "</referenceYear>";

    } else if (hayDIT_DATOS) {
    	 queryXML+=getPrintCharts();
        queryXML += "<tabla>" + $datosTab + "</tabla>";
        queryXML += "<territorialUnit>" + availableTypesDIT_DATOS[0] + "</territorialUnit>";

    } else if (hayDIT_TABLA) {
    	 queryXML+=getPrintCharts();
        queryXML += "<tabla>" + $datosTab + "</tabla>";
    } else if (hayDIT_SALUD) {
    	 queryXML+=getPrintCharts();
        queryXML += "<territorialUnit>Zona Básica de Salud</territorialUnit>";
        queryXML += "<tabla>" + $datosTab + "</tabla>";
    }
*/
    var filtroTabla="<filtrosTabla>"+getFiltrosSeccion("filtros-tabla")+"</filtrosTabla>";
    queryXML +=getTablaToPrint()+filtroTabla;
    queryXML = queryXML.replace(/localhost/g, 'idearagon.aragon.es').replace(/http:/g,'https:');
    console.log(queryXML);
    return queryXML;
}

function getFechaActual() {
    var fecha = new Date();

    return fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear();
}


function makeXMLsafeAcentos2(oldString) {
    var aux = oldString;
    aux = aux.replace(/&nbsp;/g, " ");

    //Parece que esto es conflictivo
    aux = aux.replace(/&/g, "&amp;");
    aux = aux.replace(/<link href=".*page.css" rel="StyleSheet" type="text\/css">/g, "");
    aux = aux.replace(/&amp;ordm;/g, "&#186;");

/*    if (aux.indexOf("src='") != -1) {
        if (aux.indexOf("src='http://") == -1) {
            aux = aux.replace(/src='/g, "src='" + urlApp);
        }
    }

    if (aux.indexOf("src=\"") != -1) {
        if (aux.indexOf("src=\"http://") == -1) {
            aux = aux.replace(/src="/g, "src=\"" + urlApp);
        }
    }
*/
    aux = aux.replace(/&amp;ordm;/g, "&#186;");
    aux = aux.replace(/&amp;ordf;/g, "&#170;");
    aux = aux.replace(/&amp;quot;/g, "&#34;");
    aux = aux.replace(/&amp;aacute;/g, "&#225;");
    aux = aux.replace(/&amp;eacute;/g, "&#233;");
    aux = aux.replace(/&amp;iacute;/g, "&#237;");
    aux = aux.replace(/&amp;oacute;/g, "&#243;");
    aux = aux.replace(/&amp;uacute;/g, "&#250;");
    aux = aux.replace(/&amp;ntilde;/g, "&#241;");
    aux = aux.replace(/&amp;uuml;/g, "&#252;");
    aux = aux.replace(/&amp;Aacute;/g, "&#193;");
    aux = aux.replace(/&amp;Eacute;/g, "&#201;");
    aux = aux.replace(/&amp;Iacute;/g, "&#205;");
    aux = aux.replace(/&amp;Oacute;/g, "&#211;");
    aux = aux.replace(/&amp;Uacute;/g, "&#218;");
    aux = aux.replace(/&amp;Ntilde;/g, "&#209;");
    aux = aux.replace(/&amp;Uuml;/g, "&#220;");

    aux = aux.replace(/á/g, "&#225;");
    aux = aux.replace(/é/g, "&#233;");
    aux = aux.replace(/í/g, "&#237;");
    aux = aux.replace(/ó/g, "&#243;");
    aux = aux.replace(/ú/g, "&#250;");
    aux = aux.replace(/ñ/g, "&#241;");
    aux = aux.replace(/ü/g, "&#252;");
    //aux = aux.replace(/�?/g, "&#193;");
    aux = aux.replace(/É/g, "&#201;");
    //aux = aux.replace(/�?/g, "&#205;");
    aux = aux.replace(/Ó/g, "&#211;");
    aux = aux.replace(/Ú/g, "&#218;");
    aux = aux.replace(/Ñ/g, "&#209;");
    aux = aux.replace(/Ü/g, "&#220;");
    return aux;
}

/**
 * genera informe PDF del estado que se esta visualizando en el mapa
 */
function printPDF(getModificados) {
    var requestXML = getDataToPrint(getModificados);

    //OJO: aunque makeXMLsafeAcentos debería convertirlo, parece ser hay problemas con las mayúsculas acentuadas => cambiarlo manualmente en función local
    // De paso, corrijo otros caracteres raros
    var query = makeXMLsafeAcentos2(requestXML).replace(/%/g, "&#37;").replace(/«/g, "&#171;").replace(/»/g, "&#187;").replace(/«/g, "&#171;").replace(/»/g, "&#187;").replace(/€/g, "&#8364;").replace(/‰/g, "&#8240;");
    query = query.replace(/Á/g, "&#193;").replace(/É/g, "&#201;").replace(/Í/g, "&#205;").replace(/Ó/g, "&#211;").replace(/Ú/g, "&#218;").replace(/–/g, "&#8211;").replace(/∑/g, "&#8721;").replace(/²/g,"&#178;").replace(/º/g,"&#186;");
    query = query.replace(/“/g, "&#34;").replace(/”/g, "&#34;");
    document.printForm.xml.value = "<imprimir>" + query + "</imprimir>";
    //document.printForm.xml.value = "<imprimir>" + makeXMLsafeAcentos(requestXML).replace(/�?/g, "&#205;") + getPrintCharts() + "</imprimir>";
  console.log(document.printForm.xml.value);

    if (getModificados){
        // Se imprimen los datos modificados
        document.printForm.xslt.value = printActionNoOficialXSL;
    }
    else{
        // Se imprimen los datos oficiales        
        document.printForm.xslt.value = printActionXSL;    
    }
    
    document.printForm.action = printActionURL;
    document.printForm.submit();
    //$("#pdfMapa")[0].data=printActionURL+"?xslt="+printActionXSL+"&xml="+ encodeURI(makeXMLsafeAcentos(getDataToPrint()));
}

function checkDeformation(mwidth, mheight) {
    //var newBBOX = new boundingBox(_minx, _miny, _maxx, _maxy);
    var bounds = map.getView().calculateExtent(map.getSize());
    var _minx = Math.max(xminOverview,bounds[0]);
    var _miny = Math.max(yminOverview,bounds[1]);
    var _maxx = Math.min(xmaxOverview,bounds[2]);
    var _maxy =Math.min(ymaxOverview,bounds[3]);
    
    

    /*var _minx = xminOverview;
    var _miny = yminOverview;
    var _maxx = xmaxOverview;
    var _maxy = ymaxOverview;*/

    var ratioTamano = mwidth / mheight;
    var diffX = _maxx - _minx;
    var diffY = _maxy - _miny;
    var ratioCoord = diffX / diffY;
    //alert("ratioT" + ratioTamano + "," + ratioCoord)

    if (ratioCoord > ratioTamano) {
        // estirar la Y
        //	alert("estirar la Y")

        var newDiffY = diffX / ratioTamano;
        var gapY = newDiffY - diffY;

        if (gapY > 0) {
            _miny = _miny - (gapY / 2);
            _maxy = _maxy + (gapY / 2);
        } else {
            _maxy = _miny - (gapY / 2);
            _miny = _maxy + (gapY / 2);
        }
    } else if (ratioCoord < ratioTamano) {
        // estirar la X
        //alert("estirar la X")

        var newDiffX = ratioTamano * diffY;

        var gapX = newDiffX - diffX;
        if (gapX > 0) {
            _minx = _minx - (gapX / 2);
            _maxx = _maxx + (gapX / 2);
        } else {
            _maxx = _minx - (gapX / 2);
            _minx = _maxx + (gapX / 2);
        }
    }
    return _minx + "," + _miny + "," + _maxx + "," + _maxy;
}

function getTablaToPrint(){
	if (table){
	var cols=new Array();

	var tabla ="<tabla><thead><tr role='row'>";
	for (var i=0; i<table.columns.array.length; i++){
		if (!table.columns.array[i].hidden && table.columns.array[i].visible){
			tabla += '<th  role="columnheader">'+table.columns.array[i].title+'</th>';
			cols.push(table.columns.array[i].name);
		}
	}
	tabla +="</tr></thead><tbody>";
	for (var i=0; i<table.rows.all.length; i++){
		tabla +="<tr  role='row'>";
		for (var j=0; j<cols.length; j++){
			if (table.data.paging && table.data.pagingSize<table.rows.all.length){ // si la tabla está paginada no hay celda html con el valor, cogemos el valor del objeto  (si hay formatter se imprimirá algo diferente)
				tabla += "<td>"+(table.rows.all[i].value[cols[j]]?table.rows.all[i].value[cols[j]]:"")+"</td>";	
			}
			else{  // si la tabla no está paginada podemos coger el contenido del td para que se imprima exactamente lo que se ve
				tabla += "<td>"+/*(table.rows.all[i].value[cols[j]]?table.rows.all[i].value[cols[j]]:"")*/table.rows.all[i].cells[j].$el[0].textContent+"</td>";
			}
			
			
		}
		tabla +="</tr>"
		
	}
	
	tabla +="</tbody></tabla>";
	return tabla;
	}
	else{
		return "";
	}
		
}