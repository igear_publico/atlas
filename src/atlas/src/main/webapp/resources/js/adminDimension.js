var all_dimensiones;
var dimension;
var dimensiones_table;
var estado;
var dimensiones;
var currentRowDimension;


function initDimension(){

		dimension = {};
		dimension.dimension = {};

      dimensiones_table = FooTable.init('#dimensiones_table', {
        columns: [
          { "name": "id", "title": "ID.","visible": false },
          { "name": "titulo", "title": "Titulo"},
          { "name": "tabla_valores", "title": "Tabla de valores", "breakpoints": "xs sm" },
          { "name": "orden", "title": "Orden"}
        ]
        ,
        editing: {
          alwaysShow: true,
          allowEdit: true,
          enabled: true,
          addRow: function(){
            currentRowDimension = null;
            mostrarDimensiones();
          },
          editRow: function(row){
              currentRowDimension = row;
              abrirFormularioEditarDimension();
          },
          deleteRow: function(row){
            if(row.value.id){
                var index;
                for(var i = 0; i<dato.dimensiones.length;i++){
                  if(dato.dimensiones[i].id == row.value.id){
                      index = i;
                  }
                }
                dato.dimensiones.splice(index, 1);
            }
            updateTablesTempoDim();
            if ((dato.dimensiones.length==0)&&(!$("#datoEditUdTemporal").val())){
            	$(".onlyTempoDim").addClass("oculto");
            	
            }
            row.delete();
          },
          showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
          hideText: 'Cancelar',
          addText: 'Añadir dimensión'
        }
      });

      all_dimensiones = FooTable.init('#all_dimensiones', {
        columns: [
        { "name": "id", "title": "ID.","visible": false},
      { "name": "titulo", "title": "Titulo"},
      { "name": "tabla_valores", "title": "Tabla de valores","breakpoints": "xs sm"},
      { "name": "añadir", "title": "Añadir","formatter": function(value, options, rowData){
          return " <a onclick=\"asociarDimension('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
        }
      }
        ]/*,
          editing: {
            alwaysShow: true,
            allowEdit: false,
            allowDelete: false,
            enabled: true,
            addRow: function(){
              abrirFormularioCrearDimension();
            },
            showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
            hideText: 'Cancelar',
            addText: 'Añadir nueva dimensión'
          }*/
      });
}




function loadDimensiones(esCopia){
			var rows = [];
			if (dato.dimensiones){
			for(var i = 0; i<dato.dimensiones.length; i++){
					var dim = dato.dimensiones[i];
					var row = {};
					if (!esCopia){
						row.id = dim.id;
					}
					row.titulo = dim.dimension.titulo;
					row.nombre = dim.dimension.nombre;
					row.tabla_valores = dim.dimension.tabla_valores;
					row.orden = dim.orden;
					rows.push(row);
			}
			}
			dimensiones_table.loadRows(rows);
}




function abrirFormularioEditarDimension(){
		//Por defecto se mustrar sólo los datos de la relación y un botón para ampliar
	/*	$( "#datosEntidadDimension").hide();
		$( "#datosRelacionDimension").show();
		$( "#edicionAvanzadaDimension").show();
		$( "#advertenciaDimension").hide();
*/

    for(var i = 0; i<dato.dimensiones.length; i++){
        if(dato.dimensiones[i].id == currentRowDimension.value.id){
          dimension = dato.dimensiones[i];
        }
    }
    //Se rellena el formulario con los datos de la dimensión
  /*  $("#tituloDimension").val(dimension.dimension.titulo);
    $("#nombreDimension").val(dimension.dimension.nombre);
    $("#tablaValoresDimension").val(dimension.dimension.tabla_valores);*/
    $("#ordenDimension").val(dimension.orden);

    //Se actualiza el estado
    estado = "EDITAR";

    //Se abre el formulario
    $( "#addNewDimension" ).popup("open");
}




function abrirFormularioCrearDimension(){
		//Se mustrar sólo los datos de la entidad
		$( "#datosEntidadDimension").show();
		$( "#datosRelacionDimension").hide();
		$( "#advertenciaDimension").hide();
		$( "#edicionAvanzadaDimension").hide();

		//Se vacía el objeto para partir de cero
    dimension = {};
    dimension.dimension = {};


    //Se vacía el formulario
    $("#tituloDimension").val('');
    $("#nombreDimension").val('');
    $("#tablaValoresDimension").val('');
    $("#ordenDimension").val('');

		//Se actualiza el estado
    estado = "CREAR";


    //Se abre el formulario
    $( "#addNewDimension" ).popup("open");
}


function asociarDimension(id){
			for(var i = 0; i<dimensiones.length;i++){
				if(dimensiones[i].id == id){
					//Se guarda la fila seleccionada en el objeto
					var dim = dimensiones[i];
					dimension.dimension = dim;
					//Se muestra sólo los datos de la relación
     /*     $( "#datosEntidadDimension").hide();
          $( "#datosRelacionDimension").show();
          $( "#advertenciaDimension").hide();
          $( "#edicionAvanzadaDimension").hide();


          //Se rellena el formulario (Aunque no se muestre)
          $("#tituloDimension").val(dim.titulo);
          $("#nombreDimension").val(dim.nombre);
          $("#tablaValoresDimension").val(dim.tabla_valores);
*/
          //Los campos de la relación se dejan vacios
          $("#ordenDimension").val('');

          //Se actualiza el estado
          estado = "ASOCIAR";

          //Se abre el formulario
          $( "#addNewDimension" ).popup("open");
				}
			}
}


function edicionAvanzadaDimension(){
		$( "#datosEntidadDimension").show();
		$( "#advertenciaDimension").show();
		$( "#edicionAvanzadaDimension").hide();
		$( "#addNewDimension" ).popup("open");
}

function guardarDimension(){

		
	dimension.orden = $("#ordenDimension").val();

		if(estado == "CREAR"){
			//Se guardan los datos del formulario en el objeto
			dimension.orden = $("#ordenDimension").val();
			dimension.dimension.titulo = $("#tituloDimension").val();
			dimension.dimension.nombre = $("#nombreDimension").val();
			dimension.dimension.tabla_valores = $("#tablaValoresDimension").val();
			

					var message = JSON.stringify(dimension.dimension);
          message = message.replace(/""/g,"null");
					//Se envía petición para crear el objeto
          $.ajax({
            url:  contextPath+"/admin/dimension/"+"?token="+tokenId,
            data: message,
            processData: false,
            contentType: 'application/json',
            type: 'POST',
            success: function(data){
            	alert("La dimensión se ha guardado correctamente");
               //Se añade a la lista de todas las dimensiones

               var rows = all_dimensiones.rows.all.map( function(r){return r.value});

               var dim = data;
		           rows.unshift(dim);
		           dimensiones.push(dim);

		           all_dimensiones.loadRows(rows);
            },
            error: function(error){
              //$.mobile.changePage("#popupError");
              alert("No ha sido posible realizar los cambios");
            }
          });

		}else if((estado == "EDITAR") | (estado == "ASOCIAR")){

				//Se actualiza sólo la tabla de dimensiones en el dato
				var row = {};
        row.titulo = dimension.dimension.titulo;
        row.nombre = dimension.dimension.nombre;
        row.tabla_valores = dimension.dimension.tabla_valores;
        row.orden = dimension.orden;
        actualizarTablaDimension(row);
        volverDato();
		}
		$( "#addNewDimension" ).popup("close");
}

function actualizarTablaDimension(row){
	// hay que clonar la dimension, sino al añadir nuevos wms se modifican también los anteriores
	  var nueva_dim=new Object();
	  $.extend( true, nueva_dim,dimension);
	if(currentRowDimension){
		//Editar Dimension
		currentRowDimension.val(row);
		for(var i = 0; i<dato.dimensiones.length; i++){
			if(dato.dimensiones[i].id == currentRowDimension.value.id){
				dato.dimensiones[i] = nueva_dim;
			}
		}
	}else{
		//Crear recurso
		dimensiones_table.rows.add(row);
		dato.dimensiones.push(nueva_dim)
	}
	$(".onlyTempoDim").removeClass("oculto");
	updateTablesTempoDim();
	
}


function mostrarDimensiones(){
				var url = contextPath+"/admin/dimension/";

  			$.ajax({
  				url:  url,
  				processData: false,
  				contentType: 'application/json',
  				type: 'GET',
  				success: function(data){
  					dimensiones = data.dimensiones;
  					//Cargar la tabla con todos los datos
  					all_dimensiones.loadRows(dimensiones);


  					//Mostrar tabla con todas las dimensiones y ocultar el dato
  					$("#addDimension").show();
  					$("#adminItem").hide();
  					$("#adminDato").hide();
            $("#viewButtons").hide();
            $("#editButtons").hide();
            $("#datoButtons").hide();
            $("#returnDatoButtons").show();
  				},
  				error: function(error){
  					console.log(error)
  				}
  			});
}


