// Stuff related to geojson layers for Municipalities, Comarcas, Provinces and so on
var bboxList=new Object();
function getSiglaFromUnidadTerritorial(val) {
	if (val == "Municipio") {
		return "M";
	} else if (val == "Comarca") {
		return "C";
	} else if (val == "ComarcaAgraria") {
		return "D";
	} else if (val == "Provincia") {
		return "P";
	} else if (val == "PartidoJudicial") {
		return "PJ";
	} else if (val == "DivisionEclesiastica") {
		return "DE";
	} else if (val == "SALUDP1") {
		return "1";
	} else if (val == "SALUDP2") {
		return "2";
	} else if (val == "SALUDP3") {
		return "3";
	} else if (val == "EstacionAEMET") {
		return "E";
	} else if (val == "InfraestructuraEducativa") {
		return "IEDU";
	} else if (val == "ZonaEducativa") {
		return "ZEDU";
	} else if (val == "InfraestructuraSanitaria") {
		return "ISAN";
	} else if (val == "ZonaSalud2018") {
		return "ZSAL_2018";
	} else {
		return val;
	}
}

function getUnidadTerritorialFromSigla(val) {
	if (val == "M") {
		return "Municipio";
	} else if (val == "C") {
		return "Comarca";
	} else if (val == "D") {
		return "ComarcaAgraria";
	} else if (val == "P") {
		return "Provincia";
	} else if (val == "PJ") {
		return "PartidoJudicial";
	} else if (val == "DE") {
		return "DivisionEclesiastica";
	} else if (val == "SALUDP1") {
		return "SALUDP1";
	} else if (val == "SALUDP2") {
		return "SALUDP2";
	} else if (val == "SALUDP3") {
		return "SALUDP3";
	} else if (val == "E") {
		return "EstacionAEMET";
	} else if (val == "IEDU") {
		return "InfraestructuraEducativa";
	} else if (val == "ZEDU") {
		return "ZonaEducativa";
	} else if (val == "ISAN") {
		return "InfraestructuraSanitaria";
	} else if (val == "ZSAL_2018") {
		return "ZonaSalud2018";
	}
}

function getNamesList(type) {
	if (type == "Municipio") {
		return dMuniList;
	} else if (type == "Comarca") {
		return dComList;
	} else if (type == "ComarcaAgraria") {
		return dComAgrariaList;
	} else if (type == "Provincia") {
		return dProvList;
	} else if (type == "PartidoJudicial") {
		return dPJudicList;
	} else if (type == "DivisionEclesiastica") {
		return dDivEclesList;
	} else if (type == "EstacionAEMET") {
		return dEstAEMETList;
	} else if (type == "InfraestructuraEducativa") {
		return dInfrEducaList;
	} else if (type == "ZonaEducativa") {
		return dZonaEducaList;
	} else if (type == "InfraestructuraSanitaria") {
		return dInfrSanitList;
	} else {//ZBS
		return cZS_2018List;
	}
}

function getColorStrokeDivEcles(valEcles) {
	if (valEcles == "Provincia eclesiástica de Pamplona y Tudela") {
		return '#852323';
	} else {
		return '#9B9B9B';
	}
}

function getWidthStrokeDivEcles(valEcles) {
	if (valEcles == "Provincia eclesiástica de Pamplona y Tudela") {
		return 3;
	} else {
		return 2;
	}
}

function getColorDivEcles(valRango) {
	if (valRango == "Archidiócesis_Arzobispad") {
		return '#FFD970';
	} else {
		return '#B5E29D';
	}
}

function getColorInfrEduca(id, naturaleza) {
	if (naturaleza) {
		if (naturaleza == "Público") {
			return '#FF8000';
		} else if (naturaleza == "Privado") {
			return '#646464';
		} else if (naturaleza == "Concertado") {
			return '#0080FF';
		}
	}
	return 'transparent';
}

function getColorPJudic(value) {
	if (value == "Alcañiz") {
		return "#D3C2FD";
	} else if (value == "Barbastro") {
		return "#E7E7E7";
	} else if (value == "Boltaña") {
		return "#D2DFB1";
	} else if (value == "Calamocha") {
		return "#FDE7CD";
	} else if (value == "Calatayud") {
		return "#DCD9FD";
	} else if (value == "Caspe") {
		return "#FFDC99";
	} else if (value == "Daroca") {
		return "#FFFFCB";
	} else if (value == "Ejea De Los Caballeros") {
		return "#DCFFCB";
	} else if (value == "Fraga") {
		return "#FDD5CD";
	} else if (value == "Huesca") {
		return "#FDC5FC";
	} else if (value == "Jaca") {
		return "#FDC2CC";
	} else if (value == "La Almunia de Doña Godina") {
		return "#BCD785";
	} else if (value == "Monzón") {
		return "#DFE9FD";
	} else if (value == "Tarazona") {
		return "#C5FDED";
	} else if (value == "Teruel") {
		return "#FDDBF4";
	} else if (value == "Zaragoza") {
		return "#CBDBFF";
	}
	return "transparent";
}

// TODO: ?ESTA FUNCION ES GENERICA O DEPENDE DE ATLAS/SITA?
function genericGetColorSelected(val, type) {
	var valueToFind;
	if ((val.indexOf("ZS")>=0) && (type != "ZSAL")) {
		valueToFind = currentValuesNum[val.replace("ZS",(type.indexOf("ZBS")==0?type.replace("ZBS","")+"ZBS":type))];
	}
	else{
		valueToFind = currentValues[val];
	}

	if (intervalStyles) {
		for (var i = 0; i < intervalStyles[type].length; i++) {
			var minV = intervalStyles[type][i].value_min;
			var maxV = intervalStyles[type][i].value_max;
			if (isNaN(minV) || isNaN(maxV)) {
				if ((valueToFind == parseFloat(minV)) && (valueToFind == parseFloat(maxV))) {
					return intervalStyles[type][i].fillcolor;
				}
			} else {
				if ((valueToFind >= parseFloat(minV)) && (valueToFind <= parseFloat(maxV))) {
					return intervalStyles[type][i].fillcolor;
				}
			}
		}
	}
	return "transparent";
}

function getColorSelectedZSalud(val, type) {
	if (!hayDIT_VALORES_SALUD){
	return getColorSelectedZSaludGeneric(val, type, $('.typeComboClass').val());
	}
	else{
		return genericGetColorSelected(val,"ZBS"+$('#unidadTerritorialComboMap').val());
	}
}

function isInvertedColors(dit_id) {
    if (typeof invertedValues != 'undefined') {
		for (var i=0; i < invertedValues.length; i++) {
			if (invertedValues[i] === dit_id) {
				return true;
			}
		}
    }
    return false;
}

function isInvertedColorsCodigoIndicadorAgrupado(codigo_indicador_agrupado) {
    if (typeof invertedValuesCodigoIndicadorAgrupado != 'undefined') {
		for (var i=0; i < invertedValuesCodigoIndicadorAgrupado.length; i++) {
			if (invertedValuesCodigoIndicadorAgrupado[i] === codigo_indicador_agrupado) {
				return true;
			}
		}
    }
    return false;
}

function getColorSelectedZSaludGeneric(val, type, typeCombo) {
	if (typeCombo == "significacion_estadistica") {
		var currentValue = currentValues[val];
		if (isInvertedColors(dit_id)) {
			currentValue = Math.abs(3 - currentValue);
		}
		if (currentValue == "0") {
			return '#FFFFFF';
		} else if (currentValue == "1") {
			return '#1a9850';
		} else if (currentValue == "2") {
			return '#fee08b';
		} else if (currentValue == "3") {
			return '#d73027';
		} else {
			return 'transparent';
		}
	} else if (typeCombo == "significacion_estadistica_tabla") {
		var dimens = $('.unidadTerritorialComboClass').val();
		var numInterval = parseInt(availableValues_signif[dimens][val]);

		if (isInvertedColors(dit_id)) {
			numInterval = Math.abs(3 - numInterval);
		}

		if (numInterval == "0") {
			return '#FFFFFF';
		} else if (numInterval == "1") {
			return '#1a9850';
		} else if (numInterval == "2") {
			return '#fee08b';
		} else if (numInterval == "3") {
			return '#d73027';
		} else {
			return 'transparent';
		}
	} else if (typeCombo == "tasa") {
		var dimens = $('.unidadTerritorialComboClass').val();
		var numInterval = parseInt(availableValues_tasa_interval[dimens][val])-1;

		if (isNaN(numInterval)) {
			return "transparent";
		} else if (numInterval < 0) {
			return "transparent";
		} else {
			if (isInvertedColors(dit_id)) {
				numInterval = Math.abs(4 - numInterval);
			}

			return intervalStyles[type][numInterval].fillcolor;
		}
	} else {
		var dimens = $('.unidadTerritorialComboClass').val();
		var numInterval = parseInt(availableValues_interval[dimens][val])-1;

		if (isInvertedColors(dit_id)) {
			numInterval = Math.abs(4 - numInterval);
		}

		return intervalStyles[type][numInterval].fillcolor;
	}
}

function loadProvinciaLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 2, "1", "'#CCC'", "genericGetColorSelected(feature.get('CPROV'), 'P')",
		"'12px Verdana'",
		"dProvList[feature.get('CPROV')]");
}

function loadComarcaLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 3, "1", "'#CCC'", "genericGetColorSelected(feature.get('C_COMARCA'), 'C')",
		"map.getView().getZoom() >= 2 ? '12px Verdana' : (map.getView().getZoom() == 1 ? '10px Verdana' : '0px')",
		"dComList[feature.get('C_COMARCA')]",dComList,'C_COMARCA');
}

function loadComarcaAgrariaLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 3, "1", "'#CCC'", "genericGetColorSelected(feature.get('codigo'), 'D')",
		"map.getView().getZoom() >= 2 ? '12px Verdana' : (map.getView().getZoom() == 1 ? '10px Verdana' : '0px')",
		"dComAgrariaList[feature.get('codigo')]",dComAgrariaList,'codigo');
}


function loadMunicipioLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 4, "1", "'#CCC'", "genericGetColorSelected(feature.get('CMUNINE'), 'M')",
		"map.getView().getZoom() >= 4 ? '12px Verdana' : (map.getView().getZoom() == 3 ? '10px Verdana' : '0px')",
		"feature.get('CMUNINE')",dMuniList,'CMUNINE');
}

function loadEstAEMETLayer(map, urlData, srs) {
	return  loadGenericLayer(map, urlData, srs, 4, "1", "'#CCC'", "'#CCC'",
		"'12px Verdana'",
		"currentValues['E'+feature.getProperties().id]");
}

function loadDivEclesLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 5, "getWidthStrokeDivEcles(feature.get('ECLESIASTI'))", "getColorStrokeDivEcles(feature.get('ECLESIASTI'))",
		"getColorDivEcles(feature.get('RANGO'))",
		"map.getView().getZoom() >= 2 ? '12px Verdana' : (map.getView().getZoom() <= 1 ? '10px Verdana' : '0px')",
		"feature.get('DIOCESIS')",null,'DIOCESIS');
}

function loadPJudicLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 6, "1", "'#696969'", "getColorPJudic(feature.get('pj'))",
		"map.getView().getZoom() >= 2 ? '12px Verdana' : (map.getView().getZoom() <= 1 ? '10px Verdana' : '0px')",
		"feature.get('pj')",null,'pj');
}

function loadZSaludLayer(map, urlData, srs,dZS_List) {
		//TODO: Cambiar genericGetColorSelected para que use el que toque
	return loadGenericLayer(map, urlData, srs, 7, "1", "'#CCC'", "getColorSelectedZSalud(feature.get('codigo'), getCurrentUnidadTerritorialSigla())",
	//return loadGenericLayer(map, urlData, srs, 7, "1", "'#CCC'", "'#FF0000'",
		"map.getView().getZoom() >= 7 ? '11px Verdana' : (map.getView().getZoom() == 6 ? '9px Verdana' : '0px')",
		"feature.get('nombre')",dZS_List,'codigo');
}

function loadZSalud2018Layer(map, urlData, srs,dZS_List) {
		//TODO: Cambiar genericGetColorSelected para que use el que toque
	return loadGenericLayer(map, urlData, srs, 7, "1", "'#CCC'", "genericGetColorSelected(feature.get('codigo'), 'ZSAL')",
	//return loadGenericLayer(map, urlData, srs, 7, "1", "'#CCC'", "'#FF0000'",
		"map.getView().getZoom() >= 7 ? '11px Verdana' : (map.getView().getZoom() == 6 ? '9px Verdana' : '0px')",
		"feature.get('nombre')",dZS_List,'codigo');
}

function loadInfrEducaLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 200, "1", "'#000'", "getColorInfrEduca(feature.get('id'), feature.get('naturaleza'))",
		"(map.getView().getZoom() >= 7 ? '10px Verdana' : '0px')",
		"feature.get('nombre_cen')",dInfrEducaList,'id', 'getCircleIconInfrEduca(feature);', 'getTextStyleInfrEduca(feature)');
}

function loadZonaEducaLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 8, "1", "'#000'", "genericGetColorSelected(feature.get('codigo'), 'ZEDU')",
		"map.getView().getZoom() >= 6 ? '12px Verdana' : (map.getView().getZoom() == 5 ? '10px Verdana' : '0px')",
		"feature.get('denomina')",dZonaEducaList,'codigo');
}

function loadInfrSanitLayer(map, urlData, srs) {
	return loadGenericLayer(map, urlData, srs, 200, "1", "'#000'", "genericGetColorSelected(feature.get('regcess'), 'ISAN')",
		"map.getView().getZoom() >= 6 ? '12px Verdana' : (map.getView().getZoom() == 5 ? '10px Verdana' : '0px')",
		"feature.get('denomina')",dInfrSanitList,'regcess', 'getCircleIconInfrSanit(feature);', 'getTextStyleInfrSanit(feature)');
}

/*
// para tener como símbolo un polígono de 4 lados (cuadrado)
function getSquareIconInfrEduca(feature) {
	return new ol.style.RegularShape({
		fill: new ol.style.Fill({
					color: getColorInfrEduca(feature.get('id'), feature.get('naturaleza'))
			}),
		stroke: new ol.style.Stroke({
					color: "#000",
					width: 1
					}),
		points: 4,
		radius: 10,
		rotation: Math.PI / 4,
		angle: 0
	});
}
*/

function getCircleIconInfrEduca(feature) {
	var _id = feature.get('id');
	var _valor = typeof currentValues[_id];
	var _radius = 0;
	var type = "IEDU";
	if (_valor != 'undefined') {
		var valueToFind = currentValues[_id];
		if (intervalStyles) {
			for (var i = 0; i < intervalStyles[type].length; i++) {
				var minV = intervalStyles[type][i].value_min;
				var maxV = intervalStyles[type][i].value_max;
				if (isNaN(minV) || isNaN(maxV)) {
					if ((valueToFind == parseFloat(minV)) && (valueToFind == parseFloat(maxV))) {
						_radius = intervalStyles[type][i].radius;
					}
				} else {
					if ((valueToFind >= parseFloat(minV)) && (valueToFind <= parseFloat(maxV))) {
						_radius = intervalStyles[type][i].radius;
					}
				}
			}
		}
	}

	return new ol.style.Circle({
		fill: new ol.style.Fill({
					color: getColorInfrEduca(_id, feature.get('naturaleza'))
			}),
		stroke: new ol.style.Stroke({
					color: "#000",
					width: 1
					}),
		radius: _radius,
		snapToPixel: false
	});
}

function getTextStyleInfrEduca(feature) {
	if (map.getView().getZoom() >= 7) {
		return new ol.style.Text({
				font: '10px Verdana',
				text: feature.get('nombre_cen'),
				fill: new ol.style.Fill({color: 'black'}),
				stroke: new ol.style.Stroke({color: 'white', width: 2}),
				offsetY: -12
		});
	} else {
		return new ol.style.Text({
		});
	}
}

function getCircleIconInfrSanit(feature) {
	var _radius = 4;
	return new ol.style.Circle({
		fill: new ol.style.Fill({
					color: genericGetColorSelected(feature.get('regcess'), 'ISAN')
			}),
		stroke: new ol.style.Stroke({
					color: "#000",
					width: 1
					}),
		radius: _radius,
		snapToPixel: false
	});
}

function getTextStyleInfrSanit(feature) {
	if (map.getView().getZoom() >= 6) {
		return new ol.style.Text({
				font: '10px Verdana',
				text: feature.get('denomina'),
				fill: new ol.style.Fill({color: 'black'}),
				stroke: new ol.style.Stroke({color: 'white', width: 2}),
				offsetY: -12
		});
	} else {
		return new ol.style.Text({
		});
	}
}


function getIcon(feature, customIcon) {
	if (customIcon) {
		return eval(customIcon);
	}
	return new ol.style.Icon({src:'/lib/IDEAragon/themes/default/images/chincheta.png'});
}

function getTextStyle(feature, customTextStyle, fontStr, textStr) {
	if (customTextStyle) {
		return eval(customTextStyle);
	}
	if (eval(fontStr) == '0px') {
		return new ol.style.Text({
		});
	} else {
		return new ol.style.Text({
				font: eval(fontStr),
				text: eval(textStr),
				fill: new ol.style.Fill({color: 'black'}),
				stroke: new ol.style.Stroke({color: 'white', width: 2})
			});
	}
}

function loadGenericLayer(map, urlData, srs, zIndex, widthStrokeStr, colorStrokeStr, colorStr, fontStr, textStr, nameList, codeAttr, customIcon, customTextStyle) {
	var projection = new ol.proj.Projection({code:"EPSG:25830"});

	var layer = new ol.layer.Vector({
		source: new ol.source.Vector({
			url: urlData,
			projection: srs,
			format: new ol.format.GeoJSON({defaultDataProjection: projection, featureProjection:projection})
		}),
		style: function(feature) {
				// filtrar o hacer lo que se necesite
				//return feature.getId() !== undefined ? style : null;
			var style = new ol.style.Style({
				image: getIcon(feature, customIcon),
				stroke: new ol.style.Stroke({
					color: eval(colorStrokeStr),
					width: eval(widthStrokeStr),
				}),
				fill: new ol.style.Stroke({
					color: eval(colorStr),
				}),
				text: getTextStyle(feature, customTextStyle, fontStr, textStr)
			});
			return style;
		}
	});
	layer.setZIndex(zIndex);
	layer.setOpacity(0.75);
	if (codeAttr){
	layer.on("change:visible", function(event){
		var capa=event.target;
		$("#zoomTo").empty();
		$(".zoomToBlock .custom-combobox-input").val("");
		$(".zoomToBlock").hide();
		if (capa.getVisible()){
			bboxList=new Object();
			//var codigos = Object.keys(listaNombres);

			//$("#zoomTo").append("<option value=''></option>");
			/*for (var i=0; i<codigos.length; i++){
				$("#zoomTo").append("<option value='"+codigos[i]+"'>"+listaNombres[codigos[i]]+"</option>");
			}*/
			var features = capa.getSource().getFeatures();
			
			for (var i=0; i<features.length; i++){
				$(".zoomToBlock").show();
				var feature = features[i];
				bboxList[feature.getProperties()[codeAttr]]= feature.getGeometry().getExtent();
				addOrderedOption("zoomTo",feature.getProperties()[codeAttr],(nameList?nameList[feature.getProperties()[codeAttr]]:feature.getProperties()[codeAttr]));
			}
			
		}
	});
	layer.getSource().on("addfeature", function(event){
		$(".zoomToBlock").show();
		var feature=event.feature;
				bboxList[feature.getProperties()[codeAttr]]= feature.getGeometry().getExtent();
				addOrderedOption("zoomTo",feature.getProperties()[codeAttr],(nameList?nameList[feature.getProperties()[codeAttr]]:feature.getProperties()[codeAttr]));
				
	});
	}
	//layer.setVisible(false);
	map.addLayer(layer);
	return layer;
}


function addOrderedOption(comboId, value, label){
	var options = $("#"+comboId+" option");
	var added=false;
	var i=0;
	//console.log(label);
	while ((!added) && (i<options.length) ){
		if (options[i].innerText.toLowerCase()>label.toLowerCase()){
			$("#"+comboId+" option:nth-child("+(i+1)+")").before("<option value='"+value+"'>"+label+"</option>");
			added=true;
		}
		i++;
	}
	if (!added){
		$("#"+comboId).append("<option value='"+value+"'>"+label+"</option>");

	}
}