var valores;
var tempo_valores;
var dimensiones = new Array();

function changeFontSize(newType) {
	$(".font-size").removeClass('font-size-selected');
	$(".font-size.font-size-" + newType).addClass('font-size-selected');
	$(".texto-variable").removeClass('font-size-s');
	$(".texto-variable").removeClass('font-size-m');
	$(".texto-variable").removeClass('font-size-l');
	$(".texto-variable").addClass('font-size-' + newType);
}


function updComentario(item_id,index,syncSections){
	if (syncSections){
		$(".udTemporal"+index).val($("#selectComent"+index).val());
		$("select.udTemporal"+index).selectmenu("refresh");
		updTempoMapa(index);
	}

	else if ($("#selectComent"+index)){
		var tempo = $("#selectComent"+index).val();
		if (tempo){
			$(".udTemporal").not("#selectComent"+index).val("").change();
			
			$.ajax({
				url: item_id+"/comentario/"+tempo,
				type: 'GET',
				success: function(data) {
					
					$("#comentario-text").empty();
					$("#comentario-text").append(data);
					console.log("pongo coment");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert("No se ha podido obtener el comentario " + textStatus + "\n " + errorThrown);
				}
			});
		}
		
	}
	

}

function loadDataValues(tempo,redraw, loadLayers){
	var dims = getDimensions();
	estilo = getEstilo(dato.estilos);
	tempo_valores = tempo;
	$.ajax({
		url: "dato/"+dato.id+ "/"+(tempo!=null ?tempo:" ")+(dims!=null ? "/"+dims:""),
		type: 'GET',
		success: function(data) {
			var valores_tabla;
			if (dato.tabla_normalizada){
				valores = data;

			}
			else{
				valores = data.mapa;
				valores_tabla = data.tabla;
			}
			if (redraw){
				redrawLayers();
				emptyTable();
			}
			if (loadLayers){
				addLayers();
				deleteTable();
			}
			loadTable(valores_tabla);
			if (dato.grafico){
				$(".chartContainer").removeClass("oculto");
				loadChart();
			}
			else{
				$(".chartContainer").addClass("oculto");
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("No se han podido obtener los datos " + textStatus + "\n " + errorThrown);
		}
	});
}
function loadValues(){
	var tempo=null;
	if (dato.ud_temporal && dato.ud_temporal.id){
		tempo = $("#selectMapaUdTemp"+dato.ud_temporal.id).val();
	}
	
	loadDataValues(tempo,true);
	
}

function getDimensions(){
	dimensiones = new Array();
	var dims = "";
	if (dato.dimensiones){
		for (var i=0; i< dato.dimensiones.length; i++){
			if (i>0){
				dims+="/";
			}
			dims+= $("#selectMapaDim"+dato.dimensiones[i].orden).val();
			dimensiones.push($("#selectMapaDim"+dato.dimensiones[i].orden).val());
		}
	}
	return dims;
}
function onInputDate(ud_id) {
	var calendarId="selectMapaUdTemp4";
	  $( "#"+calendarId).datepicker( "option", "onSelect", function(dateText) {
			$( ".calendarDate" ).hide();
			var fechaCalendar = $( "#"+calendarId ).datepicker( "getDate" );
			//$("#serieTempoMap").html(formatDate(fechaCalendar, false, true));

			var valDate = new Date(fechaCalendar);
			$("#serieTempoMapD").val(valDate.getDate());
			$("#serieTempoMapM").val(valDate.getMonth()+1);
			$("#serieTempoMapY").val(valDate.getFullYear());

			updTempoMapa(4);//será siempre calendario
	   });
	  $( "#"+calendarId ).show();
	}

function clearCalendar(){
	$("#serieTempoMapD").val("");
	$("#serieTempoMapM").val("");
	$("#serieTempoMapY").val("");
	$( ".calendarDate" ).hide();
}

function chk_onChangeDateSerieT(evt) {
	if (isEnterKey(evt)) {
		onChangeDateSerieT();
	}
}

function onChangeDateSerieT() {
	var _day = $("#serieTempoMapD").val();
	var _month = parseInt($("#serieTempoMapM").val())-1;
	var _year = $("#serieTempoMapY").val();
	valDate = isValidDate(_year, _month, _day);
	if (valDate) {
		$( "#selectMapaUdTemp4").datepicker("setDate", valDate);
		updTempoMapa(4);//será siempre calendario
		$( "#selectMapaUdTemp4").hide();
	} else {
		alert('La fecha no es válida');
	}
}

function isValidDate(year, month, day) {
    var d = new Date(year, month, day);
    if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
        return d;
    }
    return null;
}

function formatDate(date) {
	var dd = date.getDate();
	var mm = date.getMonth() + 1; //January is 0!
	var yyyy = date.getFullYear();

	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	return yyyy + '-' + mm + '-' + dd;

}

function isEnterKey(evt) {
	if (!evt) {
		return (evt.keyCode == 13);
	} else if (!evt.keyCode) {
		return (evt.which == 13);
	}
	return (evt.keyCode == 13);
}

function coincideTempo(mapa){
	if (mapa.tempo){
		if (dato.ud_temporal){
			var tempo = $("#selectMapaUdTemp"+dato.ud_temporal.id).val();
			if (tempo && mapa.tempo==tempo){
				return true;
			}
			else{
				return false;
			}
		}
	}
	return true;
}

function donwloadSelectedResource(recurso_id){
	location.href=$("#recurso" + recurso_id+ "url").text().replace("##opcion##",$("#recurso" + recurso_id).val());
}

function donwloadPixelResource(recurso_id){
	if ($("#recurso" + recurso_id).val()){
		var coords = $("#recurso" + recurso_id).val().split(";");
		var x,y;
		try{
			x = Math.round(parseFloat(coords[0].trim()));
			y = Math.round(parseFloat(coords[1].trim()));
			
		}
		catch(err){
			console.log(err);
			alert("El formato de las coordenadas no es correcto. Deben incluirse x e y separadas por ;");
			return;
		}
		var roundedX= Math.trunc(x/1000)*1000+500;
		var roundedY=Math.trunc(y/1000)*1000+500;
		location.href=$("#recurso" + recurso_id+ "url").text().replace("##coords##",roundedX+"_"+roundedY);	
	}
	else{
		alert("Debe indicar las coordenadas para las que desea descargar los datos")
	}
	
}