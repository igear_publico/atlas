var mapaswms_table,mapaswms_imprimir_table,mapaswms_aux_table;
var currentRowMapa,currentTablaMapa;
var mapa_wms;
var all_mapas_wms;
var mapas_wms=new Object();;

function initMapaWMS(){

	mapa_wms = {};
mapa_wms.mapa = {};
var columnas= [
               { "name": "id", "title": "ID.","visible": false },
               { "name": "etiqueta", "title": "Etiqueta"},
               { "name": "capas", "title": "Capas WMS","breakpoints": "xs sm"},
               { "name": "orden", "title": "Orden"},
               { "name": "tempo", "title": "Tiempo","visible": dato.ud_temporal!=null}];
    if (dato.dimensiones){
 	   for (var i=0; i<dato.dimensiones.length; i++){
 		   var dimension = dato.dimensiones[i];
 		   columnas.push( { "name": "dimension"+dimension.orden, "title": dimension.dimension.titulo, "breakpoints": "xs sm" });
 	   }
    }
    if (mapaswms_table){
    	mapaswms_table.destroy();
    	$("#mapaswms_table").empty();
    }
  mapaswms_table = FooTable.init('#mapaswms_table', {
	  showHeader: true,
    columns: columnas,
    editing: {
      alwaysShow: true,
      allowEdit: true,
      enabled: true,
      addRow: function(){
        currentRowMapa = null;
        mostrarMapas(mapaswms_table);
      },
      editRow: function(row){
          currentRowMapa = row;
          abrirFormularioEditarMapaWMS();
      },
      deleteRow: function(row){
        if(row.value.id){
            var index;
            for(var i = 0; i<dato.mapas_wms.length;i++){
              if(dato.mapas_wms[i].id == row.value.id){
                  index = i;
              }
            }
            dato.mapas_wms.splice(index, 1);
        }
        row.delete();
      },
      showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
      hideText: 'Cancelar',
      addText: 'Añadir mapa WMS'
    }
  });


}
function initMapaWMSAux(){

	mapa_wms = {};
mapa_wms.mapa = {};
var columnas= [
               { "name": "id", "title": "ID.","visible": false },
               { "name": "etiqueta", "title": "Etiqueta"},
               { "name": "capas", "title": "Capas WMS","breakpoints": "xs sm"},
               { "name": "orden", "title": "Orden"}];
if (mapaswms_aux_table){
	mapaswms_aux_table.destroy();
}   
mapaswms_aux_table = FooTable.init('#mapaswms_aux_table', {
    columns: columnas
    ,
    editing: {
      alwaysShow: true,
      allowEdit: true,
      enabled: true,
      addRow: function(){
        currentRowMapa = null;
        mostrarMapas(mapaswms_aux_table);
      },
      editRow: function(row){
          currentRowMapa = row;
          abrirFormularioEditarMapaWMS();
      },
      deleteRow: function(row){
        if(row.value.id){
            var index;
            for(var i = 0; i<dato.mapas_wms.length;i++){
              if(dato.mapas_wms[i].id == row.value.id){
                  index = i;
              }
            }
            dato.mapas_wms.splice(index, 1);
        }
        row.delete();
      },
      showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
      hideText: 'Cancelar',
      addText: 'Añadir mapa WMS'
    }
  });


}

function initMapaWMSImprimir(){

	mapa_wms = {};
mapa_wms.mapa = {};
var columnas= [
               { "name": "id", "title": "ID.","visible": false},
               { "name": "etiqueta", "title": "Etiqueta"},
               { "name": "capas", "title": "Capas WMS","breakpoints": "xs sm"},
               { "name": "orden", "title": "Orden"},
               { "name": "tempo", "title": "Tiempo","visible": dato.ud_temporal!=null}];
    if (dato.dimensiones){
 	   for (var i=0; i<dato.dimensiones.length; i++){
 		   var dimension = dato.dimensiones[i];
 		   columnas.push( { "name": "dimension"+dimension.orden, "title": dimension.dimension.titulo, "breakpoints": "xs sm" });
 	   }
    }
    
    if (mapaswms_imprimir_table){
    	mapaswms_imprimir_table.destroy();
    	$('#mapaswms_imprimir_table').empty();
    	
    }  
  mapaswms_imprimir_table = FooTable.init('#mapaswms_imprimir_table', {
    columns: columnas
    ,
    editing: {
      alwaysShow: true,
      allowEdit: true,
      enabled: true,
      addRow: function(){
        currentRowMapa = null;
        mapa_wms.solo_imprimir=true;
        mapa_wms.siempre_visible=true;
        mostrarMapas(mapaswms_imprimir_table);
      },
      editRow: function(row){
          currentRowMapa = row;
          abrirFormularioEditarMapaWMS();
      },
      deleteRow: function(row){
        if(row.value.id){
            var index;
            for(var i = 0; i<dato.mapas_wms.length;i++){
              if(dato.mapas_wms[i].id == row.value.id){
                  index = i;
              }
            }
            dato.mapas_wms.splice(index, 1);
        }
        row.delete();
      },
      showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
      hideText: 'Cancelar',
      addText: 'Añadir mapa WMS'
    }
  });


}
function initMapa(){

      all_mapas_wms = FooTable.init('#all_mapas_wms', {
        columns: [
        { "name": "id", "title": "ID.", "breakpoints": "xs sm" },
        { "name": "etiqueta", "title": "Etiqueta"},
        { "name": "añadir", "title": "Añadir","style": {"text-align": "right"}, "formatter": function(value, options, rowData){
          return " <a onclick=\"asociarMapa('"+rowData.id+"')\" ><i class=\"fa fa-plus\"></i></a>";
        }
      }
        ],
          editing: {
            alwaysShow: true,
            allowEdit: false,
            allowDelete: false,
            enabled: true,
            allowView: true,
      		viewRow: function(row){
      		  
      			abrirFormularioCrearMapaWMS(row);
      		},
            addRow: function(){
              abrirFormularioCrearMapaWMS();
            },
            showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Editar',
            hideText: 'Cancelar',
            addText: 'Añadir nuevo mapa WMS'
          }
      });

}





function abrirFormularioEditarMapaWMS(){
		//Por defecto se mustrar sólo los datos de la relación y un botón para ampliar
	configurarFormularioMapaWMS();
	//Se muestra sólo los datos de la relación
    $( "#datosEntidadMapa").hide();
    $( "#datosRelacionMapa").show();
    $( "#advertenciaMapa").hide();
    $( "#edicionAvanzadaMapa").hide();

    for(var i = 0; i<dato.mapas_wms.length; i++){
        if(dato.mapas_wms[i].id == currentRowMapa.value.id){
          mapa_wms = dato.mapas_wms[i];
        }
    }
    //Se rellena el formulario con los datos de la dimensión
    $("#dimension1Mapa").val(mapa_wms.dimension1);
    $("#dimension2Mapa").val(mapa_wms.dimension2);
    $("#dimension3Mapa").val(mapa_wms.dimension3);
    $("#dimension4Mapa").val(mapa_wms.dimension4);
    $("#tiempoMapa").val(mapa_wms.tempo);
    $("#imprimirMapa").prop('checked', mapa_wms.solo_imprimir).checkboxradio('refresh');
    $("#visibleMapa").prop('checked', mapa_wms.siempre_visible).checkboxradio('refresh');
    $("#ordenMapa").val(mapa_wms.orden);
    // pongo estos valores para que funcione el ver leyenda y ver mapa
    $("#urlMapa").val(mapa_wms.mapa.url);
    $("#capasMapa").val(mapa_wms.mapa.capas);
    $("#estiloMapa").val(mapa_wms.mapa.estilo);
    $("#versionMapa").val(mapa_wms.mapa.version);
    $("#formatoMapa").val(mapa_wms.mapa.formato);
    $("#leyendaMapa").val(mapa_wms.mapa.leyenda);
    $("#glgMapa").prop('checked', mapa_wms.mapa.glg).checkboxradio('refresh');
    $("#cqlFilterMapa").val(mapa_wms.mapa.cql_filter);
 /*   $("#etiquetaMapa").val(mapa_wms.mapa.etiqueta);
    $("#wmstMapa").prop('checked', mapa_wms.mapa.wmst).checkboxradio('refresh');

    $( "#fecha_defectoMapa" ).datepicker({
      dateFormat: "yy-mm-dd",
      defaultDate: new Date(mapa_wms.mapa.fecha_defecto),
       onSelect: function () {
           mapa_wms.mapa.fecha_defecto = $.datepicker.formatDate("yy-mm-dd", $(this).datepicker('getDate'));
       }});
*/
    //Se actualiza el estado
    estado = "EDITAR";

    //Se abre el formulario
    $( "#addNewMapa" ).popup("open");
}



function abrirFormularioCrearMapaWMS(row){
	configurarFormularioMapaWMS();
	//Se mustrar sólo los datos de la entidad
	$( "#datosEntidadMapa").show();
	$( "#datosRelacionMapa").hide();
	$( "#advertenciaMapa").hide();
	$( "#edicionAvanzadaMapa").hide();

	//Se vacía el objeto para partir de cero
	mapa_wms = {};
	//Se vacía el formulario
	$("#dimension1Mapa").val(null);
	$("#dimension2Mapa").val(null);
	$("#dimension3Mapa").val(null);
	$("#dimension4Mapa").val(null);
	$("#tiempoMapa").val(null);
	$("#imprimirMapa").prop('checked', false).checkboxradio('refresh');
	$("#visibleMapa").prop('checked', false).checkboxradio('refresh');
	$("#ordenMapa").val(null);
	if (row){
		var mapa=new Object();
		$.extend( true, mapa,mapas_wms[row.value.id]);	
		mapa.id=null;
		mapa_wms.mapa = mapa;
		$("#urlMapa").val(mapa_wms.mapa.url);
		$("#capasMapa").val(mapa_wms.mapa.capas);
		$("#estiloMapa").val(mapa_wms.mapa.estilo);
		$("#versionMapa").val(mapa_wms.mapa.version);
		$("#formatoMapa").val(mapa_wms.mapa.formato);
		$("#leyendaMapa").val(mapa_wms.mapa.leyenda);
		$("#glgMapa").prop('checked', mapa_wms.mapa.glg).checkboxradio('refresh');
		$("#cqlFilterMapa").val(mapa_wms.mapa.cql_filter);
		$("#etiquetaMapa").val(mapa_wms.mapa.etiqueta);
		$("#wmstMapa").prop('checked', mapa_wms.mapa.wmst).checkboxradio('refresh');

		$( "#fecha_defectoMapa" ).datepicker({
			dateFormat: "yy-mm-dd",
			defaultDate: new Date(mapa_wms.mapa.fecha_defecto),
			onSelect: function () {
				mapa_wms.mapa.fecha_defecto = $.datepicker.formatDate("yy-mm-dd", $(this).datepicker('getDate'));
			}});
		

	}
	else{
		mapa_wms.mapa = {};




		$("#urlMapa").val(null);
		$("#capasMapa").val(null);
		$("#estiloMapa").val(null);
		$("#versionMapa").val(null);
		$("#formatoMapa").val(null);
		$("#leyendaMapa").val(null);
		$("#glgMapa").prop('checked', true).checkboxradio('refresh');
		$("#cqlFilterMapa").val(null);
		$("#etiquetaMapa").val(null);
		$("#wmstMapa").prop('checked', false).checkboxradio('refresh');
		$( "#fecha_defectoMapa" ).datepicker({
			dateFormat: "yy-mm-dd",
			defaultDate: new Date(),
			onSelect: function () {
				mapa_wms.mapa.fecha_defecto = $.datepicker.formatDate("yy-mm-dd", $(this).datepicker('getDate'));
			}});
	}
	changeGlg();
	//Se actualiza el estado
	estado = "CREAR";


	//Se abre el formulario
	$( "#addNewMapa" ).popup("open");
}


function asociarMapa(id,tabla){
	configurarFormularioMapaWMS();
			
					//Se guarda la fila seleccionada en el objeto
					
					mapa_wms.mapa = mapas_wms[id];

					//Se muestra sólo los datos de la relación
          $( "#datosEntidadMapa").hide();
          $( "#datosRelacionMapa").show();
          $( "#advertenciaMapa").hide();
          $( "#edicionAvanzadaMapa").hide();


           //Se rellena el formulario (Aunque no se muestre)
          $("#imprimirMapa").prop('checked', mapa_wms.solo_imprimir).checkboxradio('refresh');
          $("#visibleMapa").prop('checked', mapa_wms.siempre_visible).checkboxradio('refresh');
          $("#ordenMapa").val(mapa_wms.orden);
          $("#urlMapa").val(mapa_wms.mapa.url);
          $("#capasMapa").val(mapa_wms.mapa.capas);
          $("#estiloMapa").val(mapa_wms.mapa.estilo);
          $("#versionMapa").val(mapa_wms.mapa.version);
          $("#formatoMapa").val(mapa_wms.mapa.formato);
          $("#leyendaMapa").val(mapa_wms.mapa.leyenda);
          $("#glgMapa").prop('checked', mapa_wms.mapa.glg).checkboxradio('refresh');
          $("#cqlFilterMapa").val(mapa_wms.mapa.cql_filter);
          $("#etiquetaMapa").val(mapa_wms.mapa.etiqueta);
          $("#wmstMapa").prop('checked', mapa_wms.mapa.wmst).checkboxradio('refresh');

          $( "#fecha_defectoMapa" ).datepicker({
            dateFormat: "yy-mm-dd",
            defaultDate: new Date(mapa_wms.mapa.fecha_defecto),
             onSelect: function () {
                 mapa_wms.mapa.fecha_defecto = $.datepicker.formatDate("yy-mm-dd", $(this).datepicker('getDate'));
             }});



          //Los campos de la relación se dejan vacios
          $("#dimension1Mapa").val('');
          $("#dimension2Mapa").val('');
          $("#dimension3Mapa").val('');
          $("#dimension4Mapa").val('');
          $("#tiempoMapa").val('');


					//Se actualiza el estado
			    estado = "ASOCIAR";

					//Se abre el formulario
          $( "#addNewMapa" ).popup("open");


			
}

function edicionAvanzadaMapa(){
		$( "#datosEntidadMapa").show();
		$( "#advertenciaMapa").show();
		$( "#edicionAvanzadaMapa").hide();
		$( "#addNewMapa" ).popup("open");
}


function guardarMapaWMS(tabla){

		//Se guardan los datos del formulario en el objeto



		if(estado == "CREAR"){
			mapa_wms.mapa.url = $("#urlMapa").val();
		    mapa_wms.mapa.capas = $("#capasMapa").val();
		    
		    mapa_wms.mapa.estilo = $("#estiloMapa").val();
		    mapa_wms.mapa.version = $("#versionMapa").val();
		    mapa_wms.mapa.formato = $("#formatoMapa").val();
		    mapa_wms.mapa.leyenda = $("#leyendaMapa").val();
		    mapa_wms.mapa.glg = $("#glgMapa").prop("checked");
		    mapa_wms.mapa.cql_filter = $("#cqlFilterMapa").val();
		    mapa_wms.mapa.etiqueta = $("#etiquetaMapa").val();
		   
		    mapa_wms.mapa.wmst = $("#wmstMapa").prop("checked");
		  
					var message = JSON.stringify(mapa_wms.mapa);
          message = message.replace(/""/g,"null");
					//Se envía petición para crear el objeto
					$.ajax({
            url:  contextPath+"/admin/mapa/"+"?token="+tokenId,
            data: message,
            processData: false,
            contentType: 'application/json',
            type: 'POST',
            success: function(data){
            	alert("El mapa se ha guardado correctamente");
                //Se añade a la lista de todos los mapas
                  var rows = all_mapas_wms.rows.all.map( function(r){return r.value});
                  var map = data;
                  
                 rows.unshift(map);
                 mapas_wms[map.id]=map;

                 all_mapas_wms.loadRows(rows);
            },
            error: function(error){
              alert("No ha sido posible realizar los cambios");
            }
          });

		}else if((estado == "EDITAR") | (estado == "ASOCIAR")){
			mapa_wms.dimension1 = $("#dimension1Mapa").val();
			mapa_wms.dimension2 = $("#dimension2Mapa").val();
			mapa_wms.dimension3 = $("#dimension3Mapa").val();
			mapa_wms.dimension4 = $("#dimension4Mapa").val();
			mapa_wms.tempo = $("#tiempoMapa").val();
			mapa_wms.solo_imprimir = $("#imprimirMapa").prop("checked");
	    mapa_wms.siempre_visible = $("#visibleMapa").prop("checked");
	    mapa_wms.orden = $("#ordenMapa").val();
	    mapa_wms.etiqueta= $("#etiquetaMapa").val();
	    mapa_wms.capas=$("#capasMapa").val();

				//Se actualiza sólo la tabla de dimensiones en el dato
        actualizarTablaMapa();
        volverDato();
		}

    $( "#addNewMapa" ).popup("close");

}


function actualizarTablaMapa(){
	// hay que clonar el mapa, sino al añadir nuevos wms se modifican también los anteriores
	  var nuevo_mapa=new Object();
	  $.extend( true, nuevo_mapa,mapa_wms);
		if(currentRowMapa){
      //Editar Mapa
		
      currentRowMapa.val(nuevo_mapa);
      for(var i = 0; i<dato.mapas_wms.length; i++){
          if(dato.mapas_wms[i].id == currentRowMapa.value.id){
        	
            dato.mapas_wms[i] = nuevo_mapa;
          }
      }
    }else{
      //Crear Mapa
    	currentTablaMapa.rows.add(nuevo_mapa);
    	dato.mapas_wms.push(nuevo_mapa);
    }
}
function mostrarMapas(tabla){
	currentTablaMapa=tabla;
				var url = contextPath+"/admin/mapas/";

  			$.ajax({
  				url:  url,
  				processData: false,
  				contentType: 'application/json',
  				type: 'GET',
  				success: function(data){
  					for (var i=0; i<data.mapas.length; i++){
  						var mapa = data.mapas[i];
  						mapas_wms[mapa.id]=mapa;
  					}
  					//Cargar la tabla con todos los datos
  					all_mapas_wms.loadRows(mapas_wms);

  					//Mostrar tabla con todos los mapas y ocultar el dato
  					$("#adminItem").hide();
  					$("#adminDato").hide();
            $("#addMapa").show();
            $("#viewButtons").hide();
            $("#editButtons").hide();
            $("#datoButtons").hide();
            $("#returnDatoButtons").show();
  				},
  				error: function(error){
  					console.log(error)
  				}
  			});
}

function configurarFormularioMapaWMS(){
	
	if(currentTablaMapa==mapaswms_aux_table){
		$(".hideAuxWMS").hide();
	}
	else{
	if ($("#datoEditUdTemporal").val()){
		$("#tiempoMapaContainer").show();
	}
	else{
		$("#tiempoMapaContainer").hide();
	}
	 $(".dimensionMapaContainer").hide();
	if (dato.dimensiones){
		for (var i=0; i<dato.dimensiones.length; i++){
			$("#dimension"+dato.dimensiones[i].orden+"MapaContainer").show();
			$("#dimension"+dato.dimensiones[i].orden+"MapaContainer label").text(dato.dimensiones[i].dimension.titulo);
		}
	}
	}
}

function changeGlg(){
	if ($("#glgMapa").prop("checked")){
		$(".notGlg").addClass("oculto");
		
	}
	else{
		$(".notGlg").removeClass("oculto");	}
}

function changeWMST(){
	if ($("#wmstMapa").prop("checked")){
		
		$(".onlyWMST").removeClass("oculto");
	}
	else{
		$(".onlyWMST").addClass("oculto");
	}
}

function verLeyendaMapaWMS(){
	if ($("#glgMapa").prop("checked")){
		var listaLeyendas =  $("#capasMapa").val().split(',');
		var listaStyles;
		if ($("#estiloMapa").val()){
			listaStyles = v.split(',');
		}
		var i = 0;
		for (var layerItem in listaLeyendas) {
			i++;
			
			// deberia haber tantos styles como layers
			var currentStyle;
			try {
				currentStyle = listaStyles[layerItem];
			} catch(err) {
				// no hay estilos
			}
			window.open($("#urlMapa").val()+"?SERVICE=WMS&VERSION="+($("#versionMapa").val() ? $("#versionMapa").val() : "1.0.0")+"&REQUEST=GetLegendGraphic&format=image/png&layer=" + listaLeyendas[layerItem] + (currentStyle? "&STYLE="+currentStyle:"")+"&width=20&height=20&legend_options=forceLabels:on;fontName:Lucida%20Sans%20Regular");
		}
	}
	else{
		if ($("#leyendaMapa").val()){
			window.open($("#leyendaMapa").val());
		}
		else{
			alert("Debe indicar la ruta de la leyenda o marcar la opción de usar leyenda del WMS (GetLegendGraphic)");
		}
	}
}

function verMapaWMS(){
	 var default_params = "&SERVICE=WMS&VERSION="+($("#versionMapa").val() ?$("#versionMapa").val(): '1.1.1')+"&REQUEST=GetMap&bbox=567985,4411217,811838,4756589&width=542&height=768";
	    var default_srs = "&SRS=EPSG%3A25830";
	    var default_styles = "&STYLES="+($("#estiloMapa").val() ? $("#estiloMapa").val() :"");
	    var restoWMS = default_params + default_srs + "&TRANSPARENT=TRUE&FORMAT="+($("#formatoMapa").val() ? $("#formatoMapa").val() : 'image/png') + default_styles;
	    window.open( $("#urlMapa").val()+ "?LAYERS=" + $("#capasMapa").val() + restoWMS+
	    ($("#cqlFilterMapa").val() ? "&CQL_FILTER="+ $("#cqlFilterMapa").val():""));
}