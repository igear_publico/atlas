var table;
var invertedValuesCodigoIndicadorAgrupado = new Array();
invertedValuesCodigoIndicadorAgrupado.push(210); // "Esperanza de vida al nacer"
invertedValuesCodigoIndicadorAgrupado.push(211); // "Esperanza de vida a los 65 años"
invertedValuesCodigoIndicadorAgrupado.push(14); // "Cobertura vacunación gripe"
var cols_color=['porcentaje','rems','prp_dosdecimales','rem','texto_tasa','edad','valor'];
var campos_color=['indicador_quintiles','indicador_quintiles','significacion_estadistica','indicador_quintiles','interval_tasa','indicador_quintiles','indicador_quintiles'];
//var printActionXSL_ZBS = "/home/idearium_eu/Mapa/templates/informeAtlasIDEAragon_ZBS.xsl";
var printActionXSL_ZBS = "/app/jboss/Mapa/templates/informeAtlasIDEAragon_ZBS.xsl";


function initComboZBS(){
	var etiquetas = Object.keys(cUtList["ZBS"]);

	for (var i=0; i<etiquetas.length; i++){
		$("#selectZBS").append("<option value='"+cUtList["ZBS"][etiquetas[i]]+"'>"+etiquetas[i]+"</option>");
	}
}

function initTableZBS(){

	table = FooTable.init('#dataTable', {
		"columns": [
		            { "name": "grupo_indicador","breakpoints": "xs sm md", "title":"Área"},
		            { "name": "nombre_indicador_agrupado", "title": "Categoría"},
		            { "name": "periodo_de_estudio_indicador", "breakpoints": "xs sm md","title": "Último periodo disponible"},
		            { "name": "sexo", "title": "Sexo","formatter": function(value, options, rowData){
		            	return parseSexo(value);
		            } },
		            { "name": "porcentaje", "title": "%",  "data-type":"number"},
		            { "name": "rems", "title": "<abbr title='Razón Estandarizada de la Mortalidad/Morbilidad suavizada'>REMS</abbr>" , "data-type":"number"},
		            { "name": "prp_dosdecimales", "title": "<abbr title='Probabilidad de Exceso de Riesgo'>PRP</abbr>", "data-type":"number"},
		            { "name": "rem", "title": "<abbr title='Razón Estandarizada de la Mortalidad/Morbilidad'>REM</abbr>" , "data-type":"number"},
		            { "name": "casos_mostrar", "visible":false},
		            { "name": "casos", "title": "Nº de casos", "breakpoints": "xs", "data-type":"number","formatter": function(value, options, rowData){
		            	return parseNumCasos(value,rowData.casos_mostrar);
		            } },
		            { "name": "texto_tasa", "title": "Tasa por 1.000" , "data-type":"number"},
		            { "name": "edad", "title": "Años" , "data-type":"number"},
		            { "name": "valor", "title": "Valor", "data-type":"number"}
		            ]});
	$('.footable-filtering-search input').attr("placeholder", "Buscar");
	loadZBSTable();
}

function parseSexo(value){
	return (value==1 ? "Hombres":(value==2 ?"Mujeres":(value==3?"Ambos sexos":"")));
}

function parseNumCasos(value,casos_mostrar){
	return (value && value.toString()!=casos_mostrar ? casos_mostrar : value);
}
function loadZBSTable(){
	showLoading(true);
	$.ajax({
		url: contextPath+"/Salud/info/zbs/"+$("#selectZBS").val()+"/"+$("#selectSexo").val(),
		type: 'GET',
		success: function(data) {
			var valores_tabla=data.datos;
			var datos_tabla = new Array();
			for (var i=0;i<valores_tabla.length;i++){
				var values=Object.assign({}, valores_tabla[i]);;
				var isSuaviz =  values.unidad_medida_indicador.toLowerCase().indexOf("rrsuavizad") != -1;
				for (var j=0; j<campos_color.length;j++){
					var columna = campos_color[j].trim();
					var col_name = cols_color[j].trim();
					if ((typeof valores_tabla[i][col_name] != 'undefined')&&(valores_tabla[i][col_name] != null)){
						values[col_name]=new Object();
						var bgColor;
						if (isSuaviz && (columna==="significacion_estadistica")){
							bgColor = getColorSignificacionEstadisticaZBS(valores_tabla[i][columna], valores_tabla[i].codigo_indicador_agrupado);
						}
						else if ((columna=="interval_tasa")&&((values.unidad_medida_indicador.toLowerCase()=='porcentaje')||(values.unidad_medida_indicador.toLowerCase()=='años'))){
							bgColor = 'transparent';
						}
						else{
							bgColor = getColorIntervalZBS(valores_tabla[i][columna], valores_tabla[i].codigo_indicador_agrupado);
						}
						values[col_name].options = {	"style": {	"backgroundColor": bgColor, "color": getFontColorDependingBg(bgColor) }};
						values[col_name].value=valores_tabla[i][col_name];
					}

				}
				
				datos_tabla.push(values);

			}
			table.loadRows(datos_tabla,false);

			showLoading(false);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("No se han podido obtener los datos " + textStatus + "\n " + errorThrown);
			showLoading(false);
		}
	});

}

function getColorIntervalZBS(value, codigo_indicador_agrupado) {
	var val = value;
	if (isInvertedColorsCodigoIndicadorAgrupado(codigo_indicador_agrupado)) {
		val = Math.abs(6 - val);
	}
	return getColorIntervalZBS_fromValue(val);
}

function getColorIntervalZBS_fromValue(val) {
	if (val == "1") {
		return '#1a9850';
	} else if (val == "2") {
		return '#91cf60';
	} else if (val == "3") {
		return '#fee08b';
	} else if (val == "4") {
		return '#fc8d59';
	} else if (val == "5") {
		return '#d73027';
	} else {
		return 'transparent';
	}
}

function isInvertedColorsCodigoIndicadorAgrupado(codigo_indicador_agrupado) {
	if (typeof invertedValuesCodigoIndicadorAgrupado != 'undefined') {
		for (var i=0; i < invertedValuesCodigoIndicadorAgrupado.length; i++) {
			if (invertedValuesCodigoIndicadorAgrupado[i] === codigo_indicador_agrupado) {
				return true;
			}
		}
	}
	return false;
}

function getColorSignificacionEstadisticaZBS(value, codigo_indicador_agrupado) {
	var val = value;
	if (isInvertedColorsCodigoIndicadorAgrupado(codigo_indicador_agrupado)) {
		val = Math.abs(3 - val);
	}
	if (val == 0) {
		return '#FFFFFF';
	} else if (val === 1) {
		return '#1a9850';
	} else if (val == 2) {
		return '#fee08b';
	} else if (val == 3) {
		return '#d73027';
	} else {
		return 'transparent';
	}
}

function printPDF() {
	queryXML = "";
	queryXML +=  getTablaToPrint();
	queryXML += "<titulo>Indicadores por zona básica de salud</titulo>";
	queryXML += "<territorialUnit>" + $("#selectZBS option:selected").text() + "</territorialUnit>";
    queryXML += "<gender>" + $("#selectSexo option:selected").text() + "</gender>";
		
	var query = makeXMLsafeAcentos2(queryXML).replace(/%/g, "&#37;").replace(/«/g, "&#171;").replace(/»/g, "&#187;").replace(/«/g, "&#171;").replace(/»/g, "&#187;").replace(/€/g, "&#8364;").replace(/‰/g, "&#8240;");
    query = query.replace(/Á/g, "&#193;").replace(/É/g, "&#201;").replace(/Í/g, "&#205;").replace(/Ó/g, "&#211;").replace(/Ú/g, "&#218;").replace(/º/g,"&#186;");

    document.printForm.xml.value = "<imprimir>" + query + "</imprimir>";
    //document.printForm.xml.value = "<imprimir>" + makeXMLsafeAcentos(requestXML).replace(/??/g, "&#205;") + getPrintCharts() + "</imprimir>";
    console.log(query);
	document.printForm.xslt.value = printActionXSL_ZBS;
    
    document.printForm.action = printActionURL;
    document.printForm.submit();
}

function getTablaToPrint(){
	if (table){
	var cols=new Array();

	var tabla ="<tabla><thead><tr role='row'>";
	for (var i=0; i<table.columns.array.length; i++){
		if (!table.columns.array[i].hidden && table.columns.array[i].visible){
			tabla += '<th  role="columnheader">'+table.columns.array[i].title+'</th>';
			cols.push(table.columns.array[i].name);
		}
	}
	tabla +="</tr></thead><tbody>";
	for (var i=0; i<table.rows.all.length; i++){
		tabla +="<tr  role='row'>";
		for (var j=0; j<cols.length; j++){
			var valor = table.rows.all[i].value[cols[j]];
			if (cols[j]=="sexo"){
				valor = parseSexo(valor);
			}
			else if (cols[j]=="casos"){
				valor = parseNumCasos(valor,table.rows.all[i].value["casos_mostrar"]);
			}
			tabla += "<td>"+(valor?valor:"")+"</td>";
		}
		tabla +="</tr>"
		
	}
	
	tabla +="</tbody></tabla>";
	return tabla;
	}
	else{
		return "";
	}
		
}