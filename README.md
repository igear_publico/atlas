# Atlas

Atlas interactivo de Aragón

### Dependencias

Para el funcionamiento de la aplicación Atlas es necesario disponer de:
- una base de datos Postgres 
- el conjunto de librerías javascript externas y código javascript compartido de IDEARAGON denominado lib (https://gitlab.com/igear_publico/lib)

### Instalación

Para instalar el atlas hay que generar el war con los fuentes proporcionados y desplegarlo en el servidor de aplicaciones.
